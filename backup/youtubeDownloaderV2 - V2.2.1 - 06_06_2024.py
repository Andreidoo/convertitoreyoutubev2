import os
import base64
import re
import customtkinter
import time
import requests
import threading
import sys
import cairosvg
import hashlib
from pytubefix import YouTube, Playlist
from pytubefix.exceptions import RegexMatchError
from customtkinter import filedialog
from PIL import Image, ImageTk
from io import BytesIO
from moviepy.editor import VideoFileClip
from unidecode import unidecode
from datetime import datetime
from mutagen.easyid3 import EasyID3

# Global variables
downloading = False
thumbnailLabel = None
updateThumbnailDelay = 1
new_features_shown = False

# Logs directory
directory_path = os.path.join(os.path.expanduser('~'), 'AppData', 'Roaming', 'ConvertitoreYouTube', 'log')

if not os.path.exists(directory_path):
    os.makedirs(directory_path)

os.chdir(directory_path)

# Configs directory
configs_path = os.path.join(os.path.expanduser('~'), 'AppData', 'Roaming', 'ConvertitoreYouTube', 'config')

if not os.path.exists(configs_path):
    os.makedirs(configs_path)

os.chdir(configs_path)

# Download folder
DOWNLOADS_FOLDER = os.path.join(os.path.expanduser('~'), 'Downloads', 'Convertitore YouTube')

# Icons folder
icons_path = os.path.join(os.path.expanduser('~'), 'AppData', 'Roaming', 'ConvertitoreYouTube', 'icons')

if not os.path.exists(icons_path):
    os.makedirs(icons_path)

os.chdir(icons_path)

# Functions
def onUrlChange(*args):
    global update_thumbnail
    url = link.get().strip()
    if url:
        download.configure(state='normal')
        mp4RadioButton.configure(state='normal')
        mp3RadioButton.configure(state='normal')
        playlistButton.configure(state='normal')
        messageLabel.configure(text='', padx=0, pady=0)

        if not downloading:
            resetProgress()

        update_thumbnail = True
        threading.Thread(target=delayedThumbnailUpdate).start()

        if 'playlist' in url.lower():
            mp4RadioButton.configure(state='disabled')
            mp3RadioButton.configure(state='disabled')
            playlistButton.pack(side='left', padx=10)
            download.pack_forget()
            showDefaultThumbnail()
        else:
            playlistButton.pack_forget()
            download.pack(side='left', padx=10)
            mp4RadioButton.configure(state='normal')
            mp3RadioButton.configure(state='normal')
    else:
        download.configure(state='disabled')
        mp4RadioButton.configure(state='disabled')
        mp3RadioButton.configure(state='disabled')
        playlistButton.pack_forget()
        download.pack(side='left', padx=10)
        if thumbnailLabel:
            thumbnailLabel.destroy()

def cleanFilename(filename):
    cleanedFilename = re.sub(r'^\w\s\(\)-.,!@#$%^&+=', '', filename)
    cleanedFilename = unidecode(cleanedFilename)
    return cleanedFilename

def convertToMp3(videoFilename, mp3Filename):
    output_file_path = os.path.join(directory_path, 'MoviePyOutput.log')
    original_stdout = sys.stdout
    original_stderr = sys.stderr

    with open(output_file_path, 'a', encoding='utf-8') as output:
        sys.stdout = output
        sys.stderr = output
        try:
            videoClip = VideoFileClip(videoFilename)
            audioClip = videoClip.audio
            audioClip.write_audiofile(mp3Filename, codec='mp3', bitrate='192k')
            audioClip.close()
            videoClip.close()
            os.remove(videoFilename)
        except Exception as e:
            timestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            error_log_file = os.path.join(directory_path, 'GenericErrors.log')
            with open(error_log_file, 'a', encoding='utf-8') as error:
                error.write(f'{timestamp}|ERROR|Error converting to MP3: {str(e)}|\n')
            sys.stdout = error
            sys.stderr = error
        finally:
            sys.stdout = original_stdout
            sys.stderr = original_stderr

def startDownload():
    global downloading, startTime, downloadSpeedLabel
    startTime = time.time()
    try:
        ytLink = link.get()
        selectedFormat = formatVar.get()
        ytObject = YouTube(ytLink, on_progress_callback=onProgress)

        if not os.path.exists(DOWNLOADS_FOLDER):
            os.makedirs(DOWNLOADS_FOLDER)
        os.chdir(DOWNLOADS_FOLDER)

        downloading = True
        download_button = language_strings[languages.get()]['download_button']
        download.configure(text=download_button, state='disabled')
        mp4RadioButton.configure(state='disabled')
        mp3RadioButton.configure(state='disabled')
        languages.configure(state='disabled')
        pasteButton.configure(state='disabled')
        messageLabel.configure(text='', padx=0, pady=0)

        title = ytObject.title
        author = ytObject.author
        publish_year = ytObject.publish_date.year if ytObject.publish_date else None
        album = ytObject.title

        if selectedFormat == 'mp4':
            video = ytObject.streams.get_highest_resolution()

            base_filename, file_extension = os.path.splitext(video.default_filename)

            counter = 1
            filename = f'{base_filename}{file_extension}'

            while os.path.exists(filename):
                counter += 1
                filename = f'{base_filename}({counter}){file_extension}'

            video.download(filename=filename)
        elif selectedFormat == 'mp3':
            video = ytObject.streams.get_highest_resolution()

            base_filename, file_extension = os.path.splitext(video.default_filename)

            counter = 1
            videoFilename = f'{base_filename}{file_extension}'

            while os.path.exists(videoFilename):
                counter += 1
                videoFilename = f'{base_filename}({counter}){file_extension}'

            video.download(filename=videoFilename)

            counter = 1
            mp3Filename = f'{base_filename}.mp3'

            while os.path.exists(mp3Filename):
                counter += 1
                mp3Filename = f'{base_filename}({counter}).mp3'

            try:
                conversion_in_progress = language_strings[languages.get()]['conversion_in_progress']
                pPercentage.configure(text=conversion_in_progress)
                pPercentage.update()

                time.sleep(3)

                convertToMp3(videoFilename, mp3Filename)

                audio = EasyID3(mp3Filename)
                audio['title'] = title
                audio['artist'] = author
                audio['album'] = album
                if publish_year:
                    audio['date'] = str(publish_year)
                audio.save()

            except Exception as e:
                error_conversion_mp3 = language_strings[languages.get()]['error_conversion_mp3']
                messageLabel.configure(text=error_conversion_mp3, text_color='red', padx=10, pady=10, wraplength=300)
                
                timestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                error_log_file = os.path.join(directory_path, 'GenericErrors.log')
                with open(error_log_file, 'a') as error:
                    error.write(f'{timestamp}|ERROR|{str(e)}|\n')
                sys.stdout = error
                sys.stderr = error

        download_complete = language_strings[languages.get()]['download_complete']
        messageLabel.configure(text=download_complete, text_color='green', padx=10, pady=10, wraplength=300)
    except Exception as e:
        invalid_link = language_strings[languages.get()]['invalid_link']
        messageLabel.configure(text=invalid_link, text_color='red', padx=10, pady=10, wraplength=300)

        timestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        error_log_file = os.path.join(directory_path, 'GenericErrors.log')
        with open(error_log_file, 'a') as error:
            error.write(f'{timestamp}|ERROR|{str(e)}|\n')
        sys.stdout = error
        sys.stderr = error
    finally:
        downloading = False
        download_button = language_strings[languages.get()]['download_button']
        download.configure(text=download_button, state='normal')
        mp4RadioButton.configure(state='normal')
        mp3RadioButton.configure(state='normal')
        languages.configure(state='normal')
        pasteButton.configure(state='normal')
        download_speed = language_strings[languages.get()]['download_speed']
        downloadSpeedLabel.configure(text=f'{download_speed} 0 MB/s')
        resetProgress()

def onProgress(stream, chunk, bytes_remaining):
    global downloadSpeedLabel
    total_size = stream.filesize
    bytes_downloaded = total_size - bytes_remaining
    percentage_of_completion = bytes_downloaded / total_size * 100
    per = str(int(percentage_of_completion))
    pPercentage.configure(text=per + '%')
    pPercentage.update()

    download_speed = language_strings[languages.get()]['download_speed']
    downloadSpeed = (bytes_downloaded / 1024 / 1024) / (time.time() - startTime)
    downloadSpeedLabel.configure(text=f'{download_speed} {downloadSpeed:.2f} MB/s')

    progressBar.set(float(percentage_of_completion) / 100)

def resetProgress():
    pPercentage.configure(text='0%')
    progressBar.set(0)

def handleCtrlA(event):
    link.select_range(0, customtkinter.END)
    link.icursor(customtkinter.END)
    onUrlChange()

def onEntryClick(event):
    if link.get() == placeholder:
        link.delete(0, customtkinter.END)

def onFocusOut(event):
    if not link.get():
        link.insert(0, placeholder)

def pasteLink(event=None):
    try:
        link.delete(0, customtkinter.END)
        link.insert(0, app.clipboard_get())
        onUrlChange()
    except:
        return

def handleCtrlZ(event):
    link.delete(0, customtkinter.END)

def getThumbnail():
    try:
        url = link.get()
        thumbnail = YouTube(url).thumbnail_url
        return thumbnail
    except RegexMatchError:
        return None

def getAndShowThumbnail():
    global thumbnailLabel

    thumbnail_url = getThumbnail()
    if thumbnail_url is not None:
        try:
            response = requests.get(thumbnail_url)
            img_data = response.content
            img = Image.open(BytesIO(img_data))
            img = img.resize((400, 180))
            photo = ImageTk.PhotoImage(img)

            if thumbnailLabel:
                thumbnailLabel.destroy()

            thumbnailLabel = customtkinter.CTkLabel(app, image=photo, text='')
            thumbnailLabel.image = photo
            thumbnailLabel.pack(padx=10, pady=10)
        except Exception:
            return None

def delayedThumbnailUpdate():
    time.sleep(updateThumbnailDelay)
    if update_thumbnail:
        getAndShowThumbnail()

def checkNewVersion():
    gitlab_project_id = '58147967'
    api_url = f'https://gitlab.com/api/v4/projects/{gitlab_project_id}/releases'

    try:
        response = requests.get(api_url)
        release_data = response.json()
        latest_version = release_data[0]['tag_name']
        return latest_version
    except Exception as e:
        timestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        error_log_file = os.path.join(directory_path, 'VersionErrors.log')
        with open(error_log_file, 'a') as error:
            error.write(f'{timestamp}|ERROR|Error checking versions: {str(e)}|\n')
        sys.stdout = error
        sys.stderr = error
        return None

latest_version = checkNewVersion()
current_version = '2.2.1'

def downloadLatestVersion():
    messageLabel.configure(text='')
    messageLabel.pack(pady=(9, 0))
    gitlab_project_url = 'https://gitlab.com/Andreidoo/convertitoreyoutubev2'
    file_name = 'Convertitore YouTube.exe'
    repo_url = f'{gitlab_project_url}/-/raw/main/executable/{file_name}?ref_type=heads&inline=false'
    
    try:
        destination_directory = filedialog.askdirectory()
        if destination_directory:
            downloadNewVersionButton.configure(state='disabled')
            pasteButton.configure(state='disabled')
            link.configure(state='disabled', fg_color='gray')
            languages.configure(state='disabled')
            file_path = os.path.join(destination_directory, file_name)
            if os.path.exists(file_path):
                file_already_present = language_strings[languages.get()]['file_already_present']
                messageLabel.configure(text=file_already_present, text_color='red', padx=10, pady=10, wraplength=300)
            else:
                response = requests.get(repo_url, stream=True)
                if response.status_code == 200:
                    total_size = int(response.headers.get('content-length', 0))
                    bytes_downloaded = 0
                    chunk_size = 1024
                    with open(file_path, 'wb') as f:
                        for chunk in response.iter_content(chunk_size=chunk_size):
                            if chunk:
                                f.write(chunk)
                                bytes_downloaded += len(chunk)
                                if bytes_downloaded % (100 * chunk_size) == 0:
                                    percentage_of_completion = bytes_downloaded / total_size * 100
                                    per = str(int(percentage_of_completion))
                                    pPercentage.configure(text=per + '%')
                                    pPercentage.update()
                                    progressBar.set(percentage_of_completion / 100)
                        file_downloaded = language_strings[languages.get()]['file_downloaded']
                        messageLabel.configure(text=file_downloaded, text_color='green', padx=10, pady=10, wraplength=300)
                        progressBar.set(0)
                        pPercentage.configure(text='0%')
                else:
                    error_during_file_download = language_strings[languages.get()]['error_during_file_download']
                    messageLabel.configure(text=error_during_file_download, text_color='red', padx=10, pady=10, wraplength=300)
        else:
            directory_not_selected = language_strings[languages.get()]['directory_not_selected']
            messageLabel.configure(text=directory_not_selected, text_color='red', padx=10, pady=10, wraplength=300)
    except Exception as e:
        timestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        error_log_file = os.path.join(directory_path, 'VersionErrors.log')
        with open(error_log_file, 'a') as error:
            error.write(f'{timestamp}|ERROR|Error downloading new version: {str(e)}|\n')
        sys.stdout = error
        sys.stderr = error
    finally:
        downloadNewVersionButton.configure(state='normal')
        pasteButton.configure(state='normal')
        link.configure(state='normal', fg_color=app._fg_color)
        languages.configure(state='normal')

def changeLanguage(selected_language):
    global placeholder, language_strings, download_button, download_complete, invalid_link, copyright_label
    global new_version, file_downloaded, error_during_file_download, directory_not_selected, paste_button
    global error_conversion_mp3, conversion_in_progress, file_already_present, playlist_downloaded
    global playlist_button, playlist_error, popup_close_button, popup_remember_box, news_text, main_news
    global date_label, download_speed

    placeholder = language_strings[selected_language]['placeholder']
    download_button = language_strings[selected_language]['download_button']
    download_complete = language_strings[selected_language]['download_complete']
    invalid_link = language_strings[selected_language]['invalid_link']
    copyright_label = language_strings[selected_language]['copyright_label']
    new_version = language_strings[selected_language]['new_version']
    file_downloaded = language_strings[selected_language]['file_downloaded']
    error_during_file_download = language_strings[selected_language]['error_during_file_download']
    directory_not_selected = language_strings[selected_language]['directory_not_selected']
    paste_button = language_strings[selected_language]['paste_button']
    error_conversion_mp3 = language_strings[selected_language]['error_conversion_mp3']
    conversion_in_progress = language_strings[selected_language]['conversion_in_progress']
    file_already_present = language_strings[selected_language]['file_already_present']
    playlist_downloaded = language_strings[selected_language]['playlist_downloaded']
    playlist_button = language_strings[selected_language]['playlist_button']
    playlist_error = language_strings[selected_language]['playlist_error']
    popup_close_button = language_strings[selected_language]['popup_close_button']
    popup_remember_box = language_strings[selected_language]['popup_remember_box']
    news_text = language_strings[selected_language]['news']
    main_news = language_strings[selected_language]['main_news']
    date_label = language_strings[selected_language]['date_label']
    download_speed = language_strings[selected_language]['download_speed']

    link.delete(0, customtkinter.END)
    link.insert(0, placeholder)
    languages.focus_set()
    mp4RadioButton.configure(state='disabled')
    mp3RadioButton.configure(state='disabled')
    download.configure(text=download_button, state='disabled')
    copyrightLabel.configure(text=copyright_label)
    if 'new_version' in language_strings[selected_language] and 'downloadNewVersionButton' in globals():
        new_version = language_strings[selected_language]['new_version']
        downloadNewVersionButton.configure(text=new_version)
    pasteButton.configure(text=paste_button)
    playlistButton.configure(text=playlist_button)
    if 'download_speed' in language_strings[selected_language] and 'downloadSpeedLabel' in globals():
        downloadSpeedLabel.configure(text=f'{download_speed} 0 MB/s')

    saveDefaultLanguageToConfig()

def saveDefaultLanguageToConfig():
    selected_language = languages.get()
    config_file_path = os.path.join(configs_path, 'language.config')
    with open(config_file_path, 'w') as config_file:
        config_file.write(f'language={selected_language}')

def loadDefaultLanguageFromConfig():
    config_file_path = os.path.join(configs_path, 'language.config')
    if os.path.exists(config_file_path):
        with open(config_file_path, 'r') as config_file:
            line = config_file.readline().strip()
            if line.startswith('language='):
                selected_language = line[len('language='):]
                if selected_language in language_strings:
                    return selected_language
    return 'IT'

def defaultLanguageFromConfig():
    selected_language = loadDefaultLanguageFromConfig()
    languagesVar.set(selected_language)
    changeLanguage(selected_language)

def downloadPlaylist():
    global downloading
    try:
        playlist_link = link.get()

        if 'playlist' not in playlist_link.lower():
            playlist_error = language_strings[languages.get()]['playlist_error']
            messageLabel.configure(text=playlist_error, text_color='red', padx=10, pady=10, wraplength=300)
            return

        if not os.path.exists(DOWNLOADS_FOLDER):
            os.makedirs(DOWNLOADS_FOLDER)
        os.chdir(DOWNLOADS_FOLDER)

        playlist = Playlist(playlist_link)

        downloading = True

        download.configure(state='disabled')
        mp4RadioButton.configure(state='disabled')
        mp3RadioButton.configure(state='disabled')
        playlist_button = language_strings[languages.get()]['playlist_button']
        playlistButton.configure(text=playlist_button, state='disabled')
        pasteButton.configure(state='disabled')
        languages.configure(state='disabled')
        messageLabel.configure(text='', padx=0, pady=0)

        total_videos = len(playlist.video_urls)
        video_count = 0
        resetProgress()

        startTime = time.time()

        for video in playlist.video_urls:
            yt = YouTube(video)
            video_stream = yt.streams.get_highest_resolution()

            title = yt.title
            author = yt.author
            album = yt.title
            publish_year = yt.publish_date.year if yt.publish_date else None

            base_filename, file_extension = os.path.splitext(video_stream.default_filename)

            audio_filename = f'{base_filename}.mp3'
            counter = 1
            while os.path.exists(audio_filename):
                audio_filename = f'{base_filename} - ({counter}).mp3'
                counter += 1

            video_stream.download()

            bytes_downloaded = os.path.getsize(video_stream.default_filename)
            download_speed = (bytes_downloaded / 1024 / 1024) / (time.time() - startTime)

            convertToMp3(video_stream.default_filename, audio_filename)

            audio = EasyID3(audio_filename)
            audio['title'] = title
            audio['artist'] = author
            audio['album'] = album
            if publish_year:
                audio['date'] = str(publish_year)
            audio.save()

            video_count += 1

            percentage_of_completion = (video_count / total_videos) * 100
            per = str(int(percentage_of_completion))
            pPercentage.configure(text=per + '%')
            pPercentage.update()
            progressBar.set(float(percentage_of_completion) / 100)

            download_speed_text = language_strings[languages.get()]['download_speed']
            downloadSpeedLabel.configure(text=f'{download_speed_text} {download_speed:.2f} MB/s')
            downloadSpeedLabel.update()

        playlist_downloaded = language_strings[languages.get()]['playlist_downloaded']
        messageLabel.configure(text=playlist_downloaded, text_color='green', padx=10, pady=10, wraplength=300)

    except Exception as e:
        playlist_error = language_strings[languages.get()]['playlist_error']
        messageLabel.configure(text=playlist_error, text_color='red', padx=10, pady=10, wraplength=300)

        timestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        error_log_file = os.path.join(directory_path, 'GenericErrors.log')
        with open(error_log_file, 'a') as error:
            error.write(f'{timestamp}|ERROR|Error downloading the playlist: {str(e)}|\n')
        sys.stdout = error
        sys.stderr = error
    finally:
        downloading = False
        playlist_button = language_strings[languages.get()]['playlist_button']
        playlistButton.configure(text=playlist_button, state='normal')
        pasteButton.configure(state='normal')
        languages.configure(state='normal')
        download_speed = language_strings[languages.get()]['download_speed']
        downloadSpeedLabel.configure(text=f'{download_speed} 0 MB/s')
        resetProgress()

def showNewFeaturesPopup():
    global new_features_shown
    if not new_features_shown:
        new_features_shown = True

        remember_choice_path = os.path.join(configs_path, 'popupChoice.config')
        remember_choice = False
        if os.path.exists(remember_choice_path):
            with open(remember_choice_path, 'r') as f:
                remember_choice_str = f.read().strip().split('=')[-1].lower()
                if remember_choice_str == 'true':
                    remember_choice = True

        if remember_choice:
            return

        popup_windowIcon = '''
        AAABAAEAAAAAAAEAIADQrAAAFgAAAIlQTkcNChoKAAAADUlIRFIAAAEAAAABAAgGAAAAXHKoZgAAAAFvck5UAc+id5oAAIAASURBVHja7Z0FeBTX18ZTJJDgEsVLgUJbqL
        u7y7/upS3u7u4SJAkQd3cjCYRAFA/u7u6ufb9z78zszuzOrESA9tvt8z6h0Y2c33vOuefea2dne9getoftYXvYHraH7WF72B62h+1he9getoftYXvYHraH7VFejx9e7miT
        TTaVQv/qRyu7p7gesXuS/3/4nJgqc4bNt58zbJ5NNtlkoNkkz5E+VU8dOFXJ2a6l3aNVnuXx8698tLTrYPea80d2T1R/if3b5fHqL378TN3X+z5b743BNtlkk6YGPVXr1Y
        5tKj39ZGO7R6uy+Hm82ov/TgC0q/o8e/Jt6KV/2yrPHX+08rOwySabzOo6xcvqx+xf+Ivip/q/DgDdvuhn98hDT9o1t3vcpZ398/70DdwlwSabbLJK+0lfPlbtBbsWdk/8
        uwDAHm2qPvM51THHbL9Im2wqnSgLiG1q167evw4AACo9UfOlgQQA2y/SJptKKcqgV1Mm3e7fCIDKHWq/PJLVNLZfpE02lRoA6wgAT/6rANDz0752uEsAqPXyyDY2ANhkU1
        kAUEIA6PCvAsCPvw6z20oZwBN1X7NlADbZ9P8NAK5+C+yoBqj8aLMPRj5e9QXbL9Imm/4/AcA+PpgDoPUjH4983N4GAJts+n8FAIeYIAEALT+yAcAmm/7fASDaBgCbbPrX
        ASD3wim2fGf38661dq6rM+3sipPtKi1PMVaxTEWiCpmS7SoXJNtVWZpgt5sNArm9PeTxh57G45We+e+ockXpWTxm/zweq/aCQrYgsAHgngDgjS2Fdv32b7azy4+zm318b/
        XGa7IfJgA8QQHfwUjFMhWJKmRK7kAA6FC5MKW93eac9o0/+3tOq0c+RKtWH5E+1tYjWvpIUEsNPayhFibU3Fitm2moqZo+ROsmH+pfqqmxihqZVhvX99GuxsuqELABwQaA
        CgdAo7XZdp9uX2Hfdv2Sj6suT/EnFVKwryWVGKlYVJFMhSklFPwypZTYp0UdcwwLgGM4ib0MFRXC5C8omBQkKlBUAMlflJ8faviK8iEtEDWfNE9QTW+SlyhP0lwmX9ScQ5
        pNmuWLWkwepJmkGT6oNV3UNNJUH9RmmkKavAC1J5EmkiaIGk8aRxo7X9AY0mjSqPmoM1LUiHmoM5w0jDRUUN0hpMGkQaSB3oIGiOrnjXpcXvTveWjQxQMPP/oNHqvyDNpV
        e57rMZ1sILABoIIAMOnwTrtRh7bbfbdzjWOjNVkD7YuTD9gtT7lLgQ5VFctUJKpQEAW+oHxRhan0fun0Pmn0dlEFovJTUTlP1DLSUlG5pCWkHCb6nItTUGURKZuUJSqTtJ
        CUkYwq6cmoypRGSiWlkJJJSaTEJFRNSIJ9PClOVCwphhSdCPsoUmQiqkWQwklhpNAEVAshBQuqHkgKIPkzxaO6H8mX5BMPhwWk+aR5JO84OHiRPElzSXPi4DibNIvkESto
        JmkGaXosakwjTSVNiSHRy4nRaPRuL/qFP0vB/5wo0yCwBYgNAGV+LDp3ks/tN1qd1bVqcfLJSsuToRn8y1WCXweAZGMA5JGWiVoqKpe0JAlVckQtFrWIlE3KEpWZSEGeiK
        pMGaR0UhopVVRKAgV6AuyTSImiEkjxpLh4CnRSDCk6HtWiSJGkCFHhcRTspNA4VA8hBZOCSIFMsRTwJL9YOPiK8iEtIM2PoWAneZO8YuDoKWpONAW7qFkkD9LMaNSYQZoe
        RcFOmipqCmlyFGpOIk2MRM0JpPGRBIAYOP00Dm3qvoZ2VZ9BWx0E9DCwQcAGgHJ/fLljpd23O1e3cShOXlGp2EzwmwCAUfDLASAPfjUALDIAQGYpAJAgKi5BCH4RANUMAc
        CCXxMAYvD7ywDgIwEgRgEARwkAc2UAkIKfVEMCwDQZAMTgr8GDP0oIfhkA6vX0QsumH6Ft5afQljKBttUE2SBgA0CFAaDp2kV2zUoW/Wa/IuX0Q5YGf7GF7p+n7v46ABi6
        f7Z17l/VMPjjZQCQ3F8NAObc3xAAau6vA0C0AIBZpXf/muPYy2jUGhGKJs/8gkerPIlH7Z8mPaMCAmMI2ALFBoBSPf4B7B4uWWzXsmTxKIcVqbcesjb9LzQAgFrwL1Nx/y
        Um0n+5+2eoACBFw/2l4I8z4/5i8Jt1fz9t93fQcv9ZBu4/XcX9tQAwlrKA8TFw/bAvWld/Bm2qPok2HAJKENggYANAuQLAeXUmA8AEAsDd0gGgHNx/kRn3VwDAwP0Ty+D+
        lqT/8uCfp+H+WrX/dK3gj1IGvwSACbGo33EqWtZ/Fa2rtEdrewaBp0QQmIaALVhsACgVAOquyrAMABXh/otMpP9m3L+qofsbAEAR/HIAhGqk/1LwywEgpf6m3F8CgBT8pt
        x/kob7iwCoOS4Gdfr7otnDH6NVlSfQyr4Dh0BrIwgIPQFbFmADQDkAYGGZAGCx++cmqTf/sg3Sf0vcXyv9lzf/osrD/Y0BwN1/rhn3lwNgikrzb6IY/LL0n2tMFGqNioD7
        i7/jkaoMAO11EFCUA4oswAYAGwAqGgCWLv3lmV76s7j5Z4n7SwCwxv3DzLi/n4n035T7z7LA/c2k/zXHRKLW6Aj6/2g4fTkUDzs+xSHwiBEE5FmArQywAeB+AEByf4vT/y
        TT6b+s+aft/omm3V8LAPKlP5b+h1ix9Cev/U01/6SlPx78JtJ/g6U/efDXHB3J3b8WZQH1usxG84avoGXVxwkAQiZg1A9QyQJsAWMDQPkDoLyX/nJMuH+mytKfWvMvyUzz
        z9LBn6BSLv3NMU7/jdx/mgVLfwr3FwFAL2sPDkLjR79Ay8rtOABsWYANAPcfAFq1v4Xpv8WTf2ruLwEgyYLav4IGf8wu/Unub2rwZ6KG+4vpPwfASPYyCi5vdsXDVR6TZQ
        EddFkA6wW0tWd7Bp61AcAGgAoEgLXuv8zK5l9WKdzf1NivqbX/sHJy/7llHPuVADDWIP03AED9nyeiec2n8XBVgoAiCzAuA2wAsAGgYgGgNfZb3nP/GVaO/cZbMPhjbvJP
        1f1jrXJ/kwCYpD34o3f/SH3wj2AvI1Gn1wI0dXsDD1MZ0NJeLQt4xjYT8P8EAM3sHuvQ/IEAgNVLf8mWLf2V2v0TLHZ/DoBgC5f+pPTf25rBn2jztb9W80/u/iME1R4SCr
        enf8LDldrgYQJAS1kzsLWuDLD1Af4/AKDpfQGARvpf2cqlP5Njv6bSf1Njv6Z2/UVJzb84y5b+/MWxX0uW/lTm/ks19qvW/BspA8Bw9jISTh/3Rwt71gdQLwO0hoJsgfMf
        uRaMfpcdqr1c8m21Hzt8Ve3b+wQAw8m/Aisn/8zt+pMP/mQYur8FzT8Lxn4tbv7JB3+8LVj6s2Tyz5rmnxT8XJGo98cMNKv3IpUBbQ0A8KRtOfD/gdrS7/TZ6m+UzKo3rc
        OEumPvIQAetF1/yaZ3/VWLtmDyzzD4LZ3799Se/NMMfq2lv/HGgz88+A0BMEwAQJ3efmj+8Od4pJK+DyBAwHQfwBY8/yUAvF4ys97UDuPqjrkPAKjIuX/D2t+aXX/mJv+s
        cP/qfmbGfg0n/2aVYu5fbexXSv0Nan8dAIaxeYAwNHmlG9pWfpq7fisx+BWNQFsZ8J/PAO4tAIrLee6/PNzfksEfc+4fbG7Pf4zR5J/e/WNMuL+puf9S1P7DRA0VIOD6xS
        i0c3wFj1V9jgc7S/2lWQAGAP0RYs/bsoD/KABm3BcAaLm/YfMvT939K9+PXX88+OOVnX8t9/fX6PxbsvQ3s/R7/o3SfzUAsOAfGk7/H4UGf85Ba7eP+LHhj9u/yE8NZoEv
        nwhUA4ANAv8tAIy9JwAoTi6d+y+zYPBnkeldf1Uo+CtR0D+UHA+7pDjYJZIS4vBQQjweiqeXcXGoxLr+8sk/c82/ECuW/jQHf2JKufQXpVn7q6b/w5UAqD0kHLWHRqJunw
        C0eOxnEQAU1PYvEQTY8tBzegDY6wGgbAbaIPCvXgLUAWAaAWDs/QOARUt/ltT+BoM/VSj4K6VT0KfGo0pqAlyz0vFk7mJ8saII3deXYNTWLZi8fTum7diBoZs2oV1WNqqa
        cn9Ldv0FWLn0Z82JP1Msdf9I7dqfuf8QEQDs5eBwNHqzD7nBC2hLZUA7ggATg0Bbygba8LLgGf7yUcoEBD0vyF5QW64XylHPP1iq9m/Xc0Ziwd+m2jN4qvprJdPrTW3PAJ
        DqlGSX0pAp2S65YYpdUsNUu0RSQsM0u/gGTOl20bXS7DKeXmq3Y94+fsnPjTM3LQTAchkArBn7XWblph9SZQr+yhkUyBlJaLEkE1+uLobHnp1Ycuoktl++hCPXruH8rVu4
        fucObt29i9v//IOr9O+Y/QfgRCVAlZg4s0d+VS+t+1uy9Gd204+VS38jVNyfiYK/FmUBbt9OwnP138PL5P4vV38Vr5Berf46XiO9Wv01+v9X8JrDy6RX8Lrjq1xvkN7keo
        3rLRW9rdPrqnrHrN6wSG+r6k2r9Jaq3tLUm2b1Nt50EPSGFXrd4R2L9ZpO72rqVU3R75s+9j3Hz7YsqO/1/iLn9EYEgBYEAFJyCwJACwJACwJACwJACwIAKb1FTJ30FimP
        5jTN+2513dX9Nlf5xK6X3dY5e+2OZJ2sIABYMfhTmZz/oYUJqEH/fqV4Gabs3o5NFy/g7K2buEGBbu6x4/RpNPX1Q5XIGAJAguV7/oNkY7+lcX9zS39TNU78maDh/qNNNf
        /0wc9UgwDQorMPhj7aDX4NfoKP0y/wc/4FAc6/Itj5N4Q5/45Il9+R7PYHstw7YjEpt9GfWNroL+STChv/jeWNO2Fl485YTVrTuAtKmnTFOtL6Jt2wgbSxSXdsatIDm0lb
        mvTi2kra3rQ3qQ/XjqZ9RfXDTqYm/bCjSX/SAPr/AfT6gVw7mw6il4K2Nx2M7U0GY1uTIaKGcm1tMoxrS5PhXJuZGo/g2tR4JNfGxqPoeY3ChsajsV7UusZjuEoaj8VaUW
        sajeNa3Wg8VnFNwMpGE7lWNJqEFe6TsNx9MopJRaRC9ylcBe5TUeA2Fflu05DHNR3LuGZgKSnXdSaWuHogh2sWFnPNxiJStsscZHHNRSZpoYsnMly8kOHshXRnb6RxzUMq
        KcV5PmkBkp0WIMnJB4mkBCdfrngnP8Q1ZPJHbMMAxJCiGwZyRTUMQmSDIHpd2K1Up4QTaU5Jh1IbJh2m4D+czJVymABwmIL/cEKDtMMU/IfjSLENMo7ENly4N7r+wkVpT+
        f1z3qzuAmFzkNnN12yO7TwpAUAUMz9J1u+62+J6bHfh9iSH9X+TxXkYPrendh/9QrukLMbPv6h110j9z915QoOXbiAPWfPYhcF/o5TpzAudylqTZ2GquFR5P4J2nP/IaYP
        /Sj1nv+ZFu75t2buf7i2+9ceFI6agyPQsG8Iur80FBFOv1Gw/4FoUpxrRyS6/olk17+Q6vo3Frp1Ql4jFuRdeHBvIG1u2g3bmnanYOyOXU17YG+zntjfrBcONuuNQ81743
        DzPjjavB+OkY4374+TzQfgFOl084GkQTjTYjDOks61GMJ1tsVQejkM55tLGo5zXCNII+ltI3G+xSh6yTSa62zzMVxnmo8ljRM1nj7/ePpaE3CqmaCTzSaSJuGEqOPNJuNY
        syk4ytR0Ko6IOtx0Gg41nY6DXDNwoAnTTOwn7Wvigb1cs7CHtLvJbOxuPAe7SDsbz8UO0vbGnlzbGnthayNvri2N5mEzaVOj+VwbGy3ABncfrCetc/cl+aGEtNbdH2vcmA
        KwmrTKLRAr3YKwgrTcLRjLXYNR7BqCItdQFJIKXMNI4ch3iUAeaZlLJJaScl2iuJY4RyPHOQaLnWOxiJTtHIcsJqd4ZDolYKFTIjJIaSRyf1Dwc1HwI4lE7g8CAMj9Sekg
        AIAAgJgGCxHdIPOfiFoLr0fVz8pc9OGKZ1m8s5LAGAArCQBrRQAUJZdp118VDQDYUfA3WZqJPls3oOTCedw1CHz2f0cuXULhwQMIKFmLEblL8GtSIj6IjMDLwSF4JjAQHf
        wCUGfGTFTxnAf7qDjLl/4sPfDTuxSDP9Ms2PQzTsX9zQFgsB4AtQeGw3FwJD79eCL8XX9HODl+hAwCCRwCf3MIZBAEct07YwU5fUmTLjoIbCUAbBchsJsAsI8AsJ90kABw
        uHlfHGnWjwJNgMBxAsBJroEUoINwugUDwRCu0/zlUAroYaKGc50hAJwhAJxpPoogIegMBf+Z5qMp0MeIGksaR5+TaTx9/vE4QQA40UzQcQIA0zEKfqajBACmI02nUNBPoa
        CfynWQAMB0gACwn7SPALCPgn+vqD0EgN0U/LuYGs+mwJ9NgT+Hgn4OBf1ckie2MjXyosD3osD3pqCfR0HPNB8bSOvdF1DQL6Cg9yH5UuAz+VHg+1Hg+1PgB1DgB1DgB3It
        JwAUU/AXkQoJAEwFBIB8AkCeSzgFfjgFfgRXLgFgCQV/jnMUBX40BX4MVzYBIIuU6RRHgR9Pgc+UgHQDACTrAJAqAEAW/HEU/LE8+En1M0HBj4g6mXcjG2ZnLP5k9cNLvy
        +x2+q933IAWOT+JsZ+Ky9iwZ+ADkVLEHX0EK7dvaMI/Ou3b2PlkcOYs3IFvo6LQZsF3hR0s2A3dzbs5nnDztcXDwUE4qGgYFIIqoSEUfDHqjT/rNjzLz/w09Lmn4eFS39l
        rP0VABikBMCTv3piVrO/Eer0K0JdhLRfngmkUCaQLkIgx60zljcSMoGNlOJvoeAXMoEe2EVZwB6CwF7SAQLAQQLAIQp+DgECwDEKfgaBEwSAkwSAU80H88AXNFQQBf8ZXf
        BLABjBAXBGFvxyAJwiAJwyBIBB8B83DP5mQvAfVgl+DgAe/DMMgt9DCH6SFPwCAOYqACAFvxwAUvCrAWCNuxT8/jz4JQDw4CcVcQDIg98YAELwSwCI1gFACn7m/pkU+At1
        wZ/Agz+NB78eAHr3lwMgQ+7+FPwiAOplI7xO9pUo95whrBTYOs8SAJTD4A8L/qrk/u+sLkTumVOQe/5tqvfXHDuKvtlZaO8zD7XmelDAz8VDwQGoHBMJ+4R42CcmCkoQFZ
        8oLAGy2r+ilv48LWz+Tbd+6a+W1tLfMNPuz1RzUATcuwdi2GO9ENzwF4Sw2l+EQIzrH4gnCCQxCLgRBNyEcmAxQaCYILCeILCpqQSBHlSj9+RZAIPA/mZ9cKBZXyoJ+uJw
        s/4UcP0JAgMIAgN1ADhJADjVgkkEQHMBAKcNgv801yguvfuPFoNfAoAAAaX7T1R1/yMy95cD4ICR+8/QBb8CADL31wNAH/x695cAMN8AAMrg1wNAHvzG7l/AU38x+Emq7u
        8iub8SAJnOcYrgV3P/JLn7N1Sm/sz99QDIQiQDQP1shNbKQnTjJekH0064b5q5VwMAxUoAGJ33l2/50h8LfnvS9+tXYuvliwrXP331KuauXon2vvNh7zEddv4LUDk2ClWT
        4vkAkH1yIp/8q5pAik/QL/upzf1beuJPgMHgjyVXfc3RuOrLmqW/cbKxX1ODP8O03b/2gHDUEvXra8Ph3/BnBDlTFkAQCCcARJFipVLA7U+kuQlZQCYBgEGgkGUCjaVSoA
        eVAj2xsxmDQG/KAnpzCBzkEOgnQIBnAQMpC2AQGEQajBMiAE4RAE41ZxIAcFoEgBD8IwUAiBnAaSP3l4JfCwDG7q9M/w3cv6m6++/WcP/tRu6vB4Dk/hvc9ek/A4Au9ReD
        Xw8AfeqvBgC9+4cp3N8YAAbubwCANAkAOvfX1/6JGu7PxNw/kgMgWwBAzSzEtVm2alfYkfabPPaZAUBZ9vyz4OcASMSnJcXYceWSIvi3nj6J7ynQa8ycikoLPFE5Lko/+p
        ucgMr0tkp88CcelePj+XbfaiS27Gd26c8QAIFW7PqzZPDHkl1/1rq/ieaf5P4MALX7UxkwMBKvfzUNnq6/wd/pFwQ5CRDgWYBYChhmAVkEgBz3zsgnCKylUmCzmAXs4BDo
        JUCgKYNAX8oE+gmlQHN9FnBcBMDJFkxDuCQACBAYYQCAkWLgm3P/8Qapv/Xuv1/D/Xdruv9cI/ffLAa/uvv7qLi/v0rtLwHA2P3zjNxfn/4b1f7O5mt/0+6vTP8l92cKqZ
        mN+Lb5JbuCjzyzaaYpABQml635R8FvR8H/9PJcrLxwVhf4rOlXfPgQ3goLQpVZU1E5PIiCPkEY/SXXr8Sm/xLjUJP+3TJrIV7KzcU3y5ej7/r1GLB+Az7JL0CdhGRULcOu
        P4uv+jI5918K9ze3689E7S8HQM0BEWj51wJMbP43fCkL4BCgTIA1BXkW4CKUAsmuLAv4i7KAvwkAnbBIhEBeo65YzSHQgyDQEzsIALso+Pc0ZZlAX4JAPzELGEAQGEAQGE
        gQGEQQGMx1ggGAgv+kGPynyP1PyYL/lJgBnKLA10se/GN58LMMgAHguAoALHd/ZfNPDwC9++804f5b1Ny/kdL9lbW/WvpfHu5vTfpvmftHG7h/eP1FHABxj1oBgMql3PNf
        iYK/eX4WUk4eg3xVP//QAbwY7IfKs6eTk4ehakYSD/4q5Px25Pr10lPwQVE+pu7YjoLTp3GAyoQLt27pPv7glSv4fMlSVI6Igb0l7m/piT+W3PRr6P5T1ef+HSdEoPq4cN
        iPCUe10eTYYyLU039Lmn8Gwc9Ui9SwVzC6P9UXPg1/gh8BIJAAwPoBEc6sIfg7ZQF/IJEgkEKlAMsCMt07IZuVAgSAXPcuyHPvilWNu3MIbG/aCzubihDgABCygINUBhzi
        EBhIEBjEIXCMQ2AIBa4AgJMyAAgaKWqUQfCPofdVpv+C++sBcEwX/Kbd/6Cl7t9E2/23Gri/evPPtPuv1HB/eedfq/knBb/55p88+JM0l/4scX85AHZaBoDSDf5UXpyIWj
        kpmLl/F5/ckx6s2fd6WCAqz5mGKvERQvCnJ6Iy2+xDjv9a/lL47d/H5wK0Hnfv3kWvrEWo7BukDgC1XX8BpWn+RVs0+edIwe8wKRL2FPRVx4ejKgV+HSoBGs+Ix2NzU9DS
        Iwl1x0eh2khyblPub6r5N0AJgNr9wlCrXzg+fW8svJx+go/zT/B3/gXBzspSIF5cFUilLGAhywIIAosIAEsIALkEgLxG3bCicQ9sbNJThADLAvpgn5gFHCAAHCQAHCYAHJ
        EF//FmTEMpgIeJEBhOGiFKAMBJAoCgMTKNFaWs/RkAVINfB4CpiqU/w9rfMvefY9T5N3R/U0t/PPhJqyx0/wKNzr/a0l+2xtKf3v2TZLV/ssz90zQ6//qlP7n7GwJgozkA
        WL30JwLALjsBX6xbjkPXr+kC99DFi1Tzx6HS7GlU74frg5/Sf6fMNPTbtIFPAv6jMgx0684dnLt2DccuXUL69u14xnsBKnn7UPqfoJ/7l077tWbpb751u/4cKfgdKfgdpk
        Wi2hQK+skR3P1dZsbjiQVp+CxyKQZmr4XP6p1YuPMIVh0+jYL9J+Czaic+CFyM2mwJkECgtevPqPZXAwAFPwcA/bvDj7Mx1f1XzHf6Eb7OP1MW8AufCAwn6bMAfS8g000A
        QI4IgGUEgPxG3VHcqAfWi9N+uwkALAvYSwAQSoEBlAUMJAgMoixgMAXpEK7jzYfyLOCEAgAjRVHgN2PBP1oj+Jn7jzNy/6MyABy10v2N1v2bKNf9tZb+Nhm4/wYV9zdc+l
        tl5P7BKkt/+tRfzf3Vlv4yufsbN/9SFQCwaPDHqPnHgj+s/mIE11hkAgArRAAUGQDAiqu+HiL3d1qajoQTR3RBfIVS+LEFeajmMQWVIqnmXygEf6XUeDTKSsfcvbv5vL/8
        cfnmTaw+cgSeK1aic2oqPg6PwIv+AWjsMRvVpnnAPijc8sEff8vm/uV3/TkQABxmRaG6B7n3TBJzfQJA/VmxaO6VhNdDF6FTxgrMWrkNaTsPY/vpCwSoa7h04xZf2pQ/7t
        z9B1tPnMdvMfnc/WuOsKL5p+L+dfoSBEiNuvhjUOsumNfgBywgALCxYGlVIJIUSxCQlgXTxGZgtpQFNBIAkOfeHQXuDAI9sa5xL+wgAOxuKgBgH88CBhAEBuIQAYBDoJkA
        AZYBCBAYLmoESR0AJ1QBYJj+T1IAgLn/YVX3nyYEv4r779Yc/FEG/9bGaoM/82Tu72M0+LPa3V9R+68s98GfOI3BH33wpyjcXzn4o+7+WbLgJ9UTABBrEgBrBABUKoP7f7
        dhJY7duK4LgJz9e9Fs3mw8FDCPnD+Bb/utlMZ2/KVR8O/CtTv6oSC24YcF/t8pKWgyezal2zNQdfZcVJrvg0p+AagSGAr78OhyveuvOgV+Nc9o2FPQV50dhaoU+A7k/nXn
        xqLR/CQ8E5KJb5IKMK5wEyI270PJ8bM4cukqge02bt65azTNqPVYd+gkXpmbDIdhYap7/i1N/xkAmOr3CsGPLw+Bd/3v4M1LgZ8RwLIAl18R7sKygN8Q5yL1AlSyAAYB92
        4ooCygkLIABoG1jXtjW5O+lAUwAPSnLGAAhwAHQDOmwRScLAsYynVcAYARmhnACQp+QeMMmn8TjNyfB38z5dTfIavcX0r/Z1vs/hvFpb91KgAwdv8ARe2vNvgjrfvr3T9C
        w/1jrHD/ZIX7J5hwfz0AFuncP5QAEFRjMQGgQATAfjUA5IgASLIMAAbu35DcP+LYId0f/ZlrV/ET29vvSYGcEsPdv3J6AmovTMbYHdv4zj75XIDPmjV4fJ43Kk+lUsHXD1
        Ujo8W1/wRx6S+hzGO/1Sn4q86LRmWvKFT2jII9BX+deXFo4peMZyMy8VnyMgxYthbe63Zg6cHj2H3uEi7dvMXhxILdsnA3ftyhjx+RuIwCPQg1zY39DjJu/vHglwDQhyDS
        NxyvfzoJHs4/wqvhj1QK/KTPAlxYL0DIAhJkKwJCFiAAYIm7AIB8EQBFBIDljXpjdeM+2CqDgC4LEAFwmABwlIsgQCWABIDjYhZwgoL/RLPRggyCn+m4zv3HywAgQOCIwv
        2nGLm/svaXAOBhYuxX2fk3dP9NRu6v7PyvVRn8scb9l2m4vzL916/7L9RY+ksxHPttaDj2myG4fwMN9+cAyJEB4KiFALCi+We3KAHvrS3EvmtXdPV7NqX3Tt4elPoH8g1A
        zP0rk/t/uXo5TsiyhLNU4w/JWYy606ai8tw5FPiRwsRffBnv+qPgr+obg8o+0ZRFkCjwGQBcAxLxZFQmPk5dhh5L12BWyXak7TmCjafO4+TV6xa7urWP4GVr4DxgPmrwU3
        7Ud/2p1/5K95cA0OZXL4xq9gc8G7As4Ef4OAlZQIiUBbiyMuAPXgakuopLggSARQQAqRfAygAJAMUEgBUiBDYTBHbrsoCBBIFBBAEBAExHmg3jAGBZwHERAMcNAHC8mR4A
        x3nwCwDQB78+Azhi4P6HVdxfPva7z8LBn22agz9qc/8+Fo/9LnfTdn+15l+OSvPP2P3jrXT/DBPurwcAc38GgEACQAwBYIcaAOotz7B7hJcAKQIArBj8qUTuXyMnGRP3bt
        c55JVbN/FnRgoqEQCqpMaJ7p8IJ0r9008cU+wDmFZUiDoU/JXmz4N9XJww+htv+V1/UvOvGgV+VQr8Sv7ReMiXHN6XUvmgBDwSmY53UnLRMXclJq/dgrhdB7Hy+BkcuHgF
        VymVv1ePEAaA3nNQY0iY1c0/uftzAPQJh2uXAHR5oifm1vsWnk5CFuBPpUAQASCMFOVKWQABgGUBfI+A1AvgS4JiM5CXARIAemEFlQErCQAMAhub9MOupgPIdQcSBAYRBA
        YTBBgAhlKwDsVRAsBRAsAxCv5jFPzHmzGNVgDguOj+2sFvjftPN3B/7U0/pgZ/TI/9Wj74o3f/UJXmX4Tq0l95Dv7EmVn6k7t/iA4AhQSAI89sMARA/eXpdq1WL5rgUGgh
        AOTp/6JEuC/LQKKs+bf9zGk87OOFSmH+Cvf/tWS1oum3aM8etPKkOt/Lk4I+Tpz3N3Hev3jenz0Fvz0Ff9WQWFQJorQ+MAa1QhLgFpmC55MX4cclxRi7ZjOidh9A0fFT2H
        3hEk5fv8FT+fv10ATAYBNr/yruX6d3GGoTAOr2CsWnb47ErPrfYm7DH8Qs4CeeBYQSACJIQhbQEUluwpJghjgZuNitq74McJf6AL2wXAaANY37YkPj/tjZhEFACQABAsMV
        ADgmAuC4FPxcevc/RgA41my8GPxy95cAMEUBAONNP9ONgt946U+562+r6tLfPNXBH+POv5/FY7/qS38RViz9GQz+qI79ai/96Tv/xu4fogNAjmkAPCIBID/J6sGfmjkpGL
        dnG+/6M/efVFQAxznTUSU5mrs/O/LLNTsd2SdP6LKEo5cv4QcK+sozZ6BqTLTe+TXcv2pkHKpQ4FcNi4UjgaBhVBLaJlLdvjgf/VeuR/DOfcg9egIHLl/hwX719p0KS+dL
        DYBeBIDBYWbn/g2X/uTuzwDAxLKA576cjvFuP2FO/e/g2fBHLJCyAIKAkAX8LmwXduvIASA0A6kMcBP6AEtJ6gDoywGwtnE/rCcIbG8ykMqBwSIEGACGcQAcac4gMIIHvw
        SAY3IAyNJ/IfjVAMBSf2XzTwp+4+afFPzK9H9nY2sHf8rL/ZWDP8tMjP2qDf5YtuU3xQL3zzIa/JG7f3C9JQggAESXGgAm5v6ZKi1KgnNuBr4vWoq/stLh5j0LlWSdf3be
        3xNLF2OPbNAnctMmNKDgrxwYIO74U7/rjw39VKHgrxeThFeylqBj0Sp4bN2B5INHsOviJZy7cZMfG3a7DE26ewKApTIAWOL+/dXdn6tXGGr2iUDL3+ahf8u/MLveN5gjZg
        G+BIFAgkCIM2UBVAbEiGVAMgFA1wwkAORQFiCUAWw5kJUBAgBWEABWiRnAWlIJAWBd4wHY2mQQ9lEGIGQBegAcIQAcpeAXNEoBgGPc+ZXBf1SniTr3P2yQ/h8s09ivAICt
        ZRn7Lc2uP62x3zK6f5LG2K/xrj/l4E9ofb37KwFw1AoAWHHkV+VsUlosKkcGoHJ4ANX+sboTf1n635Dq/5BD+3GCILD5xAl8GxsDu9mzhNRfCwDRAgBeyc5F0O595O5X+b
        Fhdx/wYLcKAIO00n9t92cAqNU7HE5dgvDLs/3gQQCY3fB7viLgI2YBwZQFsGaglAUkyssA9y5iGdBNVgb0FAHQmwDQl7KAfjwDYABYTwBY33ggNhME9hAADhIADjcTgv9I
        MyYh+I9wsSxgjKixOgAc5ZIH/0RF8++QLviVtf8Bo7Ffwz3/lri/SvpvgfsbDf64WTf2a3rPf5zqnn8GgGSrxn613H+xLviZ/GssIQAUiQA4YACAYgMAlOWyj+xkepmsq/
        2ls/4rpyaiWfZC/JCbg3ciw1GT3L9KcJDJ4K8SFYeXspdg9ZmzD2RQX6Hs4+TFyxaVGhwAPQkAg0LN7vpTdX8DANRmEOgVjnffHYvJTt9jVgNWBvwgLAk66cuASFf9VmGh
        GShsE17ElwO7URmgB4DUCGQAWNWIyoBGMgBQGbCeALCpyWDsajqUMoHhFLgjuPQAGM11VAp+0tFm4wyCXwkAyf21AGC49LfH5NKf+bHfDYr036cC3T/SIvdXDP44mXN/ad
        1fvvSXKav9jZt/LPiD6uVyAES1KSrZHnL0mfWqAFhFACggAOQlle7ATwsu+6iUnAC7mEjYBflTPR8iHvqhftlH1eg41I9LRsje/Q9EsN+9+w/OUAay+fBxZKzfjnmLi9Ej
        JBmDozNx4ep16wFgae2vkv5z9aQyoHcE2n8zC8Mb/ypmAfJm4M98STCCsgC2P0CYCWBlQCcqAzojmy8HUhkgTQWKAGDzACsb9cFqCv41FPxSCcAyAAaBDQSATU2GYGfTYZ
        QJjBAhMFLn/oLGUJCPFaUEwBFd8OvT/0MmOv9agz+GS3/Ggz9e2mO/Fiz9qQ3+FFu49FfeY7+mlv7Uxn7l7h8kB8CjpQWAqcs+sk1f9qG87UfY/FOVH/iRaHzVl+yq76rk
        /o+lZ2Hf5Sv3NtDJzdlIL3P3A2fOIW/7XvguW4lBMQvxlWc4nhs7D80GeqBenxmw7zUTb86Nw+krlgOgpiEABmos/Zlwfxb8EgAad/RF10e7YGa9rzGrodAMFAaDfhKzgN
        /4VuF4FzYa/BeVAeywEAEAiyUANNIDYIUOAH0VAJAgsJEAsJEAsKnJUGxvMoyC1BgAhwkAAgTGcR0hABxRBP9Eep9JYvBPVrj/AYOx3/0Wu7/pAz/MDf6obfnVGvxR2/Kr
        BEDFDP4oxn6N3H+RBgByEUjyq5GLyDbFVgKgPK76Ste46ivJ9E2/rPH3+bJCXDTYK1Dewc42HEmp/MbDxxC9agPGpy7BDz5ReH7ifDQeNAP1+k2DQz8P2A+Yi2pDF6D6iA
        A4jA5F9THh+CA4B2eu3rQQALNRc2Co6bn/fuH6dX9DAPRSAqA2e9k9FF+/OBDTGnwDDyoD+JKg2AtgzUBhMpCdFSCWAeJpQVkiAHgfgACQTwAoaixlAL0FADRiAJCCfwA2
        yACwsTFBoLFwvPcegsAhHQDGiAAQ3P9I03Fi8E8wCH45AKTUf6qq+5sa/Nlp5rw/w7Hf9WVy/2Dl1J9J948qw9hvqhn3Xygb/BFSf8PBnxCZ+3MAOJoCQJEBAEp7289CMz
        f9mrroMy7eCAC/Fq1UjAyX9cGc/fKNGzh24RJ2njiFjE3b4ZlbjK4RKXjTIxAtR85G/YEU7AOmw37QLFQbMR8OYwPhMCEMjhMjUYMd+MFP/Ynih344jI0oBQBCzLt/P/Pu
        z9UjFDV6ReCVDydgvMsPmNngW7EZ+AMWUCkQIK0GSNuEXfRlQKZbF30fgAOgBwGgp6wEEHoAaxvJ3L/xIAr+QRwCLPiZNjcWzvbf3XQkQUBwf0FjqbYXAHDYIAPQAcDA/Q
        1r//0WDv4Ypf+NtNN/pfv7qLh/WXf9RWns+ZfcP86k+xsP/ugBEKPo/GdqjP0au39gvaXwNQuAlQSAfALAsiSTgz8ma/9MC276TTJx068EALb0FxGHXwpX4Ort0k/r3aCP
        ZcG+9dgJLNq6E36FqzAoMQufzQ/Hk1Pmw2n4DDgOnoEqg8ndR3ij+jhy9kmhcJwaKZ76E40aU6LVD/wcRwAYYw0AVsO5h5gBlKb5Z+D+dXoIAKjZMxyP/OSFQc1+w3SWBT
        RkWcD3fKuwrhnozMqAPxDnIp0erC8DcggAue7dqQzooW8ENurDG4ESAIQsYCBBYJC+BBCDn4tf7jECOwgCB3QQGMcBcLjpePr3BFETRU0iWOjT/9K4v6ldf2oHfmwsbe2v
        0fzLUxn8WaKx9Gf52K/W4I9lu/707q8EQEBdAQARjxaXbAs59sy60gDA1G0/Ft30a4X7lxYA569dx66TZ7Bs116ErCzB8LRF+C4wGq/M9kfjsRR8I2ai0lDSyLmoMt4H1a
        YEw2FGhHDkl0csasyMEQ7+UDvvT+XIr3IBwADzk3/qAAjlqk0gcO4UiD8f745p9b/GzIbfYo7T95jHZwKEswJCKQuIdJaVAQSAdD4VyMoAPQBYH4DPA4gAEJYCJQAMohKA
        ANB4sC79F9x/GA9+ps2k7U1GYV/TMRTYY4Xg55pA/0/SBb8eAAdFABywZuzXVPPPzNjvOqM9/+YGf4LV5/5NuL9W82+hiRN/TC39mR771Xb/INH9A+oug4/jUgLAchEAB8
        0AYKkFd/0Z3vSr5v5pKu6fqOH+MQYACCcAFFgGgE3k8BMXLcMvYfF4yzsYrad4ocHYWag6ygMPjfVEpYkLUHVqIKp7hMNxTpR48AdpNskjxvjEn6lqx33LrvqSADCaABBk
        BQC6EwAGhJp2f0vSf9H9Wf3PxCDw8WvDMKnB15hBAJhNAPAiACxwFkeDWRngzMqAjogXLxFJpwxAKANYH6A7lhkAYKUIgDW8CTiQlwASADYRADYTALYwADQeLgPASK5tBI
        HdTRgEhAzgkAwAhyQAyIJfv99/miL4Tdf+sy0470977Ld8T/yxYM+/6nl/SUZz/4kmjvvmtX8DbfcP0XB/CQDhpQWAIvjNuX+6dvqvcH8dANQ3/VgDAL/la+A0ZgYqj5+L
        SlN9UHlmIOznhKG6V5R46EcsHJnkp/7MMnHkl+GBnxqXfZQZAPLg72+h+/fQuz8L/rqkWj2opPlyOka5fo+prAxw+g6eTj/wMkA6MiyCnxbE+gACANIIAAsJANkiAJa690
        C+e08qA4RG4IpGegCUNNEvATL3V2QABICtIgC2UPBvoeDf0ngUBeMo7CQIHJAA0EwOALn7T6H3maoKALXjvk2O/cq6/4bHfW1QOet/rbnz/tzUx371e/4jLDrvz9j9E8o8
        9htpZvBH7/5L4W8WAIUiAPIIAEuTFM2/KmoAyDZ2/yqa7p9ovvaPKRsAfIpWod5kb9jPj5Kd+hNr3W0/Jm/6NXZ/JQBuWA6A/qEW7fk36/4SALqxl2Fo/Lsv+jz8B6Y1YG
        XAd7oywM9ZuENAWA5kfYA/heVA1068D5Dl1pX3ARgA8ggABQSAYrERuEoGgHUyALA5gM0yAGxpbBz8WxqP5treeCwFM4PARAp2ks79J8uCf6ou+C07708PAOsGf4zdX+2y
        jxVWDP4oAOBsbtdfgmW7/hpoHPdd33jyT732X6Jzfxb8TAsclyG8zQoTAFihAgBrav+FBu6fpuH+ierur9v1J275rRIWazEAfItXo/4Ub1SbH236wE/D8/603H+qeffnAB
        gVgQ8DF1sPgDLV/sr0nwOgWxgadA7G98/0xdT6/+NlwKyG3wtDQeKZgcJy4B9iH+AvpBAA0kUALHZjfYAeWMYA4N5LPBtAKAMEAAzgAFivA8BQrs1NJAAIZcBWHQBGk8Zw
        bSFtazyOAngCBboAABb8B3jwTxGDf6oY/Oruv8eS034NALDRxODPWs09/2q7/kI00v8Ilbl/pftnyzb9GKb/5i76jCvl4I/k/gEy9/erm8cBEEYA2EoAKLEKAKbS/0wzzT
        8JAJZ0/g32/FsNAMoAqs2Lsu6uPw8rLvtQueuvzAAoq/uLAGCqTVnAa++Pw4SG32BaQzYV+B28nH7gfQB/8biwCH6XoNAHMAZAdx0AiggA+j5Af94IFPoAUgnAADBMAEAT
        fQ+AAcAw+LdSBrCVAMC0gzKBfU0nqbi/AIB9TS3f8mvuqq+Npbzq6364v1b6bzz2azz3H1rfvPszAMy3GABs4s/SuX+5+5el+ady2UeV0FICoCw3/Zpq/qlc9uE4MrzsAC
        il+8uDX8oCWn/viaGNfhZXA77DXCdxb4BYBvBrxFw7Is5V3wfgjUACwBI31gfoiXz33noANDIEAFsJkANguA4AW2UA2MZdXwj+LSIAtojaRpnAXoKA0v2nCcFvwVVfO026
        v7eJ5p81p/2qT/7lWXHctyWbfrTGfuNMnviTrdH8W2LU/BOCXwJAngCA4OMqACgQAbBMBgBzgz+ZFiz9pWgs/Zlo/kk3/XIA5FsIgCILAGCt+2sBQHbTLwdAgBUA6EYA6B
        eqPfjTV2Xd33Dpr7sKALrS60iufwagU9sumFaPlQHf6fcG8DLgV36ZKANALAEg0Y2NBbM+AFsJ6IYcHQAoA3Dvw5uAKxv1wyoRACU6AAzmANjcRL/+LwT/SAEATQgATcZw
        yd1/a+PxFKCkxhMocCdSQE+mgDcAAA/+GRYd+FGWpT/lph9/k5t+1AZ/lmoM/lhz4o+0609t8Ee+7h9jduw3R3XwR57++0oAaL1SAMCMQwYAyCcALJcDwJKlPw33T7Ww+R
        enctGn7MgvqwEwiQDgHSUE/zzD474tvOnXqqu+ygiAUg7+6IJfBQD1uoTgk1eGYkq9r6gMEPoAfDlQPCkohPUBXP9AjAiAFBEAWQwAVALkEgCWEQAK3VkGIACAZwBsGKiR
        EgCbGssBIAW/HABjudQAsIUAwLSj8WQK9qkU/AIA9qps+RXu+ZPW/WdbfN6fEPymB39WWzj4Y/6a7yirt/yavurLePBHCH7j476NO/+5RsHvWzcf8wgAoW0YAKgEmHHQDA
        CsmftfaNr9q6q6f4K6+0cKR36xs/6qhFgDgFVKAKi5vwSAsrq/7K4/xxFlBIDa3L+p9F/D/SXVpjLguU+nYozz95hen00Ffk9lgHReoHB5SAQBIJoAEO/2N5Ld2DxAFyoD
        umKxCIA8AkABAaCYALBCzADWSABoNAjr3QgCbgQB9yHY7D4UW9yHYav7cBKBwH0k1zZ3AoE7gYC01Z0g4E4AcBO0xW0812a3CaSJ9LpJ2O0+BXvdp2GPG9N07HabwbXLbS
        bXTjcP7HT1wHbXWVzbXGdjK9ccbHGdi80uc7HJxZNro4sXNrh4Yz1pncs8rHOejxLSWucFXGucfbCatMrJFyud/LhWOPljuVMAiklFDQNRyBWEgobByHcOMTv2a27XX7qK
        +6tv+hEAYLn7aw3+yAGQDx8OgHyEihnAWpMZgBT85tJ/tbn/VI3JvwR191ec92dw3Lf1APBCNa9IvftbctWXHABTNCb/VG76rSne9FsqAPQNNb/rr5f63L8lAHj453no26
        IjZtSTlgN/xDzdseHsOvE/xD7AX0jS9QG68jJgiZgBKADQSADAWtcBHACbnqCAfm82tn0+D9tJO75YgF1f+GD3F7700he7v/TDnq/8sefLAFIg/TtI0JfsZTAphN5H1Feh
        2PVlKL0tDAf/F4mj38bg6PexOPpdHI58zxSPw99JSsAh0kGuRBxg+jYJ+7mSsU/U3m9TsIcrFbu/YUrj2vVNOnaSdnybgR3fZGD710wLse2bhdj6TSa2fp2JLV9nYfPX2d
        j8TTY2fp6J1c8loLBxOPIahiDPOczisV/ta74tH/xR7vrTPu7bKP0XB3/8DADgLQJgi0kALJUBoJR7/s3X/irurwaAYAJAnoUAKCQATBQBYMr9Z5XB/VWu+nYcTgDwtxAA
        uQSAriIArBn8MdX866oUawQ6/xWIn5/qg+l1WR/gewKAeFQY7wP8hlBdH+AvJBIAUgkAGXwgiAGgB5Y16oX8RqwR2IfKgH5UBvTHatf+KGk7Elv+pgAOL8SB4u04tG4vjm
        zYh6MbD+D45kM4wbTlEE5uO4JT24/hNOkM6eyO4zi784SgXSdwbpf08qRC53efwpVD53Hr5BXcOXUVd05f5S9vl0nXRAn/vmVGN0XdOn0dN49dxYUVJ3DQYyPWvJ6GZQ3D
        sNRZzf2jLNjzr3XRp9auv4WqS3/hKs2/YMWuP+Pa30cCgEM+Qlqv0sgA8ggAxYt1ADBZ+2epjP1aM/dvyXHfoeUMAFM3/Wot/Zlo/kk3/ZYJAOb2/Pcyv/RnDADh5btvjc
        akBt9gegOhEeglNgID2E3C0jwAlQEJDACuegDkEACWNuqpA0Bx435Y4dIXa1+kVH1WJvau3YUD+4/i0IFjOHzwOI4ePonjR0/j5LEzOHX8LE6fOIczJ8/j7OmLOHdG0Pmz
        l3Dh3GVcOH8ZF3W6gksXRJ0XJLzuKq7Rz/LOnft3erPh45+7/+D88hNY/12OEPzOMvc3aP4Zu78AgDSNwR9T7h9jwv1NbfoJkC39Se6/oG6BpQBIvlt5SaL6oR9ZFbT0p3
        HVV5WgUgDAM1IIfi+D4Ldk6U9r0894dfcvNwD0sX7wRwsATLW6h6HDVzMxrNHPVAYIfQBP3gj8WdcIjCAAxLgIAEgRAcAagYsJALwPwAHQF8td+2D102OwxW8J9mzbj707
        D+IA6dDuIziy7yiOHjiOYwdP4ASB4CSB4PTRMzhz/BzOEQTOn7yA86dIBIMLZy7pdJHp7GVcIihcopfs/y+dEf4t6coFcuEb9Ht/gA5+vLT1LNZQabCkYbjuso/SjP2avu
        iz9IM/au4vAKAAC+oUwMuhAMGmAUAlQC4BICdRe+y3It0/QnnZBwPAz8usBMDcSNM3/UoAmK7u/jWtcH92y6/VAOhCAOgTqlz7Lyf31wMgHE1/XYCubTpjOu8DfI+5/JQg
        donor7wPEEYAiCYAxFMZkEwASGONQALAIhEArAwocKcMoEl/rB8chR3rd2E3A8D2A9hPADi4+zAHwHEp+I+c4gA4dUwAwNlTBIDTF3CBBT/pohT8LOApI+DBf+4KLit0GZ
        cpC2Dib6Ns4Ma1m9yBH5THsaUHkPMEOX+DCAJAtDoAnDSW/hpqDP6ojP2y4Dce+9UHv1XuX8cQACcIAIcNAZCmDgBL9/ynWbHnXz73b+KiT6sBMEEAgKOXbOzX1NKfJXP/
        Rp1/AwAMKy0ArKz9exgP/mgBQBoL/vr5gZha/2vMoDKAHRnOGoG+Tr/wPkCY8+8iAP5GkmsnDoCFBIBs9+5CI5ABwKUXVrw6AZuSlmPntn3YvWUfAWC/DgCHCQCS+59iwU
        /uf/rYWZylMuAsywAo8M+Lwc+CXuf6OrFgvyrTFZn0r79+5TruPiAlwbVzV7G85yJk1A3CEoOLPvU3/WqN/SZZ7f5RJtxfCYClRpN/cvefX6dQBMBq3gRcY5wBpNm1Ks7W
        A8DaXX8SAJJV5v5V9vxbctdflcBSAqCU7m9Z7R+pC352y69VAFgiASCkVOm/Je4vzwJe+2A8xjr/wPsAs3gf4CcqA36hMuA3hBAAIgkAsS6sEdiJNwIXunXlAOB9APdeyH
        ftjVV/+GDL8i3YsXkPdm/dxzOAA7uoBNgjAODoweM4oXD/szhDADh36ryY+l+UOT5zeDH4KcivkMNL4sEu+3/l267g2qXruH3rzn0vCe7evYut4euQ2MgfixtGUfBbmP5b
        4f4xpR77NW7+Se7PAODpUIggDoATBACDDKABAaA1AcCRAWBxYin3/Cear/1NNf/E4OcACC4lAOZEmm7+zSjdrj/d0p/M/TkAhhIA/KwBwCzU7B1S5rl/SwDQ5ru5GNDsd8
        yo/y08CADC/YG/wJ/KgGACQLhLR8QQABIIAEIfQAYA157Ib9YPa0bFYsv6ndi+cbeYATAAHMKhvUdwZD/LAI7r638GgBNnufufo/T/PAX/hbOX9ABgzn5B0JULIgAuXsXV
        i9e4DAP/6oVrXBIgrlwU+gL/3Ofbng7m70H84/7IrBdOGUCsgftr7PnXOPEnXmPwx9RFn6Vxf/MAWEYZQBEBYIkBACxp/lk0+BOvvvRn4qbfKgEEgKVWAGC8HAAa7m9qz7
        /ZpT9l8JcKAJ0ZAILLPPdvDgBsY5Drn/7487EemFb/G6EPwOcBfoYv7wP8TmVARyoD/uJlgNAH6IosAsBiKgFyGQBaDsTaqcnYsmEXtpN2URYglQAsA5AAcJwD4BROHz+N
        MyfFBuCpC0LTTwaAy+el4L9qFPwmJQPBFbEvcD9LguMbjiD+5SCk1QkWAWDJiT/mN/2YuujT1HHfARpLfwtkAJhXp8hKAKiN/Zo69MMS948x4/6qAFhuHQBmR5pf+lPZ9G
        Nu15+8888BMLIsAAjRHvzppdH5t8L95b2Az14ZhslUAsxo8D1mN/yRyoCf4eP0KwIJAKEEgCgqA+IoA0gSAbDQrTsWEQCWEADyWg7AmilJPAPYRgDYyQCwbT9fBTi4+xCV
        AEeEDOCIAIBTxykDOHlWTP8v6NL/i2Lqz4OfB72kazy1l3T1kj7o5a/nb5PBgEHg+uXruHP7zn0BwImNRxHzSgCS6gQSAOI0B3+0LvswXvrL0Bj8yTYa+w01ueknz6j5Jw
        S/AIC5BIDA1musAEC2NXP/Ku6vcdy3AgDhKu7PFMQAEFNKABiP/Tpa6/4iAGpQ8Neg4K9BgV9jZLigEaThJKr/HYYQAHwtB4ALAaCWKQCYW/rrarlYGfDsZ9Mw2vUnKgNY
        H4AA0PBnLCAABBAAQpz/EPsAfyORAJDq1oXKgO7IduuBHJceWEYAWM0BsAPbCAI7Nu3hS4FSD+DI/mM4fuiEAQDkGcBFWe0vNPd4as8C/RILciHQr1++odM1Cmwm+ev461
        VAwD7H/SgJjhMAol/25wBYJAKgNGO/CYqrvrTGfs0N/uRqDv7I3d+bA6CIA2AzAWC1EQCWigDIIQAsSiz9rr9Ejcm/GAtrf9H9OQD8CQC5VgBgnAiA0g7+iO5fY1wEP+yz
        2qhwvt+/ztgoOI2PhsuEGLhNiIXb+Fg4j4lGg1FRHADvLlhkHQB6hVh25FcZ3F+aB2j2y3z0a9ER0wkAvA9AAJhPAPBz/g3BlAFEEABiCAAJBIAUygAYALIIAItdegoAmJ
        yIzSIAdooAkJcAEgCYTvES4CxfAmTBL28AXhHdXwIAG/a5Tqn8dfbyijLY+f+LMoSBAgKibl67xW9turcACECiDgBaY7+WDP5kWDT4Y2rXn7/K2C9L/yX39+YAKMYcswAo
        lAGgAtzf3sTYr8L9GQACSwmAWZEmxn6jTbp/jQkU9GPJ2cn53afG4cUFC/F7XBEmLd2EgNU7EV6yB7Eb9yN2w34Ert6FeUXb0SNxJUYsXIcL129ZBoBOIgCscf9SAoDtDm
        zQKRi/PNkH0+p/i5kNfsDchj9hntMv8OUA+APhBIBoAkA8bwR2QbpbNw6ARWIGsGpyAjat246t63aIANiHfTsO4KAIgKNSCXDsNE6f0JcAuvT/vNTxvyKk/RT8LOivnL2M
        C0fP8rX/Gzdu8bpeHvhMN67eJN3gkkNCrTRgb797++496gEQAF7yJwAEUf0fZ3LsV839E81c9mFc+1vu/r4a7u+lA8BaKwCQVR5z/6YGf+JVa3/m/tUDY8sOAEvcXwRAtX
        EU+JT2d/BOw8CstYimQN9Cqezlm6YD+zrVoRcp+C26HNQQAOW89KdVBrz99lhMbPg9B8AcAoA3AYD1AYIIAGG8D/CX2AfoglRX1gcQAJD7cH+snEQAKNnGAbBj4y7s3roX
        +3bqAXDs0HEcF92fAeDsKSoBTgs9gItnqQTQ1f4CAK5duY6Tmw9h5ag4LPvDB2tHJeJo4U6+1n/r1h3cuH5TgMFVAQA3rwli/9bBwTAjEHsHrC9w+2bFlwSsCRilA0C8Yu
        w33cR5f9aO/YbVyUZILaZFCKmrsuvPxODPfNI8AwDMdihGQCsGgJMEgCMaAFhMAMhOtOKuPwsn/6JNp/+64JcDwM9KAIwlAHhEmt/zLwMAu+2n+vgItPVKwbCcEhQeOImr
        tyqmuRSqBoAyjv2aXQ3oFoa2387BSLdfxEbgT7pGIOsDhLp0RCQBIJYAkEAASHEVywACwBIRABsJAFtKtusBQBnAAd4EZACgEoDX/3oAnD9D9T8F/8Vzl3QAuHqJBf81nD
        90Gss6+yHGqSdy6g9CketolLw1D7tm5uDMjmM8G7hJQXyToCpIDwEJBDoYqJQG7OUt+riKnB4USoBAPQBMnPVv9dhvvSyE18pEWK0sxDTLRUK7fMQ+kofQ+hT8dXIsHvsV
        lv70we9VZzlmVzcFgFwCQIEaACy77KNqadw/XMP9mQKsBEABA4AnASDC4l1/juT8tUifRuZi8Z5j5Pa3K9Q5dADoGWLiqq/SL/1prQS4dfRH99ZdxD7Aj1QGCI1Af8oAhD
        7AX4hx6URlQBcku3ajMqAHB0BOiwFYPjEeG9ZuxZa12/lS4M5Nu/lSIAMAmwVgJcBxWQlw9jRlAGfO8+DXAeCiAIDr129gX0YJ4h8ZgAKXkdjeeDq2NZqOLa5Tsa7FdKz/
        PBB7g5fj0vHzlA3cxi36fdy6cUsPAwtBwMTeVlFLhawEiHo5AAl1gqn+j1dZ+ksyWvrTNf60zvqvtxARteml8yKkvVCIlf234mD6CZxadR7H8s5gRf9tCHNbiqA6S2Rjv6
        YHf+Tu76kDQEnJJgLAKrMA0Br8Sbdi7DfOsl1/au7PAeAbg1+WlAIA0tq/Cfd3mBSJ+lNjMXDRWhygP9J70ULiAPjbHADKL/gl1esSii9eGoJpMgDMEwEQRAAIJwBEMwC4
        dKYyoBvSCACZBIDFLfpj+QQCwJqt2Lx2m9gIFAGw6yAvA44eOCbMAcgAcP6sLAO4IADgCgPAjZvYGb8CGS2GYFOjKQSAGdjG1GQGtjaaiU2u07G21Uys/y0CR3O24Qab/q
        MS69bNO7zbrwYCrfKAQ4D+ny8VlvMv9/j6o4h8MQDxMgBYes237sCP+umIqZeO6LrCgZ9JrZcg7/u12Ol/EOe3XcatS8qNUFeOXceynhsp6BeLqb/5wR+9+xMAai/HLAKA
        v0UAyEo07f4W7fqTBX+MFe4fKLh/df9SAGAMAWBmhNldf9Up+OtNjcHQnHW4cOMW7tXDCADlOPhjrgx44ZOpGO/8o9gHEADgRyVAIAEg1PkvRBEA4ly6EAC6Is21OxYSAB
        YxALAMYM0WbCIIbOONwN3Ys3WfQR/gBAHglKIE0Kf/pEsSAG5gV+JKZLYchi2NpooAmImtTTywtbEH/Zteuntgo9sMrH3aE1vHZOHMliM8yG+TmwsZwW0rMgJhBaG8+wI8
        A3gpEAm1gyn9TzAAQJLJa77j6qUhpnYaD/7EZtlY9HYR1o/ZjhP5Z3Dj3C3cva39PDcl7MaCpunwq7XEoPZXG/wpVLi/AIDlFgBgkQYATA3+lHbPv0bzr9wAoOL+DpMjUW
        dqNAYtXovz12/iXj50AOgRYtL9y6P5Z1gGtPh5PgY364iZ9X/gfQChEfg7AigLCHX+E5HOf1MZ0BmJBIBUAkCGCIDiCXFYv3ozNq7egq3rtmPHJmUfgC8FHj4hzgEIy4Dy
        EkACwNXLAgB2J65C5sPDsNl9quj+HlwMAIJmkWbT22dhDWUFq9/2w+75Rbh48AxB4A4/J4BBwCwIDFYOyrMvwHoAUa8EIb52CAeA/LjvVIO5f+7+DVIRXzcFcbXp3+4Lkf
        X8Mqzsuh77Y4/g8v6ruHvLslJle84+zG0ThwU1FhEA8jSaf4W69F/u/nMVADihAoAlBIB8GQDK46Zfw8s+DJp/VUNiUTkoBpUCSQExeMg/Gg/5RaOSbzQqMy2Ixi85pQCA
        icGfqhMj8FVsHg5fvHrPJ8gUAKjg5p8hAJz+DsLvHfrCox4bCPoJnpQBLHD6jQMghAAQIQIggQCQQgBId+mOLAJA0fhYlKzchA2rNmOr1AjcIgDgoAiAY+IsAAfAqbN8FU
        AqAS6LJQADwI2bNzkAFjIANJrK3V+QEPxbKPi3UPAzbW48h95nLjY4z8Lq5rOx/ucYHErZiGtnL/M1f7YxSAKBDgbX1BqGehDweYFyWCpkAIh8OVABANVrvuslU5aQhIT6
        qchotwhFP67Czvl7cWb1Ody+bH2/iQFgVpsozHPM5gAwNfZr6P5za6+ABwHATzMDkAPAkrHfFDOdf43Bn6oU/JVCKeBDYlAnPAHNY9PwWEImXk3LwfuZy/BexlK0j8tEy8
        g01A2Ix0+LLQRAPgFgNAFAuulXBQDVKPVv7Z2C9J2H78sIKQfAX1oACC23pT+1eQD28r23xmBag+/hQQCY0/AXKgN+g58TawT+iXACAO8DEACSCQBpzt2RSQAoHMcAsJED
        YMvabdi+fiffE7Bvx35dBnBU2g9wTMwA2DKgWAYoAHBDBgB3VgIIABCCXwj8rfwlBX/judjE5UklgSdKCASr2s/Hxj6pOF60hzcHWZPv9s3bChDc0soKZLMFZS0JBAAEka
        MzACQql/7YUd91k5BYh0qBxulY9kEBtkzejuOLT+DqkWtl+vvhAGgtAMDfzNivt8L9V4gAWEEAWFeyMfjUMytnHDUAQI4IgGw9ACw67jvRAvcnAFSNiOPB3yA6CS8uzEHf
        VesQvfcAlp88g01nL2DXxUvYT6niPtJm+v815CTxew4h++Ax3L571zoAqAS/4xQh/e+VtRpXb92+PwDIMQBAOQ/+mNsc1OGrWRjl/is86v/IAeBNAPClMoA1AsMIAJEEgF
        gCAGsEpooAKBgbg7UrNmA9ZQFSH0C3KYiNBO8VGoHCMJBGH4AD4BpvArIegBIAHgoAsODfQoG/mQJ/UyMKfi4vrvUunljtNgerXvLHtim5OL/zBO6Qo7NGnyEItMoDqVnI
        R4hLWRJIAIitFUL1f6K++deA5JyMRU8twtoeJTgUfxiX91zGnWvls6y8ffE+eLSO1gFAufRXaOT+XjL3n0OaSQDw5QA4qQGAPBkALJj8s+Sm36pRcTz4XeJS8EPBCqQeOo
        IDV67iEgWhueEZ9uY7FpKaA2CUJ6pPj9B0/+Zzicr3yf1NA6Di3F8PgHA0/c0HvVp3wax6bB7gF3gRAHwoAwhw+hOhHACdEevSFYmUATAALGzRD/kMAMsJACv0AGCbgthE
        oACAI8KeALEPoAMAWwk4d1HIAEjSKoCUAWxyn6pI/7fKUn89ALy4WPBvaOTNtd7dGyUuXljZmPRBKPZElODqqUu4w/oDVoCAlQbs9aVZKtQDIFQAQENyf+dE5L29FLu9d+
        H8xvO4daH8ZxF25AgA8HZcRADIN+H+RQr3n8O10goALDSz519r7t/A/StHxqJ2TCK+yCtECgX+OaoBK2q5zRgAyj3/DADvRyy5L7W/bhJQAkD3kHKf+7ekD9CwUxC+f34w
        PAgAsxr+LALgdw6AEAJABAEgxqULEigDSJEAMCYaa4rXY93yjRwAwkTgbt4IlDIAXSPwmH5H4PkzQh+AZQBsKZBNArI5AHUAzNKl/roMoJGnGPzeothFH/OwjlTiTnKbh9
        Uu3ih6mF7+noDDi3fgxoVrPLW3BgSsL3Dn1h1YUxEc23AU4S8GIrZ2KNX/ZCoNqARwS8auWTsq9O9nx5L9OgD4qQKgSDf3bwiA2RwAK+FjGgCLJjhmMQAklHnuvzI5/8PJ
        GRi3aQuOXrtW4cGlAIBa+j8pCj0yV+HmfdxHrusBMADcQ/eXLwe+8f5ETHL6GbMa/Iy5DX/DfAKAv1NHBDv/hXAqAaIJAPEEgCTnbkhv3g95BIDVResEALCVAIOJQPlS4A
        n5luDTEgAuCaPAl/SrABkEgI3u0xSd/y2y2n+zCgBY8K9nN/2Q1rnPJwjMx1r3BVjj5oMVzt4ofMIXm0bm4PT6I7h1XThHkAW2JggMYHD75h2L+wIMABEEgBgDAOz23FWh
        fz/bCQAzW8foACAf/DHn/gwAMzgA1pdsDCIATDcEwGIJAEkCAMxM/pm66qtSVCzaL8xGwqHDuHbn3uzZ5gAYSQCYpg6AWlOiMbFg0309ScYjKQ/1f59hDIDu9wYAbF9Au2
        /nYGizvzC7PhsI+hXzCAB+BIAgAkCYcydEEQBiCQCJBIBUAsCy0VFYVVjCAbBx1RZsKdmmAwDrA+hXAo4rlwJP6zMA3SDQdWEOIOPh4ToACJ3/WWLwy5p/jaQSQAp+EQDi
        PX8lpLXuPljD5YtVzgtQ6DIPxW+HY7vnclw4cJYf38VS/DtUbjIQqMNADwH2ursW9JuObzyGiFeCCQBhVP8n8do/zS3lngHAiwOgwCr3n117lQUAWEYAyEy6WyUjodRXfV
        Ui5+9Awb/w6LF7GlwKABgM/jhOjkKdqTHwWL71vgX/HfrD6u2TjBq/zEBtFvQVOPhjqhHY6A8/dHqsF2bVY43AX+Hd8Hf4Ov2BQAJAKAEgkgHAhQGgOwGgL5aOjuQAKCne
        gA0rxZUA2elAB6SDQQ4dV28EntevBAhNwFVIlwDQSL70pwfAZhkAJPeXMgAp+IVLPoWLPldz+WG1qx+WN1iA/Oa+WPF9AvbGb8K181fFsuCuLiMwVR6w1QXWTzBVq/IS4K
        UgEQDJSBEBsOueAWAxpf8Fisk/taU/ufvPIgBMJwAs4AA49cyK6cdKAQAz7l8pOg5NU9K589/rhx8BoIEaAKaIM/+TozEh//5lAGfJAb+bEg6H3+egtrm7/ioIAKwPwF5+
        8coITG/wE2Y3+AVeDVkfoCMCCAAhBIAIAkAMASDBqRtSGABGRWJlQQnWFq7H+hUbsXnNVt1S4N7t+/hIMAOAfE8AKwPkjUBpQ5A0CZjeggDgpgfAZmndX3R/If03BQDta7
        75JZ/O/ihs6IO8toFY3X0hjhbtp7LgNp8fYBAwC4LrUl9AnQLH1h9F2ItBiCYApDEA1L+XAIiFpwgA+a4/b5XBH8n9WfALAFhFANhQskEVAIvS7FpLAEhPUDb/UjQGf2Tu
        XyUmDtVjEzBk/UZcvn37/gFgqgEAxE0/bMvv6KUb7hsAVmw/gCd6eMDhDy8K/vB77v7yMuDFT6dhnNvvVAb8IvYBOsLf+U8EO/+NcBEA8QwAzfoilwEgfy3WFEh9gK26PQ
        F8JWDnQbEPIJwOJJ0MpAMAKwPEeQChBFiFNALABvfp2NJoltj5FwCwicvQ/WUlgC791wKAdMtvAJa7BqLYOQB5zn7IfToEGycX4OLBc2JJcEcmFRCIMODzAiqdfCEDYCVA
        OAdAMgEg9R5mAAIACs0O/sjd34MBoJoJADRkAFhKAFhoAACL3D8BD5H7v5aTi03nL9yXAOMAGCECQOXEH8cJkfg9sRAX7+Hsv3w5k9f/P09CzS4B9yX9lwOg5U/zMbBFF8
        yu97MeAE4SADojmgAQRwBIbt4POSMisDxvDVbnC2UAbwSypUBxJUC+J0A3Esw3BZ3nI8EXzuoHgq4RAHYmMACMwAa36eT0Hjz4N3GJABDX/pUAmC+r/1Wu+SatMrrmmwDg
        GoQilyAUOAUil96+rsdiXDtxRSgJFBC4o9kw5H0Bg8Yx6wGEvxxMGYAEgCQCQCoBYHfFAiBnP2YQAOYSAHzqGAJA6f5zDdzfo/ZqTDMHgDaaADA99svcv1Z8ImZs237fHN
        YvTwQApdlqx32zPf8d5qVh1eHT9/y5bT5wHG8MnofqP08Xu/7mx37rdAkhVUwZ4NQpCH88OQCz6gsAmOf0B/wIAIEEgFDnzogiAMQSAJIoA1g8IhzFy1ZzAKwjAGwUR4JZ
        BiBfCRDOBjiuWwngB4Oc1QOALQVevXYDOxJW8gxgPQfALGXwk/vLB38kAAjurwSAVvqvB0AQASAYRaRC1xDkN6R/t4rE8cQ9/HfCM4Hbdy0CARN7X6kiOEYACCMARNUiAD
        RM4QBIIQDsvCcAiCUA5HAAmBv8kbu/Ry0GgNWYTwDYaBEATC39GaT/lQkA7TOzsfbsufsHAJ4BzBUAMNX4mm/HCRGoPzkaHsVbLTq9p7wel6/fxKiwTNT6bixqdvI1nf6L
        Y7u1O4egPv1/bYIA+3dFZAEfvjkW05x+xZwGv8GbAOBLABD6AJ0R6dIVMQSARAaA4QSApauxKm8tSooEAKgfDqI/IdhwJUACgDwDWO9uDICNjfQA2KBzf29Z+m/s/sbpf6
        AqAAqc6GWzCBwN2aEDgCQBBHfNguC2eFCMAIAQDoBUttuvfvI9BEAcB4By8q9Yl/4rl/707j+z1hpMFQHAMoDlRgDIlgEgLcGqXX9VKP3/vmgFrty+fd8A4LNsBeoPnwsH
        NQCIB35WHxuOV/2ysO7Y2XvynNgW1tiCDWjacRIcfvUwe9mHNLP/5uR0zMjciJEJa9FuaAKBoPwB0P7r2Rjn3pEA8Cu8Gv4BHw6Av6kM6Ixw566IJgAkSADIXYWVy9YQAN
        aLKwHscJCd2MU3Be1XNAL5SoB4OpARAFgGEG8AgEazKeAp+Ln7GwNACP55vPZfp+v+MwD4KdxfAIAs+HUAoOB3CUFevSCUvJeBy5uF3z2r7ZnkILh7W4SBQWYgbxjSRxEA
        jooZQIQMAGkVDoBtOfswvVUs5jgsIQAUaTT/Vhg1/5j76wGwUaMEYADIJQBkqAPAXmPPf9VYYb//oPX3r8HGHlMXLkOtIbPhMDlC87Yfdtin49gI9EpfhdNXrlf4sl/Oul
        14utcsVP9hEgV7kJH7G6b/NTsHo+XAGMSs3Itb9Ad54epNjKWUuW7nQB0cymsgqNEf/ujbqjtfCfBs+DsWEAD8RQCEOXdBlFNXJDTti0XDwlEkAmBt4To+EizfFCRfChQO
        B9GfDcA3BZ3V7wm4evU6AWAlUqkEWOc+g4J/FgW7CIBGAgA2EAA2sLl/KfjZ9J84+CMs/1nq/qz+J9d3Jgg0CcOmbxfj7JIjPMhZD0AnNRDc0QbB3X/u4sj6Iwh5MRiRBI
        CUhqkcAMkMAF4VnAEsETIAPQCMB3+Mmn88+CUArME8DoDTlAEcNwMAefqfpDL3L7p/VQr+OnFJ8N+z974CYExKDhyHzOUbfnQn/qpc9uFAAGg4KQbjctbh7NWKgcCNW7eR
        RS75TO9ZcPhuPGp39UedXuFmr/qq0SkYz4xORsn+02Lz8B+ELVkLpz/mlms/oA59vXr08ofnh2JWA7YS8DvmOXWkMuAvBDl3IgB05QCIb9oH2QSAwiUrsYLKAAkAfE+AdD
        oQvydAPB7sgHxPwFkdAKSBoKvXrvMeQCplAOvcZnD338A1l2ujIvi9xeCfp5j8Uwa/AICVhu7Pgr9hIAfA2leScdhrM24cu4p/7vyj+7kaSQUCan0ClgEcWXcEwS8EIpxK
        AAaARAkAFV0CLDlAGYAAgPkmBn+MAbAGM0hTOAA2aQAgSwRAOgEgNcHiXX8MAHXjkxC6b/99C/5rt26ha2gyqhEAHC247IOd+e80IRpjF5XgKP1hlufjFKW68zOK0a7rdD
        j+MEEf/Bbs+a/ZKQT/88yh53RVV0KMj1iMWt9P4U3B8i0DwvDah1Mwzfl3ngEwAPgQAAJFAEQSAGIJAFlDw1CQswLLl6ziS4FsFoCtBCiWAqU9AQSAY4dkm4JOC41AKQO4
        dk3IAFIIACWUAWzkwT+HUnwCABNP/QUArJO5/zrR/aXBnzUG6b+i8+9EgU9a/WQs9gxbicsbzuCfm3c1VmfUQaBZGog9gKMsA3ghiAMgqWGyCID0ewCA/RwAszkAihVLf3
        PVlv5k7m8ZAJYYAsDM2K8IgFqxifDeufu+AWDXidN4c7ofqgz1pvQ/Wg8AEzf9Oo6iTGBcFH4Nz8Gy7Qdw/WbZlgcvUjaRQ0Hxx6xoNPhpHKr/PI1S7QCLd/2xAGeuPCpx
        LZUPglNdopr5h8nhqPHjjApZDXjkp/kY1bQT5jb4Hd4iAAKcOiOUABDRUATAEALA4hVYkSuuBEizAOv0ADgg2xQkPx6MHw4i2xR09eo1EQAjKZBniO4vAkBM/4XgFwCwrp
        Gh+/vIOv/y9J/c3yUAxQ0IBi3DsL1jLs7lHMbdq5b1pLQgYAiCO+JyIJsDCHkpmAMgkQAQXz8JifeiB0AAmCYCYJ4cABa4/4xaazkAvC0GgNT5t2DXH9vvP6hkw327uTm5
        ZAuaDJkJ+9H+FPzRsuZfpMm7/mqMjED1YSHoMDUWo1MLkUd/zOevWL5bkJ1TcJRS3AwKiF4+SWjbbQbV++Ph+JcnBT4FWe9wi/f8s25/8/7RSF13QPf5D506jxf7eKLmr5
        70fmHlDgHnv4PQ5Yl+mFv/N3g17IgFBAB/p04IUQEAywD4SgBfCtwibAveKJ0PeFB5PqB8KfCMUAbw24GuXMN2AwCsdxcAsL4Rk5cCACWK4F8gzv0L6/5y91/e0B8rGgVh
        82cZOB66AzePlX7Hp7mMQMgAjiL4xWCE1YogAKQgjgCQ4JaK7RU8CCQAIJ4AkMsBYNL9ayvdfzoBYLJlAEi+WyUlwaK5fwkAVSLj8OmyQpy5cfOeB/95cpXuYcmw7zMNjh
        PDrbjpN1J30Wf1YWGoPSQY7caG4je/NMzJLMbCtduwlf6Yj5y5gDOXrnCHP33xCrZRjbtm92HEFW3E0NAMfDohEA93noYaP0+EQ8c5qN09UDzxN9yqPf+1CADfeOXg5EX9
        zsmU5ZvR+JfJqPWnb4UAgOmTN8ZidoPf+EqABIBgAkA4ASCGAJA5JJQDoDhnpWwpcAtfCmR7AoyOB5MvBcozACoDGAC2xa1EsgEA1hEA1okAWCcCoEQHgAU692cA4HP/bs
        Lgz0oXfz72u/7FOByetR5Xd5wv1z34aiDQAeAFCQCpBIBkJLimYtucneIHViwAZskBYLLzv1oX/AIA1sKLALCeAFBsBIBMEQBpSQIALNj1J7k/A0DL5AwsPX7yngNg4cbt
        aD5kOuxZ+q9wfxEA4w3cn8nwpt8REagxnEAwOBSOA4NQv78vmg6Yj/ZDffD6GH98PDkE386MxEcTgtC+7xy07jkL7n9PQ83fp6B6Rw84dp6P2j2DxMs+wq0+8JO5/8MDYp
        CwZp9u/pzND3SeHYea30xEnc5BFbY34OmvZmOy21/wbvAH5hMA/AgAQc5dEEYAiBYBkL9oOYoWEwCWrcVa3VLgNmEpkB8PJg0DKWcBpAxAWgq8clXIABgA1hAA1jdiwT+H
        gpwBwFMHABb8a7nz6wGg2/XHpv5c/bDS2Q9r24Rhb78CXFp9EnevV8yuU8OMQCgBjlEJEEoAiBQBkEIASKcMQCgB+EpDBVxMIgHAgwDgTc7vZWLsV5/+C8E/rVYJJnEAbD
        YBgBwDAFhy0y877osA4BCZgKHrNuHm3Xu3337/6XP4yisUVXpNQo2JoToA1JR3/824vwSAWsMF1RxKMBhCmcQg+nz9g1GjTwAce/nCsft8OHadB8du9LJnAGr0CkSt3sGo
        zS757Btu8V1/hmO/zPmdeoRjQuo6XLmhr1kzV29H6z+nosYvcypsLJgBoOlvvhjcsge867NGoB4AoQ27IIoAsJAAkJdVTABYgZVL1/BNQToAKJYCDWYBjp3WXxMmLgVKJY
        AAgJk6AKwTAbDOCAALFOn/alcfrHQiELQIwrZvFuJs2j7cuXhvsk5DAATrAJDGARDvko4t03bygSK2UiDPGMoNAIv3Y2qrOALAUg4AKf2Xb/mVp/9y91cC4AwB4ISJDMDM
        3L/acd+VwuPQOnkhFh6+N9uAz1y+iqHxWajZawIcRvkKzT/Z5J9h518KfjX31wFgGIkAUHtoOJUEpMGiBtHrBtLLAUz07/70sh8FNZPhNd9WuH+NzsFo2D0c/SJX4PQl/Z
        LkyfOX8evUSDh+PR51OgVWIABC+SlBvzw3DN712Ejwn/AlAARyAHRFJAEgY3AIAaAIhYtW6GYBNqzcZHBA6AGxEXhEdz6gMA2ovyjEKANwm6l3fxEAJQYAWMMlBD/b78/S
        /81vxOP4/I24efwKWe396TrpM4AoDoD4Bqn8pp+sV5dh7fSN2Fd4EBdPXNI1DcsXAPEcAFpjv4bNPyn4BQCUwFMTAAtZBpA9wTGVASDe/IGf8ss+SPYR8agcGovPlhRg54
        WLFfoLOH35CkYlL0a93hNQfain6k2/pXF/HQCGyAAwKFwW/OFC8EsA6Btm+ppvDfev2SWEB/8jg2L5tN+x8/q6/9rNW5gZvwyuP4xHzd+8KFDDKnRzEIPA2+9PxmynjpjX
        8E/4iAAIkQFgGWUAhVQGsFmANYXrdNuCt63XHxDKpwFlB4Sekp8QrMsArmJb3AokEQBWcwDM5gDgEgFQQgBY6y4BgILeZT5WO8/HhmcicGBkMS6vO4l/bt+/U52kUeDgly
        UApPPbfhgAouukIaRePEIej0fa7zlY478RRzec4CcSl88k4AECQIIIgBUGgz+m3X9qrXWYaBIAGal2bRZnEQAS71ZJii/VTb9Vw2LhGBaPP/KWY28FQeDI+YsYHJ+Jer3G
        o9rgORT4kUr3N7H0JwDAtPvXGmoQ/GoAoOCvbcr9NZp/NbuGoBYFf/MB0fh5wVIkrtnPp/2kx63bdxC6eA1a/j4ZDt9PQ90uwRUa/NIhIY9/64lxTbpifn22EtAJATIApA
        9iACgSAJC7WjcLsFk2DLSHnQ+4U8wADkpXhZ1SAID1AS5fVgKgRMwAStwp+N2F4BcAQOm/GwW+0zysaxWIPZ0W41z2fty+LJwp+c99W2+SAyAMobWikdggQ3nRZ312x18a
        Amsmwt8tHhGvpSFrQAE2J+3E2UMXynQc+VbKAKYQAGY6LIOXLvjNu/9UywFAGUCKBgDMnPevu+yDsgDHkDj8L3sZlh85xkdiy2W67vZt5G7fg6/nk0OT81cbOlcf/GruP8
        Gg8Sel/4YAUHP/wQbuP9CM+2ul/zL3r90tBI8NT0Cv8OVIWXcAR88pl6pu3rqNyKUlaPv3NDh8O5mP/1a0+0sAaPyHP3q1648FIgD8nbogmAAQ0YQBIBhLMwtRmC0HwCZx
        FmCHDgBG04AiAHgJIN4WLGQAK5FIAFglA8BaAsBad8n9vbHGhdRoAbZ9moTTsTt4uv+P9J9BQ+5+lQDBLxEAakYjgYI+TvWm3yxE1MlEcM00+NVNQkDLZER/mollU1ZRiX
        AY1y/dtLpHwDKAKa0FAHiaGfudYeD+U2oyAKzD3FZbStYRAIrMAsBw7NfkRZ/Ku/7sQ2JhHxSDxyJTMGvVOhy9eIlPtVndeCFdp8DYduwkRqfmoPUID9j3noTqo1nNrwx+
        Xe1v0v0jtd2faYgGABTBH26F+wsAYM7ffGA0wop34Tw5vuHfLlteZGn/w+T8jt9Ooro/oMKW/dQagfUpK/nmlTGY14AB4G8OgCAJAAMJAAsLUZBdrBgGYleFyYeBeAaguy
        xUeVegcFXYRXEZcAUSmzMAeHAACMEvAGCNK7109camF8JxZOZqXNt7nq+9s8Dn5/vdvftAAIAtAwa9GKoAgD74FwrXfBMAmCLqZyO8XjZCamchoFYGfBqkwLdtMhJ/X4o1
        oVtxeu85flqRJd8PB0CrRMyozgCw0njXn8HgDwMAd/+aAgAmmARAOgFgkQiAxHjr7/oLk931F0wvSVUDYlHHPxrvx6bCa/kabDl+EueuXuV1rto3zF7HhmsuXr+BI+cuYN
        mOPRielI0nxs2FY99JsB86B46TQ1FjerTqnn8e/FoAKI37D1J3/9r9rHB/kgPV+8+NTcGGQ8pdiOcoJS7cvBd/ecTA6buxqPHDdNTtdG+c3zALeO2T6Zjh1pn+QP+GnwiA
        cAJA2kAhAyjIKubDQPpzAYSDQXYYnAvAZwE4AE4bTQOyEmBr7EokNBuBVa4eFPSC+69xmUt1/lysbxeAvT1zcHHlUdwRg0IKfMPgv68AoAwg6EWWAcRQ+p+h6v5S8HMA1F
        +EsPqLEVJvMYLZ7b51FsOndia8XVIQ+HIGsoauwI7FB3Dx2GV+OrHWY3XMDox1j8FMxzxy/5XKPf+1jAd/5O4/peZ68wBovVgLAPHq6T+v/eNM3vRbjV3z7RMFR89gPO4b
        gd/jMzB+cT5i1m1C8oatyNyyA3m79vKXsWs38Tv++sRm4M1ZgXAfPhPVBkyF/XACwKQQ4bTfGdGKAz+5+0824f5jLXR/OQAGWZH+W1j7N+kXhbmLt+DAqfPYfeQUMlZuRa
        dZsWj5x1TU/GYCav02l2r+kHsa+PLtwW1+nI+RLXvBt/5f8GUAaNAVYQSAVJ4BFHAAFHMArCUACCcEC0eEG58MpDgh+NRZXQlwiTKdIyV7sfjtuVjeYDrWNJyF1Q1no6Tl
        Auz4Lhmnk3fi5oXrVDbe4QdzKubxH6QMYCMB4KVwhHAALDRyfzkAhOBfhNB6DAA5XMH1liCoXi6/5tuvVg48a2TAs2kqIr9YgmKvzdi/8hgun76q6HVcOHYFYZ0WY7hjJG
        bXKuIA0Kr9Fel/TUMAbDWTASQTABLira79WfBr3vRLquYfhyrzo1BpVhCqTvVF3Wk+aDjJG26T56HZlAVwG++JeqPnoMaIWag0ZAYqj/SC/cQAOEwPhyM5ZI2ZMaqXfSrW
        /a1J/4db0fyTB78l6b/Krj+2nfdhKgO+nJ6Md4cHoOlvUyndn8jX+evoXD/svgCAbQ92+SsQnZ8cShnAXzwDCJQAMCAIuRkFygyA7QdQAEDlunCD24L15wJcwf4UyiC+iM
        KGZwKw7bM4HJm/FlcOnKfs745wA/BtQQ8sACgDCKQMINgIANruHyoLfh0A6i3l13zziz5rLYOXw2LMqpuBBU9mIeGvQqwK2Y6dyw5h08K9iO6ViyFOAZjgmG7R2K+h+08m
        ja+2HnM4AM4SAE4qAeCUlmr3aDYBIEkbALrg1wCAoftLwc+u+nbwI/ky0et94mDvHY2qnhGoMiccVTzCYD8zDNU8wlF9VgQcZ1PGMDtG0KwY2VVfBqm/mV1/Ru5vCIBhZW
        z+WbD0J9/1x1YAHP/0Q/VfPFHj93n6Nf5u9yfwDSHw+ZsT4eXcGX4NO+sAkMIAQBlAfiZlADlSCSD0APjRYBuFHgCfBdhzSB0AsqVA6Y6Ac1uP40z+Plzcfgo3rt3ArTu3
        cYtKQyZTAJBD4P71AAgAL4RRBhBL6f9CHvxqAJDcX0j/DdyfFEAAYFd8S9d887P+6xTA23EpPByyMdstC7PbpmNii1gMrxWO8Y6pFPhFuvP+DNN/o6U/0f0ncwBssAAAi2
        QAiDcBAK3aX839KfirS8HvQ1rAFAOH+aR59G/vWDgyeZE8SXMp6OdEU+BH66/51rjq2/DEH4sHf4aXbulP1f17abi/ycs+wsSgv/+BL6kmlQHPfjUH0xr3gF+DzgiQA4Ay
        gLzMImEcOE9ZAuzcuEvYEKS4Llw6G1CfAZyXnQ145bJwS9CNW7dw8/YtfmvwzRu3dAC4feu2SQDc734AzwBeiqAMgAEgUxf80WLzz9j9F8uCX+7+S3XBr7zoU7js07tmIW
        Y7LIMHAWF2rXzVTT9MUvAbDv5Mlbn/JALAOBEAJQSAQrMZgMbcv0n3lwAQaAoAUvCL8qKA94wRAl8K/tl6AKi6vxEATA3+RFjm/kM0av8BBp1/LQD0vLd3/ZV/HyAMzX7z
        w7DW/eHfoBMHQGiTvhwAS9JZBlDEMwDDDUG6swHlJcBBYwDwEkC8JOSKdFvw1evc/W+w23lumgeA6v79+5AJHN14XBUAUTIAsOBXB4Dg/hIA/Ayu+WYAmC+76kvY9rucD/
        54Km76WWlm8KdE5/4s+AUAbMDsVttKSgIJANMMAZAqA0B8vAXubyr9lwW/lP4buT/JmwJeDgCD4Dfr/uaaf4ad/5Fqgz8R1rl/KSb/HvTg150SRCVKx+dGwbdhZz0ABlIG
        kC5kAIZbgtnZgBIA9hMAdGcCGB4Oesa4BGAAuHblmh4AKhmAIQQehOAXBoEIAC9HIIgAEMucX3R/If3PNmr+hRq5f66G+1ty1ddKi8Z+Dd1/YukAIHb/o61o/gWaSf/niw
        Dwlrm/KQDMMJf+l0PtP6SUgz//EffXlQE9wvH++9Pg7dKN9wAEAATLMoCVegCs3qy/KHSLPgM4LMsATssuCtVfF245AKzJAO4lDFgPIOCFcATKAFAa99cDQO/+apd9mNr1
        JzX/tNx/suj+E2tuxFizAMgiACQSAFjgmxn7Nev+hs0/H2P3d9Byf6P0P8qy5p9W+m+J+1fQ3P+/CQCsDGj3w3xMb9qbAyDEvTeSu/thSVq+0ARcvFKRAZgGwEn9TcHyDM
        AAAPIywFwG8KDMAuzN3w/vtgEIqhVHAMjS1f5Gzb96yu6/ofszqbt/odmLPs27vzL95wCw34hZmgBIIQBkEgASkswDoLTNP3ntbwoAcvdnwV/qpT8zu/7Kw/17aez6+5cF
        vzQV6PZXIAY+PgRBDbohpGEPJHw9CzlxucjPFlYB2JkAuhJADgALmoD8qnBZCcCCXwLATUuygH/uPwDY+O7asPXwcJmPsLrJVP9nGXX+wzWCX1r7N0z/efCLtb8cAMZXfa
        1Q1P4eGmO/xun/RhkAtpesDTz3TMG0UyoAYBlAAmUAsSqDP5EWLP0FxSlq/+rWur8WAAwHfyZZOfgz0symH0vcv09Yqff8/1sAIEHg+9cmItCpO4Lrd0f0s6ORPT8N+fxU
        IBkAVos9APmpQLImoHQsmGIZUGoCEgAk99cBwEQPQDcU9AAA4OrZa0jqmo6ZjvMR2SCD3D9LZelvsa72V7r/UoX7+2m4v3TZh6fKeX9aJ/5MU1n6kwf/hBqbMIYA4GESAJ
        nGAFB1/3CVzr987V8e/BIALHX/WZbW/mWY+1dzf425/zpWjv3W+RcHv1QGvPzFHMxz60UZQE9EuPRFWic/5C8sQjHbC5BXYjAJuMt4FPjQccWZAOdkZwJIAFDrAUgQsKYE
        uNcQ2L1kLzxaz8P8WmFG7q82+BMiH/ypKwz+CMEvAEBy/wUa7j/XrPuvUXF/JQAmKACwQwMAyQYAsHTpL9gg/bfE/aXuvxT8mkt/YvCbSv+19vyPNrXnP7zCdv392wHAMo
        Dmv/lhcssBCHXqifAGvRDXbgQWT0nA8lzhPAC+GchgFWDf9v26W4LZIBADAN8LcFJ5MrAqAMQMQJ4FPIgAOLntFEI+j8K0ml70c0lRcf9s7bFfg9pfzf1NXfRp0a6/mvLJ
        vw0KAIznANhkDgBZExzjCQAxceabf+bcvyKaf+Xl/pak/+bc/z+w9KcKgK7slKBgdHt2DMIpAwh37ouohn2R+OQYZI+OxvKs5Vi/ajM2r9uObRt3YhcF/96dwhTgYXYvwB
        EK/mPiDcFnyPnPXcCFC5dw6dIVPgDEjgZndwTeuHmT69bt27h15w7fLco2grHt43cfkE0/0uPurbvYX3QQoV9GY3Kd2QioF0vun2m868+o86+e/vvJAFAa95+ptedfpfaX
        3J8BYDQBYKY2AFIIAJkEgAQBAJYs/QVb2fzztnDt35LmX3mO/Voz99/rv+v+8izgk/dmIMS5NyKc+iLauT9iG/ZHXIuhSP3RC0vmpmBFejHW5ZZgY956bCnchO0rtmLnqu
        3YvXYn9q7fjf0b9+Lg5v04su0gju44hOM7j+DkrqM4tec4Tu09gTP7T+LsgVM4d/A0zh06w3X+0FlcIF08fE7QEUGXdC/P49JRQRdlunT0gqAjF/T/lumiWV1U6IL48vzB
        8zhQfBB5Mygwn/fDlBpzKPhjTI79hpro/AdY5P5FFrm/1qafySoAGM8BsFkGgLMqAEgiACzMUgIgSrbrL9zCpT958Fvj/h4WLP1Z1PyLKPuuP/mWXyt2/f3b3V++L6DDd/
        Pg1WQAuX8fRBEA4lwGItFpEBKcByH20RGIfmMiIj+YxBX90RTEfjwNcR9PR/wnM5D4qQeSPpuF5M/mIOXzuUj5whOpX3gh7UsvpH85jzSfK+OrBcj4ny8W/s+PXkryJwUg
        4+tAmYKQzhWM9G+CkfZ1iE6pX4eJCudKEZX8vwiupP9FciX+L4orQVT8/6K54rhiuGL/R86uUxwiP4vG/Kf8MaOBJzxr+lGAJyBWZerP/NivcvDHr9SDP6u1T/xRGfyRu/
        84AsAo+80EgJ0laygDyJ92WgMAcQl3q0bHWbTnX5f6WzL3LwW/NZN/U0vh/pbs+R9SRvfv9d8Y/DEFgMZ/BGIU1f4xBIBoDoBBSHQZjBQXygKchiKlwVAkNCAg1B+MpAZD
        kNxgGFIbDEdag5FIbzAKGQ1GY2GDschsMI40HlkNJ5AmIqvBJGQ3mEwvp9DLqaRppOmkGfS6mciq74HM+rPo37Po42bT5yDVn4sMUnp9TxKBpL43aR5S681DSr35XMn1Fi
        Cpng/JF4l1fZFQ1w/xXP6IrRuAGKY6gYjmCkIUKbJOMCLqhCC8dgjCaocilCsMIaTg2uEIqhVBikJEXRb46bqNP3r3z9bV/mq7/hgAAmW7/gyX/kwN/kin/RoP/qhs+a1l
        2v1Z8CsBcN5KAIRbPvhT3U/u/rHq7i8HwCwzS3+lOfDTkvRf0/3LMPjT/d8f/NJYcIPOIfj9lckiAAboAeA6DOmuI7DQZRSyXMdgketYLHGdgFy3iVjqNgl5blOQ7zYVhW
        7TUeQ2k+SBYrdZWO4+G8vd5pA8SV4kb6xwm0eaT/9egOWuPvTSl+RH8qePYQpAkWsgKRiFpAJ21bdrKPJdw5BHWuYSTorAUq5I5LpEYYlLNHKco7HYOYZrkXMssp3jkEXK
        dIrHQqcEZHAlIp2U5pSE1IbJSGmYgmSuVCSxCz8bphHg0vmJP/GKAz/UAKDc9afc8luWsV/Tgz/K9H+90eDPeJn7jxUBMMMkADJkACjXXX9ld38dACZYeOLPSLVdfxGWDf
        5Yk/7/x9xfagSyLOCdT+Yg2G0AYp0YAAYTAIaIABiJha4SAMZhidt4EgPAZCyj4M93m4YCtxkEAQEARQSAYvc5FNBzSQIAigkAywkAywkAxQSAYgJAMQGgmABQLAKgiABQ
        SAAodA0SARBMwR8qBL8IABb8uVyRFPyRyCEICACIpuCPoeCPpeCPRSYHQBwBIJ4DQAj+RMpm9ABI4krll30k8OO+0xXHfcWopv+LrN71Z9r9BQDM0Zj7N+3+G3Tp/0QD9x
        9bYwtG2m/hAFirWgIkCgCoEUsAiIrTbv7Jl/4M9vybbP5ZOvdvqftXxNx/WXb9/UeCXz4P8NiPPvBoOQzxrAFIAEii9J8DwIUBYLQOADkiAHINAFBAACjkAJitCoBiAkAx
        AwB3fx8x+P3o/f3p4wIEiQAQgj+Egj9E5/5LSbkiAJZwALDgjzIK/iyj4E/Quz8phQCQTGIASDQBgGhTe/5lm36sc3+1pT8t919tsftPMHB/iwDQRg4A+dKfpe5vYtefQ1
        l2/Vnb/BtpZumvnI/7rvsfBADLABr9GYhBT42jYBiAeAkALsM5ADI5AMYSAMYTACYQACZRBjCFgpMBYLouAygk9y+i9L/YTQKAFPzeQvBL7u/qS4EvBH+RgfsX6NyfBX8o
        d/+lMgAskbn/Yg33X8jTf2P3T+Hun8zTf6X7p2mc9ist/ak3/5jMuf8CDff3NHPVl7H7l5gc/Bkvc385AFgTMM8QAM4JlAGkEwBiCACRcWXc8288+aff8x9dtqW/CVY0/+
        7Brr+6/5GlP7UygL388a0ZiHEeiATnwUh2GYZUlxEEgFEEgDHIJgAsFt2fA8CVAEDurweAhwAAlgFQ8BeR+xcZub8AgCI3PQAKpQyAAFAgc38h/ScAuIQZuX+OgfszGbt/
        vMz9CQAN9e6fLLp/Aq/91dw/0+R5f6EVMvizymjwR7HpRzX9N27+seAfU2MrRhAAprfaVbJaCwBtLQGAhvtbNfdvOPk3o6zuH2n5ZR8mBn9qW3zV138fAMIpQWF49Usv+D
        cdhmQnAQBpBIAMVwkA4zgAclQAkE/uX0AAKCAAFBIACg0AsJwDYAGKWPCL9b/k/obBb879c2Tuv0jn/jEa7p+g4f4pCvePN+n+WSbdP1Bl8MfXgrFfT6vGfk3P/evTfxb8
        SgBoZgBt00QAsJN+LRn7VZv719r1Z8nY73Trx345AKy96kvr0A+b+xsBoNWv/pjWdgwFzBACwHAFALI4ACZwAOS6TiYATMUyDoAZIgCk4J8jA4A3F3P/IlcJAIL7F3KpAS
        BEDwAL3V+X/svcX0r/U1XcX9/517t/nLz2r1+egz8FpXJ/teaf2tivYfrPgn80A0DVrSIAzhMAzhgAIF4EQLQlAIjVz/37WTD3b2LXn+bc/+Qo7Zt+K3LXnw0AiuVAp84h
        6PP8FCQ7C/V/GtX/GVT/6zMAwf31AJhOWQADgJD+KwHgJQJgnhIAbhIAhOAvYOIAYM4vpf8CAJbKALCElKPa+S8P97ck/Tdf+/vLNv343Ef3ZwAYTgCY1mq39QAw3vUXa3
        7XnyVjvx73Yc+/NQd+WnHT738t+OVZwFfvz0Gs+3CkOssBIDQAOQAo+HMp/WcAyDOT/ksAKBIBUCgCQHJ/ffAHicEfLHb+Tbi/UfqvbP5laDT/klWbf+my5p/2ab/hqu6/
        RMP98zTcv6h0m34Mjvu2xP1HO26zEABRiXerhpu+7EN715/K0p8EAGv2/Jta+rPU/YdV7Ik/df7j7i8HwDPf+SCg+WikOw/nDcCFBun/Eu7+BABK//PE9D+fA0At/dcHvw
        AAXxEAYvCLAMgXAZCnW/pTAsCw9ldf+jN2f2HwJ0nF/VM1an/5ZR/Zqpd9GJ/1rzb2W6Ax+LNcdenPQ/XEn7Ua5/2J7l9T2/1HEQCGVd2GqQSA1QSAZUYAiCMApDIAJAgA
        CCvj3L9XGeb+5ef9lfWqryFlvOrLxGUf//Xg5/MAbCz4zyBMfnwiAWAE0sn9F5L7Z0sAcJ3E3T9XrP8ZAPIo+PPJ/RkACkQAFKq6/wJe/zP3L5C5f74IAEXw6wAQoQDAYj
        NLfxni5J+x+5se/LHssg/LB3803b+O8sAP5a6/NUbHfSvP+1Pf9GPo/iz4LQXARAUAWPCbmPt3UJv8s2bsd6aF7l+Wpb/SXvbR2+b+8l5A11dmKtL/bJ7+T1Ck//r6f6YI
        ACn4DQDgqk//C90kAAQYpf95Gs0/pftHKVL/bIsHf1KMBn/iVNw/2ir3t2TTjzWDP2vKNPjDAeAoAGCk43YMJQBM0QKAW3yqXduUrPE1IhPuVA2Ltey2H18L5/5V3N/R2j
        3/4zX2/I+y8rjvgWV3/zrd//8Ev1QGvP/ZPMQ3GqNL/zkAXCeqAEBI//NdBfcv4MHvyQFQyN1/Hnf+Qh0A5LW/HACWuz8DQLbK5J+a+7P0P1mj+afu/uVx2Ue+yU0/Wqf9
        Wub+G1Tdn0nu/noA7FEHwCNJC+1aJS3s5xgRf8Ve7v4hsvP+zM39G172Ycnkn7nLPsrL/Utz15/N/XVTgW1+CUBAywnIdBEAsMh1gggAIf1f6jqNADBDB4ACBQBY8HtzAB
        SS+0vBXyACQEr/83nwq6T/LkL3P5cyAIX7u0QpOv8693dWc/9Ek+4fb6H7Kw/81HJ/c+f9FRm5v1b6P93s3L/24I8EABb8AgC2iwC4QAA4a6d4PJ2+yO7ZjMXvOoTF7bMP
        jdV2/9IO/pT1uG+rmn9Wjv32/f899mvJVKBzpxCMfno6sl2E9H8RBT+r/3UAIPdfJtX/rrNEAMyVAWCeAgAFHAD6+j/f1Tj9V7p/mKb7m970Y675p1/6i1Vp/hm7/6JSXf
        Vlya6/WZrNP9Mn/hjX/lL6v00VAKvUADBozQa7KZu3164RFhdcOST2TsUf923BXX8TSrnnf6iJsd8BVp73ZwOA2AwMxc/velL9P05Y/uMAmMwBwNxfAMBMEQCzKQuYIwLA
        0wgABaL7F7iy4PdTcX8BAMrgD1fs+JPX/ubHfpMUgz9JGoM/sRYM/oRZO/hTV6P2N9rzv8roqq8ZFrr/BBPuLwFghOMODCEATG61lwNgqSEAuq1Ya/dD3nK7p9Ky2zuGxe
        dUCY69rer+/mUc+7X0wE9ze/7Le+nPNvhjdjXghW/9EN90IgU/S/8ncQDI3Z8BgLm/vP4vUEn/JfcXAODPa/98g6W/ZaSlLtLgj5T+R5is/bXcP9WE+5ua+zd93h9L/3M1
        3F+66SdfdfBHy/2tu+pLPvizWXPpTwh+CQA7tAHAHgCYHno2bVG7OmEJAVUCY05UC4y9Ru5/kwBwk4L/JgX/zeq+sTcp+G+S+9+k4Bc0j+RN8oq5SQC4SQC4ScF/k4KfFH
        PHcXYsuGaRZhIcZoiaTpoWQxAgTSFNJjhMEjWRNIE0njQumiBAGhOFWqSao0mjSCNJIwTVGk4aRoAYShoiarCoQaSBpAFMBIn+pH6i+kYQCOhlH1JvUb2YCBQ9BdXpIaq7
        TN3uj+rqFMbP77sXqt09DE3+DIZnu2kUiAwCkwkALP0X3H+pLP0XMgDB/SUAFOgA4KNI//Nl6b8SAMrgl9f/evePVq39lbv+kiwe/DG35Vdr7DfQxNjvArWx3zoaY7/i5J
        9a7T9V5aYfc4M/cvcfThrMAPAIASCAADBVBQAFJ07ZDVu70c7O8RG7D7Pzar+WvuT1eqGJ/e0DYicTAKYQAKYQAKaQ+08hAEwh959CwT+Fgn8KBT+XoxdpLmlOtKC5sZPr
        9J+1rP4vI1D/15Go/wvpZyb6/5/o5Y8jdGrwA+l7puFo8B29/Ha4KPr3NyPQ8Ovhev1P1FekL0V9wTRM0OeiPpPpU0FOnzANhdPHQ+H8sfjyI3opVoKLjwAALH5JREFU14
        cyfUBvN9T791cu7w9Hm9fGov1LE9H+5Qn3RC+8PBHTW02kYJzEg38Jd/9pOvdfJgFA5v6C5lGAs9R/gQwA+to/Txf8QvrP3d9Vy/0jDdw/Rub+WmO/xnv+rd/1t1jjuG/r
        xn7Nu/9qs1d96Qd/Npoc/JG7vwIA/hoAkLKAAavX29m9+pVdnxUlDzUMS6pU1T+mEgGgEgGgEgGgEgGgEgGgEgGgEgGgEgGgEgU/F7l/JQJAJQp+entCJfqElZu4vzakef
        Un0Lx6e8vlUM4y/Lzi/7eQvb6FKOn1Laob//tB0cPVO+A1+4/wZdU/8UXVjhWuz0l9HHsgyWmMavOP1/+u8vrfUwx+bwUADNP/fDH9zzNyf/ncvzL9l7t/lkn3Nz/2G2/B
        nv8wjaW/QFNXfVns/uau+jJ33Lcy/efuX8PY/Yc77uQAmPTIPtMAKM9HreHzGVEqN3N9bWTLym3xcJV2NpWTWlRpiw72r+CT6n/gs+p/k/6qMH1C+r1GV4Q6jeTBLzX/cg
        2af3kq6T8DQKEi+EX3d9Uv/UkAyDNI/3Nl7q8DgFHwG+/6S9ft+jNu/pnb8qs++CN3f+OxX+P0v8Bo6c/LYNdf6Wv/DaVy/2EEgEFVd4oAuHhvAFBbAoDb6yNbVn0cLe1t
        Ki89TGpj/xTerf4tvqjeqUKD/1uHzvCsP4R3/oXmn5T+T9et/bPgz1N0/027f76r3v2F1F9K/0N16X+ugfvniO6fbQQASzf9pFh84k+EGfcPKsVpv6bc39Tgj7H7bzI48F
        Pm/iq1Pwt+OQBWEgByp567dwBo7v7GyEeqPoFH7NvbVE5qac9+nk/gxWrv4/MKCv5PSV9QdjG+bn9ksqO/zLh/nor76wGgb/4JwW8IAOP0X+n++qW/bMXgT6zG2G+iauef
        A0DF/WNMDP6Yu+rLT3XuX+uyD5XBH5Njv/qlP7Wz/s0t/Q0Xg3+Ywy4MJABMfGT/vQNAjRELRAC8NbJV1fZoZd/BpnIUg0D7ai/jo+q/EAT+Lvfg/7T6n+hfuzeSXFjdP1
        EHgFxZ80/v/iz9nyMCwDD458vc31dW+weIzm/c/FO6f4Ru15+h+2ea2PWXYvXYb5bG4E+O6q4/fysGf7Sv+lqlcdWX9pZfS9zfEABD7wcAHu01x649AaC161sjW1d9Eq3t
        bSpPMQg8av8M3qz2ZbkD4BMK/r9qdkMEq/vd9Ov+SvefYeD+xs2/Qs3mX6CB++vTf9Xa3yVKVvsrAWCJ+ydpun+GgftnKS/7MHJ/cc9/vfJ3/xkm3F9t049599+hC34BAL
        vuLQDyW/e2Q/87ld+u+erI1lSvtrF/2qZy1VMcBM9Xe5fc+o9ygwCr+39w7Iz5DYaKQz9yACiX/qTOf57o/vkcAFq1v5j+u8k7/3r3V2v+GZ73Z1z7a+z6MzH2G6ex59/c
        pp9gK8d+Ldn1N1Mj/Z+i1fwz2PWnNfZr6P73BQCHXbrY4Y2DlT90eHUk+4NlbmVT+YpBoH21l/B+9R/KBQAs9f+SPs+UegOEQz/ETT/q7j9Tkfqz4M834/7y2j9P4f4hBr
        W/bOlPNvij5v4LzWz6SSrFWf/mrvry1zjuW/W0X3HP/1zVsd/SX/VlifsPcxAAMMRhNwYQACbcSwDsc+tqh3cPVf7Q8bWRbao9g7bVnrWpnMUg0Jb0erXPyq0ZOKx2H6S6
        jNEd+SVf989VcX+W+uubf96CFLW/D72fBABh8EdZ+wfLan+1sV9l+p9pNPYbb+XYb4bq4I8y+OWTf6Yv+6iYsd91Kkt/ytpfDwA199en/0NEAPSvuhvjCQArCABL7hkA3j
        lQ+SPH10c+Wu05tLOpQvQowfXZam9S6v57mbIAlvr3qNUDcc6jyPXHYxGr/XVn/gmpv+Guv3y1pT9d8C+Qdf59xbHfQBUACO6fK2v+5ai4v2LqTzb4o2/8mbvsI1236y/G
        xHl/6kt/uSrHfSs3/WgN/niWevDH3KYfefq/3WjwRw+A3RisA8ABAsAlAsD5ewGAbhwAHzu+MbJttefxmE3lpnYysUzg8Wov4L3q35UaAKzp95tjVwQ1HMav+xJu/NG7/1
        I3g6k/lV1/0thvoYH7C51/P13tL2/+Gbr/Elnnf7EMAHL3X2iR+1s29luW4761rvryVh38WVmqsd9JGoM/Yy0Y/JG7/30EwD4CwJsj29Ef6GM2lYPUYcAg8Er1jwgAf5bK
        +b9x6IRZ9QcJZ/2Jx33luMlHfgX3z5MO/dDt+ptrQeffV7fpx3jwR+7+puf+s8p41Ve8bNOP2uBPad3fx+qrvlarXvVlftefyuSf41bVsV+5+wsA2GMDwH8ZCqwUeKr6a/
        io+q+lGvYZW7cfMsSrvqTgzzU47muZ/Lw/cctvvsqmn0JD9xdrf2X9L2/+yYPfuPmnXPqLs3jsN8lM7V++7l9g5divivvXKh/3Hy4O/sjTfwaAflX3YNy9B8Deyp8QANgf
        6uPVXrSpnKXMDF7AW9W/sqoZyNy/b+2eSHIZRen+eOOjvnXLfvLz/pTuXyjb8qs69ivb9adW+2s3/wxrf3OXfSSVaew33NSmH5Xjvi0f+7Vk8Ed97FfL/Y3Hfo2X/uTuP0
        gGgOUEgJx7B4DdBIC3CAC2YK1oCLAs4IXq71Jgd7TY/f+swYZ9hlPwC6l/ju6mnym6q74Uwe9meOCH8sQf5dSfn+quP8Olv1yVuX/DsV8dAJwsO+8vSTzrX2vpL6qB8eAP
        d//62pd9+Gte9qF90eccjcs+TA/+GM/9C82/LZYt/Tkq3X+QCIC+HAAH7w8A2tkC9R5A4Hk8We0VKgPMzwSwMd8fHDrDp8FgHvw54jXfueI138z9l7kJN/3kye76KxDT/0
        LdWX/6E3+0tvzqd/0p3X+ZmU0/6rV/nOZVX+run16qa76tH/zRp/+mrvqyxv0nqA7+SAd+KNP/4SqDP3L3H+iwlwNgrA0A//1S4M3qn/K63pTzf+XwN6bXG4BFVPcv4Vd8
        s+CfSME/Wbzldyq/6Vd504/8sg/hqi/dcV9GJ/746c77Vy79qbv/Egvcf6EFl31oAUA+9x+lMvcfZuaqL8vcXzn2O9ek+6+x2P3HWzP4Y7D0J7j/XgEAVfbaAPBfhwBbEX
        i++lv4zEF9JuBTUcNq90aGyxhK9YXgX8KDX3D/Za5S8Cvv+VPe9ONp0Wm/BW7y1F+/9Gfc/Isw2fzLNHXZh1Vjv+bdv7THfQvubzz3b/1pv9q7/kZrDv6Ydn89AA6VrPC/
        TAC4YAPAfxUAHaq9hA8cvlXNAth6f/eaPRDvPJIcfzzXUl3wT6agn0JBL13yOcPgos85ojxl6b8p9zes/YPMuL+w9r+IAJCt0fzTu7/l6b/lV31pjf0us3js10vh/pak/+
        Z2/W22fM+/g37t39D9B1bfiz4EgNEPH1q1IvByexsA/uNlwGvVP8IXDsqZgI/ZsE+NLghpOIyCnQX/BB78y7gm6YI/n4K/gIK/QAx+42u+PTUv+yiUn/WvcuKPcstvmMGW
        X+b+kWbGfs2f9Z+oufQn7PqLNHHVl6k9/6YGf8p21dc6y6/6MuP+QzTS/wHV96FP1X0Y4nIgfX38VfdFk20A+E9nAc9Wfx2fOPysKwOkYZ+59QdS4I+jgJeCnwW+3vkLKP
        UvUABALfg9dcFfpAt+4abfQoPa37rBnyju/osMO/+lOu8vTdH51xr8CdcY/AkyM/ijWvtbdNWXcvBHrfafqLrpx3jXn9T8G2Zi6U8K/v7V9qGv/b7LgxocGMJO/s4eb+sB
        /KcB0J5evuvwFZUBQs3PZgPG1e2HHNexPPiZ8ij487ko+N31wV8oBn8huX8RAaCIAFDEg19+zbd00Se74ddH1f0LVNx/meppv4a1f7TS/Z3lgz+ye/6cjMd+Te760236UR
        /8CbbK/QvM3vRj1v0tGPwZpzH4o7XnXz74I3f/fvb77vavsT8z8JuTD4f+fMoue4INAP9R6WcCXqrOzgn4nR8aOrROX77cl0+pfz45Pwv8AnL+QvcppKmkaRTQ00kzSB4o
        psBf7j4bxe5z6N9MniQvkjdpHmk+aQGKKfCLKfC5KPiLKPgLZbf8FlDg53Ppb/ldRoG/lEt+2WcUcvg13+ymH/0tv1nO8cgi188kLaTAzyClU+CnOSVT+s+UInb/UwkCaQ
        SAdCRS4CdQ4MdxLSQIZBIEsggAWQSAbDH9X0RaTBDIQRgFfigFvtD8W8oVRAqkwA+gwPenoPcj+ZLz+9YthA85/wIK/PkU9PNI3uT83nzpj9X+K0mrCAKrCACrCQBs7HcN
        AWAtqYQgUEIAWEcAYGf9rydtIAhsIAhsJABsIgBsovR/M2kLQWALAWArAWArAWCbwWm/5gd/WPD3r7aXBf/lvjX2LQr64eQL4t0fdvfkYQPA/c0Cnqz2Mt6v/j261ezFU/
        x1jVhnf4Is+KcaBP9MXfAXu4nB7z5XO/jdfMj9fUks8GXB7xaku+NPEfyuYYrA11/1Fa275FMI/jjxqO94ReBnUODrgz+F3F8f+FLws8CPVwS+FPzZYvBLgS8F/xIx+HP1
        gV9vmS74/WTB71NXCHwp+L3F4Peqs8Io+GcZBP8MMfin8eBfz4NfCnx98G/mwS8FvhD826Tgv0bp/w4K/vWkjRT8G4dz7dxIwc9Fwb9xMNeejYOYHPdu6Oe4L2tUk4NDQ3
        852XxTytWH8r0u2t2TBqANAA/CZODzeL7ae/it5o+YWP9vjKrXEYPq/IqBkur+RvpVEH/db1wDuH7HgLqkOn+I6ijqT5n+woDaf6F/7b/Rvw5TJ65+dTqjX22mLjr1rd2V
        q0/tbjJ1R2+uHuhVqydXT65e6KFQb3SXqVutPqS+6FqTRC+7cPVD55qS+qMT1wD8LeqvGgN1+rPGIK6OXIPxh6jfawzR6TdHpqH4VdQvjsO4fnYcrtNPjiO4fmRyGIEfHE
        aKGoXvRX3nMBrfVR+Nbx2YxnB9w1R9LL5mchD0P4dx+F/1cfiKazy+1GkClW/j6H1nbB3kuOKL0Y672pD7P04AeJwA8Di5/+PDHHY9TsHPRQB4nIL/8X6V9zw+4ZGDjxZ4
        XnRiNf/iyeftDq27aXdPHw8iANpWeQ6PVjJQ5efQruoL/9EVgRfQlsqBVvZP45GqJPun0NK+Ax62b48W9k+guf3jpMfQrGo7NGWq0pZetkWTqo+iSZVHhZdGavNAqOkDp9
        YWq5kValK1Jf3unlz7R/WQx7s6pNgRAEg77AgAdgQAOwKAHQU/FwHAjgBg17/KHrvxDx+kWv+c3Zn9t+22L7pmd88fDxoA2ju8jFfdP8A7LT/H2w8Leoe9bPEZXmj49n92
        SbCduFtQOkGIHc/WWjxPkB0sKhw1/gQ/ZVi6c+Dhqo/Z9ECIAE3QbWP/fEnH6uEdfqo+3+5f83iQAMCc/5VG7yPGNxH7dx7Eri17SHuxe+s+7Ny0G2O7T6H3eR6P2//39g
        dIAOAQsFeHQCvdnQNPyGBg04OgFvbt6Hf2gg0AZRFL819v9jHyM4tg+Lh9+w48x/qiLb3PY/Yv/L/IAh4VTxbWQ0CeDShhYNP91cMEgLb2L9oAUHYAfIS8hYVGALh18xbm
        jl4gAuC/elaAIQSe1UFAOF5cDoInbRevPEB6hEqydvYvEQAiCAALbACwAaAsWYAeAm11x4sbgkAPBJvut54iCDxBf5evlPxZParDz9V9bACwAaB0qwGGENCD4FmNeweetu
        kBUGvKAh63f5UyAAYAXxsAbAAoGwQMG4M6EEiyt+lBUhvKBJ6wf62kowMBwOFfBID9DABv2wDwYEPAGAbtOAies+kB0aPVnkb7aq+X/OkQTQDw/xdlAO4EgPf22gDwQEJA
        CwTP2y5beeAufnkaHaq9SRlAzL8LAFHP9LJD6JXKr9V8fWTbqs9X+Dq/qSW88gBAuyoUHBX8fdwfELygeemITQ+CnqPf2yslb1Xp2eGNKp3/PQDo9GV/tvOocvvaL49sU/
        mZivljpqB/qtar+LDt13i69muaAVpWADDAvN7kI7zR9JP/xKyA7S6Ff5fa2j9X0tiudYemdo/+ewDQ7Yt+HAAdCACPVn62Qv6QWcA/W+9N+E0N4dN8HRxfwWMqc/1lAcCj
        FPzvPPI5ohbEo8tnfVFR34sNEjaZAkAzu8c7NLd7wgYAQwAw50+PzMLJo6cwpttktHd82QgCpQXAo1WexdstP+Of/+ypc+jxv4H/bwBg04OjdvbPcwC0sAFAHQDJoRk8mE
        8dO41RnSfiieov8Zq9LABgz/nthz9DSlgGbt+6jUsXLqPbF/1tALDpvgCguQ0AWgB4nQPgn3/+4QF94shJjPh7At/9J0HAUgBIm4F48JPzJ4em87ezx8Xzl9Dty4oHAHvO
        7LmwvoMkltFY23tgH8O+7zaVniE9q9koZe8n/3qGfRT2MfxtbNt0FbW3Pa/69naGr7c37t/Iv1fDz634OVR+TrW0M/V3wX5P7HtnLy1t3vKfheznrvXcTb4/fS32vZVnr8
        gGACsAwB7HDh3HyE4T0EGEAG/iWQgA9lzfevhTJAanceeXHvcCACxzebb+G3je+S087yTouYZv0utf5M9PCOZnTP5xsbc9SgH/hMNLeLXx+3i/7Zd4p/Vn/POw586/T9n7
        P0UZ1AvS16OXT9d5Tf92+nl0qPkKnqePfYHe/gw9N/YcdVusHV/in1f62OcavMm/ru77oNezz83+/Xh15fPsUONl/cc6Ce/DP7e99HPQv42JPU9LArjNQ8/iyVqv4I0WH+
        P9dl/irZaf0v+/yl9vEgT0ddnHyb8m/57489JoQNd+VfG7Yt/rk/TzYj9j9jto/dAzwi5TGwDuLQDY4/ihE5QJjOd/TG3oF2EaAD5CgLHgbyEEv+T8CgBUYAnAIPVq4w8w
        d9wCpMVkIjkiDcmR6UgMS0XAzFDMHOaFgb+NwKcdvhUaQwaBrHPVas/j6xd+gdcEXyxOXYo1hSVYvnQVkujzDPlzNF52e1e385Hpr096IiUqnX+tlKgMjO0zma+sCDB8Dn
        9+3ANxQUn0nBZiwbRA+vj3Rcd7Hl8+/yOi/eORQh/LnrPXBB+82vQDHrwLpgbwz5cavRBh86LwWYfv+Mexz8u+/pfP/YTQeZH09gz+fvPp/V92fY//rl5xfx/ek/2QSl8z
        RXxevb8bbNKN2d8Cg8pf9HyD50QgL7uQf++Fi5cj1CsKXT7vwwOWPweVj2crVd2/7o+E0BT+s2A/f/bcg+aG491WnxsFMusP9ftpKJLC03Tvz36OYV7R8Bzjg7E9p+DnN/
        /mYGMwKMuMiQ0ApQCAlAkM/2s8+wHitaYfmgRAq4ee4s7PPtfNG7eM3q+iAcCcnTnWhlWbofa4fecOzpw+i42rN2P6sDk8WIzSZvo+WQCw97l9+7bR5zh/9jwFbALeIVd8
        VCwLRnaZgLv/3NG9T/HSFXjJ5R3upq3snobnWB/cvXuXv+3Q3sMUyN8LmQgFzJC/RuH6teu6j12ZtwZvtfqEn72we/te3ev/of/G9pisS4+ZG3f9X1+cOnla9z7s/VnPpa
        Xdk3iv9RfYsXmX4rn7Tg7S3LLNXseCf1yfqTiw56Dqz+/IwaPwGOXFsxS1UoaBM8ov3ujjLl26hK5f9jUC7iOVn0KgR5jq3x173ZXLV7B3537EBydxIEvfuw0A5QyAp2rp
        m4BqjxOHT2LwH2PwZstPsCy9QBUAHsO88UaTj5ASvtDI+aXHhXMX0e3zfhUOgJLl62HucfHCJfh7hPB0XUpr2R/1h4/9D+tWbjD5sbdu3YLvtCA8VedV7rZd6I+bgUV6bN
        2wg/8s2B88S/GjfOP1/ZWjJ9Hp816U2j6N1pWfxtQhs3Htqh4ALAt43vUtvNroA6MATghNxbMN3uDPlwGgy5d9cOzIcd3b2ftLAGCOu2X9NsXHz58YwDMStSBibtz1q744
        dvi4ye/93JlzGPrXGKNgZN/riy7v8qzB8HGHwDu66yS0NfiYVgQAv+khuHXjlsmv+c8/d1GUuwKfP/UDf/42AJTzIBD7ZQ7+fTR3e63HkQPHMGPIXMoAjA8EuX7tBiK84x
        Drl0zOf1P149kfQeGiFfig9VcVNg2oA0CxEgCsD6GWkRw9dAydPhOCkafFVKfOGu3NA1x6MOe+RLC4euWq4mMPHziCb1/6DY/YPYUvn/0Ru7fp3froweN4v/WXaG3HUvH3
        sCR9mQyCFzC88zh6GwGgytPwnR6s+Jn5EFiYk77e5ENs37xT8TVZgH/Y7n8UbBoA2LSTH8+mA8A6ywDAwPdcg7ewOCVX4cbs58Cgbfg7zc8u4mYg/5tkP3v2NTeXbFX9/X
        tSWfa444uqAJD/btjPm/3/3Tt3jT7H/MkBvJ/yWCnOn7QBwAwE2Nr/qC6TNCHA/jDYOv7p42eMU2sKMPa2a1euaQZ/EQX/l0//VKGjwGoAuHP7Dk+rfWYEYW3xOkV2wlL8
        kLmRPPBZELxEDla8dKUiCDau2YIBHYdjxoi5VMJcVHzeCX2moVWlp/BOm8+xuqBEXyZQkP/2fmeCAwVia+Xbrl29hpnDPHmQPln7FV6bs58PB+n165jUb4bQb6EMwhAAVy
        5fRe/vB/OyQ8g8ygcA7Of21XM/48Dug4qSIydtKbp91w/xIcmKn9vJY6fw50c9eAkkfQ4G0W9f+R37dh5Q/Rtg/Q9WOpgDwOH9RxC+IIZnQqzckj/Y7+Id+r7alaIpaAOA
        Bd1f1uxjTT/mYOX1YEQvyF6OL5/6scKX/9QAwP645k8JQGO7tvjq+Z+xYfUmxfNjzb03W3zCa3V2wOnBPYcUb/cY4YXW9k/jZbf3sHb5OsX3xRpzj1V/Aa80fh+LyD3lgT
        rg9+FoYdcenz/9A7aUbFMcnxY8O4IHzEuu76JgcTEPN/ZgZcSA34bz5/KGCgDu3v0H/jND8ASVFa3LEQAsG+n3y1CcOaUvY87Sc+n/yzBQ0OCbl3+l/z+nzwxu38KorhMV
        ZQD/HD8PVfQk5I81RSV41f19hXurAWBN4Tq82fZjPFH3Rf7zleDIsycCMMvYtJqQNgCUQz+AQ6DTBBwrBwjw4M8qxv+e+YUHf0XvAVADAHMu9kf2SKUnKbV+AeHe0bqGHK
        /X123nQcobZ62+pBr9lMIFx/eexp87yw6WZRUoMiLWsWZdcZaWhnpGyUoicvIBMwgAT+DPj7vj4L7Dip9LRlwWnqz3Cl5v/hE2rdWnzHt37MfPb/3NM4c3mn5sBAD2NVl/
        4rXGH/KAKy8AMOCM6DKelye6ku/QUXT+oje9jTIc+pzHD59QfK6JfafzhqkcAFMGeBiVStLjAIH1/TZfKjJANQCUFG/AB499hYftOuDvT3ry8kuesY3pMZmN9doAUJFDNL
        wc6DzRZE/AbPBTDVe0eAUF/8/iEk7FbwDSBkAwWlG9zf5Ipw+dy9Nw3R/m3kP448NuPOjee+RL+kM/qQDAxD7TeYOLAYDVvrq3kRsnR6bhqTqv8bfPGOqJu2LpwLOOSf54
        +KH2fNnw/DllKlucuxIvNX0X79Fz3bNtn+71G1ZuxkePf60JAKm86Cg+3/IEAOtLnJcB4OjhY/zzMwC8+8gXvBEs/7lMoJ+LfGCHLasGzQlXwFXRACb3ZjCUu7caANbS74
        4BgD2nD9p8hcP7jigMhfWh2lazAaCCtwYLEGDTgKXJBFjaVpyzEl+x4K/87D3b/WceAM9gVLeJ/I9RV88eP4nePwzizbz36A/dFADyVADAVhEY4NhS4OVLl3XfP5s9eNzh
        BarpZ+LmTWUTbeOqzXj70U/x03t/4agMsgwMb7b4mINKCwA32YrLSG+0fOjBAQD7nC+5vYtFyUt073Pj+g362Z5SPG/2s3+0qmUAYCUS+xls27BDUQL5zQjG444v2QBwLz
        IBVg6M7jJJERSWBP/y3FX46umfzJ4xcD8AMLwT/aGfvaBw1GF/j9X9oVsNgLqvCek4pctsVUFK1fMXFeFF57fhNc7X6Ge0a+sefPHCj+jzy2Cck9XWrCH4TD1h6EULAOxz
        s6U29nVZPfwgAID93N979As+NKQbIjtyAumxmboa/g4/Ot5H4d6WAGCTbFWBfe+J4akE3det/ruyAaCUjUG2H2BM18lGNaB68LO0f6XQ7a9y7w/+sAgABn/oF85fICiUBQ
        Cv84/94pkfsaVku+7t61ZsxMeUzrMOutFchTgLwOpZKWu4des2FkwO5N19FlhaABCWZI/is6e+5xOGDwIAWLB+9+rv2LVlt+y57MKcsfNxSfz+WPrOeh/P1H1D9/VLA4Ck
        iDT6HDYA3LtMQIQA7wmYKAdYQBRmL9el/ffjuZYWAMMIAK3LAAD2x/pumy+wKn+t3uW37OFjrLnpecaTcRcv8d7AnNHzdWvsVy5dwaT+M3k2oQYA9sfPUmD2uHzxMkZ0nY
        COH3W7vwBgTcCqwr6JPj8O1q0AsLezwZ2Bv4/QDRYJDcyN/Go5qRFYOgCk2gBw7yHwPEHgJc3GIKM7W+f/+rlf72nN/6AAgAXAy+7vYVGKvgZmy4mDO45SQEH+nGaP8kak
        T6yuaXb65Bn0/XmoJgDYBN6h/Yd5psA+hu0t6PFdf96tv58AYM+V/Xwm9J6GGzdu6MpAth/g65d/wfYNOxVr/O+3/kq3J8AGgH8JABSrA3xY6IQy7c9Zif89+8t9Df77CQ
        Be7lR/kS8FSkNErAEW5BmGnWJafPXqVe7WgpPfRXJEOpZl65cVD+w+hG9f/o3/4asB4NCBI0hP0A/HsBJjQv9pOLj30H0FAAvmJxxexIIpgYrZC8+Jvnij7ccKALLeS8eP
        u+syRBsA/kUAkHbbtXd8hSAwkfcE2B/y8iWr8dWzP9/34L+fAOCNMPrcbMKPdb+FNP8yipYtx7GjJ3R1e0ZsFndw9odcsmo9dm7Tz/pv37AD77X6gn8PagBgDcapQz345h
        jeZKPPmxCRjAN7D95XALDf+4tu7yKdvjfdHMTV6xjTazKeb/qWYkDq6pVrGNNzMqTzLW0A+JcBQLc6QOXA+J7TsDgxF1+T8/Nu/wPw3O4nANjnZptkzp05r/u6zPHZVCB7
        bC7Zhlmj5/EhIfaHfPLEKcV8QNHSFbyMkA7EMATA8WMn0POHgchKzOFfm4Fm68ZtvNt+PwHAfuZvt/qMj1tLD9aj6PZNXzxW8wU+aq3PDG5i3uQAtK5iA8A9AQAf0KgqUz
        k5NPtc7ICIl5zfLb/PWQ7P9f4C4GnelGObhOR/sNJjaWY++vw0WD/f/o9y6TSJSoIONYWzGNUAwIDxx4ddMXmgBwX/Td0eDPmo7P1YBWAjyexcAvlmKHai1AftvkIzu8f5
        HIS0rZoNh7FMge2i5FulbQCo+AyAreXLVd67CMvzJJ+yPtfSAOB8eQGg0tP45IlvsXPTbtVVkhDPSHz8xDdGew0kZ2Q73djnkJquhgA4RQBggPn+tT9w5uRZ1a9xrwHAtv
        eyBmi3//XD2TP6eQZ25sHv73bFx09+A89xPorx4LXL1+O1Jh/yn6kNABUIALa/u9MnvREyO5IUhdC50fCZEsSX6R6oyznsX+SHkg7/cxzCPWMQTM+XPdfZI+bhRad3rPpF
        l2YQiE0FstexICgLAFggvNn8Y6OtyNJjcn8PnuKXrDB++1UqE8b3ZTsLTQPgr0964Hmnt7GqYI3qIRr3HABU+rEj30d1m4Tbd24r9kIwELK5CLaNXJ6lsNHnD9t9zc9YNA
        uAJh8r9kqw7zk5Kt0GAEvETueZN9Gfu4v0x3Ke6tO+3w+9Z7P5lmYSzzd4G9kJS3Tjnuzpsq74W80/tWrvtyUAYOvn8k0vp0+dQf9fh5dpEEg62JOdz7c4NZd/nPzBlscG
        /TEKTzd4HRnxWaoHpfT4tj9//qYA8PenPfFotWexYEqA6n75+wEAtgdg1ghviydF2VwA28vASgeTABDHoeXfA/uZsx2C/EwAGwAsBMD1m7K1ZBEAlZ99YO7wY7/IFwgAWf
        FLlLvHKgQAT2PKwFnccXVr0weP8qk8YS/Al6UGAPt5sqyLnZ0nPwiV18THT+KXdzqhrcNzPNU3fLD7GL54+gfdkVmaAPisJz9/oDul3KzRds8A0FIdAMz92QlFSeGpFgOA
        nakwuvtE/rswBwAGHvZ3IJ83mT1yHtpWt50HYBEAvCf46Zal+B7vU+fQ6xth4wv7fA+CWCrI5t8zYxcr/lD27zpYrgB4pJLwPbN/y8/6Y4MqrInV0q6DsB34iHI78ITecg
        AUGm0HflqWjrKf+bShc/hyl/zBNrSwphhL8cf1nGq0Y277xp0U8B/pxqdNAYAFxtsPf4rNBsFtKQDaapwHYLgdmC07SgBg5yTIR8ElMLKfJzt8dGX+GsXPhQGWZTWsvDI8
        UYg1ML3G+/IUXwsA7HfIdjuyrdHnZL0FVkrw34dtO7A1AND/8M+fuYDhf4/ntShrxLz+AOjVJh9w4i9JzasQALA/Ljak0sSuHT/eujBnufKgisJ1PFhaibsBDScd2Xgugw
        c7sro4d4UiA4gLSeLbgXV74umPevCfoxS7DdmjcMkKvNHiIx5ovX8YjMuXrygCpjCnGC+6vKM/m9AUAOhrsI1A7CRhawHgNdaXg459Dvazai0ekc6CbejfYxQn8Jw4dhLd
        v+7HDwRhW5RPHdcf9HH3n7sY32sqB8D7j36pOAXoypWr8PMIxrDOYzG8yzgM+Xs08rIKddDjG3rC0vBk7Vf5czE6EKRoHd5u/RnB4Ul4jPZWwPrKlSvo+d2gUh0TbgOAGA
        wbV29BTsoy5FLA5abmPwDKQ15GIV86qggAsHQ8M2Exev84GHGBSfwwUMXustBUHsSPVhIuL926frvieaTHZOGD9l/xJTg2iitPR+eOWYB2snSUff2f3/pLESzswcZ2WX+A
        bfT55qVfcfTgMYWrxQQkoD1bAhRBYgoAbfg5+c9Ryj7eaJuxOQCEe8fyI8rep2yEZSQfPPY/HsDsLH/2/clnCljfwmdaIF57+EOM7zdNkdVcvnyZH+fNDjX986NuytKBvr
        ePHvuaPwcGGrYUOGPoXH5upHzb8xvNPuYHphgCgO2UHNd7Ci/V9u06oGh27t2xD5/yo9FtACgVAP5Nj/ICAPsDYjvuzp09x51LccLt6XMY9MdI3aGg7HSfmMBERYPt8qUr
        3JVYmi5P3dlJNT2+7o+2sv3t7HfEpvnYH678wTb9PF5NWCl455HPsFnW2WaAmjXSG4/aP6vYe6EFAPY5WNnEQCJNBVoKADbCvbZoPT91h4mNE7OMiJ2881rjD1CyXHkaMg
        MZOzKNzfD/c1cfiNs27sCXz/7E+xEjO09QHKLKviZ77tLvjTVeRxqUF1vWbsMnFMhN7R4zOhWYgYctKUr7CuSPmIBEKhdfL9Uqlg0A/08BoPVgqeXC+EXC9J14Sg2rwdnp
        QCz9NfdgB2ayCzzkf4zMmdhx3vKlPnbQ5+iuE4WbbioLF5ewoSD5DABzc/lBGeYAwD7Py43eR3bSEqsAoPZgZ++PoK/P+hPsiHKtI73kPzd2YQnLGhi0Zo+apwAj+97YWY
        c6ABBc//q0h+6sBD4nsO8w38rMsgNLjgXny4fb9+HXdzqrXuZiA4AGAFjH+dat2/9KABzce5gA8FmpALB+5UbNk41ZU5C5EbuJ5pP23yrSSZaCswlHdjT4JZUuu/Q5tqzb
        LvwxGhxOyZ4rc6ispBz9MuPJ07yWZoHLAMPeHukbJ8swLvPlPfnn4gCgFJmlw9KDHRjKViv45xH34XsM9zI6aER3L0DrL7hTm3uwrckcAHbCwafsFqHr129oHvayJCOPX5
        XGftasF8EOMZE/In1j8VRdWV+E3o8NSMknBS+KX7M5lQDsxiZ2WIjRz5n+Y1+PHd/GDnLt+c0Afr1YaY4E/38JADZVNrHvDOzYuAt7t+/H3m37/j2i58vOFni98UdW/cL5
        nYQtP0Fq5EL+B7eN0vZt5IosEBgUcumPN3B2GL8A4/mGb6mmkux17Pjq8X2mYv2qTTh+9CRvjrFTchmUWNbw05t/aV5eyTrUk/vPpJJhF38OrN/yMYFGuo+QrZuPoLSZBS
        vbKciu3Xq7hTLTYc+B7Ztnc//sLgD2vivyV+PHN//UdfHZ9/rHB92wsmANf/sO+lzs/dnPjDU03ySAZMRm87fxn4OK2Ndn2QovZcRLPNmRZL7Tg/jXZTsa2dDU6RNnuAOz
        JU52LwF7P/Z8X3B6B/HByfzzsBKJfb+sJGAnJUs/G5atvNbkI6RFZ2Ine570eVlmMvTPsfxYM9ZkZSsx2zft4s9pO/2+Nq3dwm9Xig9NwZgek/gtR2UdZf9/BwD2w3rR+V
        2+tPVvE7tUg6X/HRxfLtX9Bmx14Z2Wn/PbiZneIVdky2w86KsJO9hMgUUK1Debf8KbY6yTzY7N/prq7ufqv2nyqDM+19DwHfr6n/HnwFL5J2soB1fY52BOzd9Ogdpe5ftk
        r2PPWfg8n9HP4xM+MSnd68cv1qRshX08+zxMrInJPo5f88V/DsLHv60h/nkJPs/We9Nos9cHj/4PXb7qw1cHenw7gGdLLDuSZyrtZV9D0nP13zK6e5AdKMNKH/37fc7hwX
        4HbNrzHcPnRN8ry0ba13iZ7xxsZ7sctHR7AdgPro3YNPq3qbR7H/hUnnizrEJmrqnW+jxsYo3f4kPix3VZ8MfYTpyR53cHqt3EI3v7o7Ir1Y2yCfn3obLdWjqQU/61dJ/L
        /kX1n4OK2lV9QfV2ZH59Gfve+bKhOjQNv4ZWg07r/djPU+05SVe6264Hf8C2A9tk079RNgDYZJMNADYA2GSTDQA2ANhkkw0ANgDYZJMNADYA2GSTDQA2ANhkkw0ANgDYZN
        N/EQDrCABP/isB0L7Wy8NtALDJpjJnAO3/dQBgj8ccnu/0aJVnr9t+kTbZVOqj5/Ka2T3W/F8HAKKW3cN27Z9qW/W51bZfpE02lUo3SBNc7R6p+q8DwCMPPWnnbte6KhGs
        I30TB22/TJtsskp3SfGklmSidv8qALBHS7sOdm0qP2P3RPWXHOkb+Lx1lWeCW1V+agNpp0022aSqXaTtrSo/vYRK5xGPVXvhEQKAHdO/8sGeOGUA9C+HhxrbtW3Q1O6xlq
        TWNtlkk6ZaNbF7rLGrXWuHDg6v8Pj51wKAPZpT6mKTTTaVXraH7WF72B62h+1he9getoftYXvYHraH7WF72B62h+1he9geVj/+D3XiQQ2Z737rAAAAAElFTkSuQmCC
        '''
        popup_windowIconData = base64.b64decode(popup_windowIcon)
        popup_windowTempFile = 'icon.ico'
        popup_windowIconFile = open(popup_windowTempFile,'wb')
        popup_windowIconFile.write(popup_windowIconData)
        popup_windowIconFile.close()

        popup_window = customtkinter.CTkToplevel(app)
        popup_window.title(f'Convertitore YouTube - V{current_version}')
        popup_window.resizable(0, 0)
        popup_windowScreenWidth = popup_window.winfo_screenwidth()
        popup_windowScreenHeight = popup_window.winfo_screenheight()
        popup_windowWindowWidth = 500
        popup_windowWindowHeight = 400
        popup_windowX = (popup_windowScreenWidth // 2) - (popup_windowWindowWidth // 2)
        popup_windowY = (popup_windowScreenHeight // 2) - (popup_windowWindowHeight // 2)
        popup_window.geometry(f'{popup_windowWindowWidth}x{popup_windowWindowHeight}+{popup_windowX}+{popup_windowY}')
        popup_window.wm_iconbitmap(popup_windowTempFile)
        os.remove(popup_windowTempFile)

        main_news = language_strings[languages.get()]['main_news']
        label = customtkinter.CTkLabel(popup_window, text=main_news)
        label.pack(pady=(70, 20))

        news_text = language_strings[languages.get()]['news']
        text_box = customtkinter.CTkLabel(popup_window, fg_color=popup_window._fg_color, text=news_text, wraplength=500)
        text_box.pack(pady=10)

        def saveChoice():
            with open(remember_choice_path, 'w') as f:
                f.write('popupChoice=' + str(remember_choice_var.get()).lower())

        def closePopup():
            saveChoice()
            popup_window.destroy()

        popup_close_button = language_strings[languages.get()]['popup_close_button']
        close_button = customtkinter.CTkButton(popup_window, text=popup_close_button, command=closePopup, image=icon5)
        close_button.pack(pady=(20, 0))

        remember_choice_var = customtkinter.BooleanVar()
        remember_choice_var.set(False)

        frameDataCheckbox = customtkinter.CTkFrame(popup_window, fg_color=popup_window._fg_color)
        frameDataCheckbox.pack(side=customtkinter.BOTTOM, fill='x', pady=5, anchor=customtkinter.E)

        date_label = language_strings[languages.get()]['date_label']
        date = customtkinter.CTkLabel(frameDataCheckbox, text=date_label)
        date.pack(side='left', padx=10, anchor=customtkinter.SW)

        popup_remember_box = language_strings[languages.get()]['popup_remember_box']
        remember_choice_checkbox = customtkinter.CTkCheckBox(frameDataCheckbox, text=popup_remember_box, variable=remember_choice_var)
        remember_choice_checkbox.pack(side='right', padx=10, anchor=customtkinter.E)

        remember_choice_checkbox.configure(command=saveChoice)

        if not os.path.exists(remember_choice_path):
            with open(remember_choice_path, 'w') as f:
                f.write('popupChoice=false')

def generateIcon(svg_code, size=(15, 15)):
    svg_code = str(svg_code)
    svg_hash = hashlib.sha1(svg_code.encode()).hexdigest()

    icon_name = f'icon_{svg_hash}.png'

    icon_path = os.path.join(icons_path, icon_name)

    if not os.path.exists(icon_path):
        cairosvg.svg2png(bytestring=svg_code.encode(), write_to=icon_path)

    image = Image.open(icon_path)

    image = image.resize(size, Image.BICUBIC)

    return ImageTk.PhotoImage(image)

def showDefaultThumbnail():
    global thumbnailLabel

    thumbnail_size = (150, 180)

    photo = generateIcon(svg_code6, size=thumbnail_size)

    if thumbnailLabel:
        thumbnailLabel.destroy()

    thumbnailLabel = customtkinter.CTkLabel(app, image=photo, text='')
    thumbnailLabel.image = photo
    thumbnailLabel.pack(padx=10, pady=10)

# System settings
customtkinter.set_appearance_mode('System')
customtkinter.set_default_color_theme('blue')

# Icon
icon = '''
AAABAAEAAAAAAAEAIADQrAAAFgAAAIlQTkcNChoKAAAADUlIRFIAAAEAAAABAAgGAAAAXHKoZgAAAAFvck5UAc+id5oAAIAASURBVHja7Z0FeBTX18ZTJJDgEsVLgUJbqL
u7y7/upS3u7u4SJAkQd3cjCYRAFA/u7u6ufb9z78zszuzOrESA9tvt8z6h0Y2c33vOuefea2dne9getoftYXvYHraH7WF72B62h+1he9getoftYXvYHraH7VFejx9e7miT
TTaVQv/qRyu7p7gesXuS/3/4nJgqc4bNt58zbJ5NNtlkoNkkz5E+VU8dOFXJ2a6l3aNVnuXx8698tLTrYPea80d2T1R/if3b5fHqL378TN3X+z5b743BNtlkk6YGPVXr1Y
5tKj39ZGO7R6uy+Hm82ov/TgC0q/o8e/Jt6KV/2yrPHX+08rOwySabzOo6xcvqx+xf+Ivip/q/DgDdvuhn98hDT9o1t3vcpZ398/70DdwlwSabbLJK+0lfPlbtBbsWdk/8
uwDAHm2qPvM51THHbL9Im2wqnSgLiG1q167evw4AACo9UfOlgQQA2y/SJptKKcqgV1Mm3e7fCIDKHWq/PJLVNLZfpE02lRoA6wgAT/6rANDz0752uEsAqPXyyDY2ANhkU1
kAUEIA6PCvAsCPvw6z20oZwBN1X7NlADbZ9P8NAK5+C+yoBqj8aLMPRj5e9QXbL9Imm/4/AcA+PpgDoPUjH4983N4GAJts+n8FAIeYIAEALT+yAcAmm/7fASDaBgCbbPrX
ASD3wim2fGf38661dq6rM+3sipPtKi1PMVaxTEWiCpmS7SoXJNtVWZpgt5sNArm9PeTxh57G45We+e+ockXpWTxm/zweq/aCQrYgsAHgngDgjS2Fdv32b7azy4+zm318b/
XGa7IfJgA8QQHfwUjFMhWJKmRK7kAA6FC5MKW93eac9o0/+3tOq0c+RKtWH5E+1tYjWvpIUEsNPayhFibU3Fitm2moqZo+ROsmH+pfqqmxihqZVhvX99GuxsuqELABwQaA
CgdAo7XZdp9uX2Hfdv2Sj6suT/EnFVKwryWVGKlYVJFMhSklFPwypZTYp0UdcwwLgGM4ib0MFRXC5C8omBQkKlBUAMlflJ8faviK8iEtEDWfNE9QTW+SlyhP0lwmX9ScQ5
pNmuWLWkwepJmkGT6oNV3UNNJUH9RmmkKavAC1J5EmkiaIGk8aRxo7X9AY0mjSqPmoM1LUiHmoM5w0jDRUUN0hpMGkQaSB3oIGiOrnjXpcXvTveWjQxQMPP/oNHqvyDNpV
e57rMZ1sILABoIIAMOnwTrtRh7bbfbdzjWOjNVkD7YuTD9gtT7lLgQ5VFctUJKpQEAW+oHxRhan0fun0Pmn0dlEFovJTUTlP1DLSUlG5pCWkHCb6nItTUGURKZuUJSqTtJ
CUkYwq6cmoypRGSiWlkJJJSaTEJFRNSIJ9PClOVCwphhSdCPsoUmQiqkWQwklhpNAEVAshBQuqHkgKIPkzxaO6H8mX5BMPhwWk+aR5JO84OHiRPElzSXPi4DibNIvkESto
JmkGaXosakwjTSVNiSHRy4nRaPRuL/qFP0vB/5wo0yCwBYgNAGV+LDp3ks/tN1qd1bVqcfLJSsuToRn8y1WCXweAZGMA5JGWiVoqKpe0JAlVckQtFrWIlE3KEpWZSEGeiK
pMGaR0UhopVVRKAgV6AuyTSImiEkjxpLh4CnRSDCk6HtWiSJGkCFHhcRTspNA4VA8hBZOCSIFMsRTwJL9YOPiK8iEtIM2PoWAneZO8YuDoKWpONAW7qFkkD9LMaNSYQZoe
RcFOmipqCmlyFGpOIk2MRM0JpPGRBIAYOP00Dm3qvoZ2VZ9BWx0E9DCwQcAGgHJ/fLljpd23O1e3cShOXlGp2EzwmwCAUfDLASAPfjUALDIAQGYpAJAgKi5BCH4RANUMAc
CCXxMAYvD7ywDgIwEgRgEARwkAc2UAkIKfVEMCwDQZAMTgr8GDP0oIfhkA6vX0QsumH6Ft5afQljKBttUE2SBgA0CFAaDp2kV2zUoW/Wa/IuX0Q5YGf7GF7p+n7v46ABi6
f7Z17l/VMPjjZQCQ3F8NAObc3xAAau6vA0C0AIBZpXf/muPYy2jUGhGKJs/8gkerPIlH7Z8mPaMCAmMI2ALFBoBSPf4B7B4uWWzXsmTxKIcVqbcesjb9LzQAgFrwL1Nx/y
Um0n+5+2eoACBFw/2l4I8z4/5i8Jt1fz9t93fQcv9ZBu4/XcX9tQAwlrKA8TFw/bAvWld/Bm2qPok2HAJKENggYANAuQLAeXUmA8AEAsDd0gGgHNx/kRn3VwDAwP0Ty+D+
lqT/8uCfp+H+WrX/dK3gj1IGvwSACbGo33EqWtZ/Fa2rtEdrewaBp0QQmIaALVhsACgVAOquyrAMABXh/otMpP9m3L+qofsbAEAR/HIAhGqk/1LwywEgpf6m3F8CgBT8pt
x/kob7iwCoOS4Gdfr7otnDH6NVlSfQyr4Dh0BrIwgIPQFbFmADQDkAYGGZAGCx++cmqTf/sg3Sf0vcXyv9lzf/osrD/Y0BwN1/rhn3lwNgikrzb6IY/LL0n2tMFGqNioD7
i7/jkaoMAO11EFCUA4oswAYAGwAqGgCWLv3lmV76s7j5Z4n7SwCwxv3DzLi/n4n035T7z7LA/c2k/zXHRKLW6Aj6/2g4fTkUDzs+xSHwiBEE5FmArQywAeB+AEByf4vT/y
TT6b+s+aft/omm3V8LAPKlP5b+h1ix9Cev/U01/6SlPx78JtJ/g6U/efDXHB3J3b8WZQH1usxG84avoGXVxwkAQiZg1A9QyQJsAWMDQPkDoLyX/nJMuH+mytKfWvMvyUzz
z9LBn6BSLv3NMU7/jdx/mgVLfwr3FwFAL2sPDkLjR79Ay8rtOABsWYANAPcfAFq1v4Xpv8WTf2ruLwEgyYLav4IGf8wu/Unub2rwZ6KG+4vpPwfASPYyCi5vdsXDVR6TZQ
EddFkA6wW0tWd7Bp61AcAGgAoEgLXuv8zK5l9WKdzf1NivqbX/sHJy/7llHPuVADDWIP03AED9nyeiec2n8XBVgoAiCzAuA2wAsAGgYgGgNfZb3nP/GVaO/cZbMPhjbvJP
1f1jrXJ/kwCYpD34o3f/SH3wj2AvI1Gn1wI0dXsDD1MZ0NJeLQt4xjYT8P8EAM3sHuvQ/IEAgNVLf8mWLf2V2v0TLHZ/DoBgC5f+pPTf25rBn2jztb9W80/u/iME1R4SCr
enf8LDldrgYQJAS1kzsLWuDLD1Af4/AKDpfQGARvpf2cqlP5Njv6bSf1Njv6Z2/UVJzb84y5b+/MWxX0uW/lTm/ks19qvW/BspA8Bw9jISTh/3Rwt71gdQLwO0hoJsgfMf
uRaMfpcdqr1c8m21Hzt8Ve3b+wQAw8m/Aisn/8zt+pMP/mQYur8FzT8Lxn4tbv7JB3+8LVj6s2Tyz5rmnxT8XJGo98cMNKv3IpUBbQ0A8KRtOfD/gdrS7/TZ6m+UzKo3rc
OEumPvIQAetF1/yaZ3/VWLtmDyzzD4LZ3799Se/NMMfq2lv/HGgz88+A0BMEwAQJ3efmj+8Od4pJK+DyBAwHQfwBY8/yUAvF4ys97UDuPqjrkPAKjIuX/D2t+aXX/mJv+s
cP/qfmbGfg0n/2aVYu5fbexXSv0Nan8dAIaxeYAwNHmlG9pWfpq7fisx+BWNQFsZ8J/PAO4tAIrLee6/PNzfksEfc+4fbG7Pf4zR5J/e/WNMuL+puf9S1P7DRA0VIOD6xS
i0c3wFj1V9jgc7S/2lWQAGAP0RYs/bsoD/KABm3BcAaLm/YfMvT939K9+PXX88+OOVnX8t9/fX6PxbsvQ3s/R7/o3SfzUAsOAfGk7/H4UGf85Ba7eP+LHhj9u/yE8NZoEv
nwhUA4ANAv8tAIy9JwAoTi6d+y+zYPBnkeldf1Uo+CtR0D+UHA+7pDjYJZIS4vBQQjweiqeXcXGoxLr+8sk/c82/ECuW/jQHf2JKufQXpVn7q6b/w5UAqD0kHLWHRqJunw
C0eOxnEQAU1PYvEQTY8tBzegDY6wGgbAbaIPCvXgLUAWAaAWDs/QOARUt/ltT+BoM/VSj4K6VT0KfGo0pqAlyz0vFk7mJ8saII3deXYNTWLZi8fTum7diBoZs2oV1WNqqa
cn9Ldv0FWLn0Z82JP1Msdf9I7dqfuf8QEQDs5eBwNHqzD7nBC2hLZUA7ggATg0Bbygba8LLgGf7yUcoEBD0vyF5QW64XylHPP1iq9m/Xc0Ziwd+m2jN4qvprJdPrTW3PAJ
DqlGSX0pAp2S65YYpdUsNUu0RSQsM0u/gGTOl20bXS7DKeXmq3Y94+fsnPjTM3LQTAchkArBn7XWblph9SZQr+yhkUyBlJaLEkE1+uLobHnp1Ycuoktl++hCPXruH8rVu4
fucObt29i9v//IOr9O+Y/QfgRCVAlZg4s0d+VS+t+1uy9Gd204+VS38jVNyfiYK/FmUBbt9OwnP138PL5P4vV38Vr5Berf46XiO9Wv01+v9X8JrDy6RX8Lrjq1xvkN7keo
3rLRW9rdPrqnrHrN6wSG+r6k2r9Jaq3tLUm2b1Nt50EPSGFXrd4R2L9ZpO72rqVU3R75s+9j3Hz7YsqO/1/iLn9EYEgBYEAFJyCwJACwJACwJACwJACwIAKb1FTJ30FimP
5jTN+2513dX9Nlf5xK6X3dY5e+2OZJ2sIABYMfhTmZz/oYUJqEH/fqV4Gabs3o5NFy/g7K2buEGBbu6x4/RpNPX1Q5XIGAJAguV7/oNkY7+lcX9zS39TNU78maDh/qNNNf
/0wc9UgwDQorMPhj7aDX4NfoKP0y/wc/4FAc6/Itj5N4Q5/45Il9+R7PYHstw7YjEpt9GfWNroL+STChv/jeWNO2Fl485YTVrTuAtKmnTFOtL6Jt2wgbSxSXdsatIDm0lb
mvTi2kra3rQ3qQ/XjqZ9RfXDTqYm/bCjSX/SAPr/AfT6gVw7mw6il4K2Nx2M7U0GY1uTIaKGcm1tMoxrS5PhXJuZGo/g2tR4JNfGxqPoeY3ChsajsV7UusZjuEoaj8VaUW
sajeNa3Wg8VnFNwMpGE7lWNJqEFe6TsNx9MopJRaRC9ylcBe5TUeA2Fflu05DHNR3LuGZgKSnXdSaWuHogh2sWFnPNxiJStsscZHHNRSZpoYsnMly8kOHshXRnb6RxzUMq
KcV5PmkBkp0WIMnJB4mkBCdfrngnP8Q1ZPJHbMMAxJCiGwZyRTUMQmSDIHpd2K1Up4QTaU5Jh1IbJh2m4D+czJVymABwmIL/cEKDtMMU/IfjSLENMo7ENly4N7r+wkVpT+
f1z3qzuAmFzkNnN12yO7TwpAUAUMz9J1u+62+J6bHfh9iSH9X+TxXkYPrendh/9QrukLMbPv6h110j9z915QoOXbiAPWfPYhcF/o5TpzAudylqTZ2GquFR5P4J2nP/IaYP
/Sj1nv+ZFu75t2buf7i2+9ceFI6agyPQsG8Iur80FBFOv1Gw/4FoUpxrRyS6/olk17+Q6vo3Frp1Ql4jFuRdeHBvIG1u2g3bmnanYOyOXU17YG+zntjfrBcONuuNQ81743
DzPjjavB+OkY4374+TzQfgFOl084GkQTjTYjDOks61GMJ1tsVQejkM55tLGo5zXCNII+ltI3G+xSh6yTSa62zzMVxnmo8ljRM1nj7/ePpaE3CqmaCTzSaSJuGEqOPNJuNY
syk4ytR0Ko6IOtx0Gg41nY6DXDNwoAnTTOwn7Wvigb1cs7CHtLvJbOxuPAe7SDsbz8UO0vbGnlzbGnthayNvri2N5mEzaVOj+VwbGy3ABncfrCetc/cl+aGEtNbdH2vcmA
KwmrTKLRAr3YKwgrTcLRjLXYNR7BqCItdQFJIKXMNI4ch3iUAeaZlLJJaScl2iuJY4RyPHOQaLnWOxiJTtHIcsJqd4ZDolYKFTIjJIaSRyf1Dwc1HwI4lE7g8CAMj9Sekg
AIAAgJgGCxHdIPOfiFoLr0fVz8pc9OGKZ1m8s5LAGAArCQBrRQAUJZdp118VDQDYUfA3WZqJPls3oOTCedw1CHz2f0cuXULhwQMIKFmLEblL8GtSIj6IjMDLwSF4JjAQHf
wCUGfGTFTxnAf7qDjLl/4sPfDTuxSDP9Ms2PQzTsX9zQFgsB4AtQeGw3FwJD79eCL8XX9HODl+hAwCCRwCf3MIZBAEct07YwU5fUmTLjoIbCUAbBchsJsAsI8AsJ90kABw
uHlfHGnWjwJNgMBxAsBJroEUoINwugUDwRCu0/zlUAroYaKGc50hAJwhAJxpPoogIegMBf+Z5qMp0MeIGksaR5+TaTx9/vE4QQA40UzQcQIA0zEKfqajBACmI02nUNBPoa
CfynWQAMB0gACwn7SPALCPgn+vqD0EgN0U/LuYGs+mwJ9NgT+Hgn4OBf1ckie2MjXyosD3osD3pqCfR0HPNB8bSOvdF1DQL6Cg9yH5UuAz+VHg+1Hg+1PgB1DgB1DgB3It
JwAUU/AXkQoJAEwFBIB8AkCeSzgFfjgFfgRXLgFgCQV/jnMUBX40BX4MVzYBIIuU6RRHgR9Pgc+UgHQDACTrAJAqAEAW/HEU/LE8+En1M0HBj4g6mXcjG2ZnLP5k9cNLvy
+x2+q933IAWOT+JsZ+Ky9iwZ+ADkVLEHX0EK7dvaMI/Ou3b2PlkcOYs3IFvo6LQZsF3hR0s2A3dzbs5nnDztcXDwUE4qGgYFIIqoSEUfDHqjT/rNjzLz/w09Lmn4eFS39l
rP0VABikBMCTv3piVrO/Eer0K0JdhLRfngmkUCaQLkIgx60zljcSMoGNlOJvoeAXMoEe2EVZwB6CwF7SAQLAQQLAIQp+DgECwDEKfgaBEwSAkwSAU80H88AXNFQQBf8ZXf
BLABjBAXBGFvxyAJwiAJwyBIBB8B83DP5mQvAfVgl+DgAe/DMMgt9DCH6SFPwCAOYqACAFvxwAUvCrAWCNuxT8/jz4JQDw4CcVcQDIg98YAELwSwCI1gFACn7m/pkU+At1
wZ/Agz+NB78eAHr3lwMgQ+7+FPwiAOplI7xO9pUo95whrBTYOs8SAJTD4A8L/qrk/u+sLkTumVOQe/5tqvfXHDuKvtlZaO8zD7XmelDAz8VDwQGoHBMJ+4R42CcmCkoQFZ
8oLAGy2r+ilv48LWz+Tbd+6a+W1tLfMNPuz1RzUATcuwdi2GO9ENzwF4Sw2l+EQIzrH4gnCCQxCLgRBNyEcmAxQaCYILCeILCpqQSBHlSj9+RZAIPA/mZ9cKBZXyoJ+uJw
s/4UcP0JAgMIAgN1ADhJADjVgkkEQHMBAKcNgv801yguvfuPFoNfAoAAAaX7T1R1/yMy95cD4ICR+8/QBb8CADL31wNAH/x695cAMN8AAMrg1wNAHvzG7l/AU38x+Emq7u
8iub8SAJnOcYrgV3P/JLn7N1Sm/sz99QDIQiQDQP1shNbKQnTjJekH0064b5q5VwMAxUoAGJ33l2/50h8LfnvS9+tXYuvliwrXP331KuauXon2vvNh7zEddv4LUDk2ClWT
4vkAkH1yIp/8q5pAik/QL/upzf1beuJPgMHgjyVXfc3RuOrLmqW/cbKxX1ODP8O03b/2gHDUEvXra8Ph3/BnBDlTFkAQCCcARJFipVLA7U+kuQlZQCYBgEGgkGUCjaVSoA
eVAj2xsxmDQG/KAnpzCBzkEOgnQIBnAQMpC2AQGEQajBMiAE4RAE41ZxIAcFoEgBD8IwUAiBnAaSP3l4JfCwDG7q9M/w3cv6m6++/WcP/tRu6vB4Dk/hvc9ek/A4Au9ReD
Xw8AfeqvBgC9+4cp3N8YAAbubwCANAkAOvfX1/6JGu7PxNw/kgMgWwBAzSzEtVm2alfYkfabPPaZAUBZ9vyz4OcASMSnJcXYceWSIvi3nj6J7ynQa8ycikoLPFE5Lko/+p
ucgMr0tkp88CcelePj+XbfaiS27Gd26c8QAIFW7PqzZPDHkl1/1rq/ieaf5P4MALX7UxkwMBKvfzUNnq6/wd/pFwQ5CRDgWYBYChhmAVkEgBz3zsgnCKylUmCzmAXs4BDo
JUCgKYNAX8oE+gmlQHN9FnBcBMDJFkxDuCQACBAYYQCAkWLgm3P/8Qapv/Xuv1/D/Xdruv9cI/ffLAa/uvv7qLi/v0rtLwHA2P3zjNxfn/4b1f7O5mt/0+6vTP8l92cKqZ
mN+Lb5JbuCjzyzaaYpABQml635R8FvR8H/9PJcrLxwVhf4rOlXfPgQ3goLQpVZU1E5PIiCPkEY/SXXr8Sm/xLjUJP+3TJrIV7KzcU3y5ej7/r1GLB+Az7JL0CdhGRULcOu
P4uv+jI5918K9ze3689E7S8HQM0BEWj51wJMbP43fCkL4BCgTIA1BXkW4CKUAsmuLAv4i7KAvwkAnbBIhEBeo65YzSHQgyDQEzsIALso+Pc0ZZlAX4JAPzELGEAQGEAQGE
gQGEQQGMx1ggGAgv+kGPynyP1PyYL/lJgBnKLA10se/GN58LMMgAHguAoALHd/ZfNPDwC9++804f5b1Ny/kdL9lbW/WvpfHu5vTfpvmftHG7h/eP1FHABxj1oBgMql3PNf
iYK/eX4WUk4eg3xVP//QAbwY7IfKs6eTk4ehakYSD/4q5Px25Pr10lPwQVE+pu7YjoLTp3GAyoQLt27pPv7glSv4fMlSVI6Igb0l7m/piT+W3PRr6P5T1ef+HSdEoPq4cN
iPCUe10eTYYyLU039Lmn8Gwc9Ui9SwVzC6P9UXPg1/gh8BIJAAwPoBEc6sIfg7ZQF/IJEgkEKlAMsCMt07IZuVAgSAXPcuyHPvilWNu3MIbG/aCzubihDgABCygINUBhzi
EBhIEBjEIXCMQ2AIBa4AgJMyAAgaKWqUQfCPofdVpv+C++sBcEwX/Kbd/6Cl7t9E2/23Gri/evPPtPuv1HB/eedfq/knBb/55p88+JM0l/4scX85AHZaBoDSDf5UXpyIWj
kpmLl/F5/ckx6s2fd6WCAqz5mGKvERQvCnJ6Iy2+xDjv9a/lL47d/H5wK0Hnfv3kWvrEWo7BukDgC1XX8BpWn+RVs0+edIwe8wKRL2FPRVx4ejKgV+HSoBGs+Ix2NzU9DS
Iwl1x0eh2khyblPub6r5N0AJgNr9wlCrXzg+fW8svJx+go/zT/B3/gXBzspSIF5cFUilLGAhywIIAosIAEsIALkEgLxG3bCicQ9sbNJThADLAvpgn5gFHCAAHCQAHCYAHJ
EF//FmTEMpgIeJEBhOGiFKAMBJAoCgMTKNFaWs/RkAVINfB4CpiqU/w9rfMvefY9T5N3R/U0t/PPhJqyx0/wKNzr/a0l+2xtKf3v2TZLV/ssz90zQ6//qlP7n7GwJgozkA
WL30JwLALjsBX6xbjkPXr+kC99DFi1Tzx6HS7GlU74frg5/Sf6fMNPTbtIFPAv6jMgx0684dnLt2DccuXUL69u14xnsBKnn7UPqfoJ/7l077tWbpb751u/4cKfgdKfgdpk
Wi2hQK+skR3P1dZsbjiQVp+CxyKQZmr4XP6p1YuPMIVh0+jYL9J+Czaic+CFyM2mwJkECgtevPqPZXAwAFPwcA/bvDj7Mx1f1XzHf6Eb7OP1MW8AufCAwn6bMAfS8g000A
QI4IgGUEgPxG3VHcqAfWi9N+uwkALAvYSwAQSoEBlAUMJAgMoixgMAXpEK7jzYfyLOCEAgAjRVHgN2PBP1oj+Jn7jzNy/6MyABy10v2N1v2bKNf9tZb+Nhm4/wYV9zdc+l
tl5P7BKkt/+tRfzf3Vlv4yufsbN/9SFQCwaPDHqPnHgj+s/mIE11hkAgArRAAUGQDAiqu+HiL3d1qajoQTR3RBfIVS+LEFeajmMQWVIqnmXygEf6XUeDTKSsfcvbv5vL/8
cfnmTaw+cgSeK1aic2oqPg6PwIv+AWjsMRvVpnnAPijc8sEff8vm/uV3/TkQABxmRaG6B7n3TBJzfQJA/VmxaO6VhNdDF6FTxgrMWrkNaTsPY/vpCwSoa7h04xZf2pQ/7t
z9B1tPnMdvMfnc/WuOsKL5p+L+dfoSBEiNuvhjUOsumNfgBywgALCxYGlVIJIUSxCQlgXTxGZgtpQFNBIAkOfeHQXuDAI9sa5xL+wgAOxuKgBgH88CBhAEBuIQAYBDoJkA
AZYBCBAYLmoESR0AJ1QBYJj+T1IAgLn/YVX3nyYEv4r779Yc/FEG/9bGaoM/82Tu72M0+LPa3V9R+68s98GfOI3BH33wpyjcXzn4o+7+WbLgJ9UTABBrEgBrBABUKoP7f7
dhJY7duK4LgJz9e9Fs3mw8FDCPnD+Bb/utlMZ2/KVR8O/CtTv6oSC24YcF/t8pKWgyezal2zNQdfZcVJrvg0p+AagSGAr78OhyveuvOgV+Nc9o2FPQV50dhaoU+A7k/nXn
xqLR/CQ8E5KJb5IKMK5wEyI270PJ8bM4cukqge02bt65azTNqPVYd+gkXpmbDIdhYap7/i1N/xkAmOr3CsGPLw+Bd/3v4M1LgZ8RwLIAl18R7sKygN8Q5yL1AlSyAAYB92
4ooCygkLIABoG1jXtjW5O+lAUwAPSnLGAAhwAHQDOmwRScLAsYynVcAYARmhnACQp+QeMMmn8TjNyfB38z5dTfIavcX0r/Z1vs/hvFpb91KgAwdv8ARe2vNvgjrfvr3T9C
w/1jrHD/ZIX7J5hwfz0AFuncP5QAEFRjMQGgQATAfjUA5IgASLIMAAbu35DcP+LYId0f/ZlrV/ET29vvSYGcEsPdv3J6AmovTMbYHdv4zj75XIDPmjV4fJ43Kk+lUsHXD1
Ujo8W1/wRx6S+hzGO/1Sn4q86LRmWvKFT2jII9BX+deXFo4peMZyMy8VnyMgxYthbe63Zg6cHj2H3uEi7dvMXhxILdsnA3ftyhjx+RuIwCPQg1zY39DjJu/vHglwDQhyDS
NxyvfzoJHs4/wqvhj1QK/KTPAlxYL0DIAhJkKwJCFiAAYIm7AIB8EQBFBIDljXpjdeM+2CqDgC4LEAFwmABwlIsgQCWABIDjYhZwgoL/RLPRggyCn+m4zv3HywAgQOCIwv
2nGLm/svaXAOBhYuxX2fk3dP9NRu6v7PyvVRn8scb9l2m4vzL916/7L9RY+ksxHPttaDj2myG4fwMN9+cAyJEB4KiFALCi+We3KAHvrS3EvmtXdPV7NqX3Tt4elPoH8g1A
zP0rk/t/uXo5TsiyhLNU4w/JWYy606ai8tw5FPiRwsRffBnv+qPgr+obg8o+0ZRFkCjwGQBcAxLxZFQmPk5dhh5L12BWyXak7TmCjafO4+TV6xa7urWP4GVr4DxgPmrwU3
7Ud/2p1/5K95cA0OZXL4xq9gc8G7As4Ef4OAlZQIiUBbiyMuAPXgakuopLggSARQQAqRfAygAJAMUEgBUiBDYTBHbrsoCBBIFBBAEBAExHmg3jAGBZwHERAMcNAHC8mR4A
x3nwCwDQB78+Azhi4P6HVdxfPva7z8LBn22agz9qc/8+Fo/9LnfTdn+15l+OSvPP2P3jrXT/DBPurwcAc38GgEACQAwBYIcaAOotz7B7hJcAKQIArBj8qUTuXyMnGRP3bt
c55JVbN/FnRgoqEQCqpMaJ7p8IJ0r9008cU+wDmFZUiDoU/JXmz4N9XJww+htv+V1/UvOvGgV+VQr8Sv7ReMiXHN6XUvmgBDwSmY53UnLRMXclJq/dgrhdB7Hy+BkcuHgF
VymVv1ePEAaA3nNQY0iY1c0/uftzAPQJh2uXAHR5oifm1vsWnk5CFuBPpUAQASCMFOVKWQABgGUBfI+A1AvgS4JiM5CXARIAemEFlQErCQAMAhub9MOupgPIdQcSBAYRBA
YTBBgAhlKwDsVRAsBRAsAxCv5jFPzHmzGNVgDguOj+2sFvjftPN3B/7U0/pgZ/TI/9Wj74o3f/UJXmX4Tq0l95Dv7EmVn6k7t/iA4AhQSAI89sMARA/eXpdq1WL5rgUGgh
AOTp/6JEuC/LQKKs+bf9zGk87OOFSmH+Cvf/tWS1oum3aM8etPKkOt/Lk4I+Tpz3N3Hev3jenz0Fvz0Ff9WQWFQJorQ+MAa1QhLgFpmC55MX4cclxRi7ZjOidh9A0fFT2H
3hEk5fv8FT+fv10ATAYBNr/yruX6d3GGoTAOr2CsWnb47ErPrfYm7DH8Qs4CeeBYQSACJIQhbQEUluwpJghjgZuNitq74McJf6AL2wXAaANY37YkPj/tjZhEFACQABAsMV
ADgmAuC4FPxcevc/RgA41my8GPxy95cAMEUBAONNP9ONgt946U+562+r6tLfPNXBH+POv5/FY7/qS38RViz9GQz+qI79ai/96Tv/xu4fogNAjmkAPCIBID/J6sGfmjkpGL
dnG+/6M/efVFQAxznTUSU5mrs/O/LLNTsd2SdP6LKEo5cv4QcK+sozZ6BqTLTe+TXcv2pkHKpQ4FcNi4UjgaBhVBLaJlLdvjgf/VeuR/DOfcg9egIHLl/hwX719p0KS+dL
DYBeBIDBYWbn/g2X/uTuzwDAxLKA576cjvFuP2FO/e/g2fBHLJCyAIKAkAX8LmwXduvIASA0A6kMcBP6AEtJ6gDoywGwtnE/rCcIbG8ykMqBwSIEGACGcQAcac4gMIIHvw
SAY3IAyNJ/IfjVAMBSf2XzTwp+4+afFPzK9H9nY2sHf8rL/ZWDP8tMjP2qDf5YtuU3xQL3zzIa/JG7f3C9JQggAESXGgAm5v6ZKi1KgnNuBr4vWoq/stLh5j0LlWSdf3be
3xNLF2OPbNAnctMmNKDgrxwYIO74U7/rjw39VKHgrxeThFeylqBj0Sp4bN2B5INHsOviJZy7cZMfG3a7DE26ewKApTIAWOL+/dXdn6tXGGr2iUDL3+ahf8u/MLveN5gjZg
G+BIFAgkCIM2UBVAbEiGVAMgFA1wwkAORQFiCUAWw5kJUBAgBWEABWiRnAWlIJAWBd4wHY2mQQ9lEGIGQBegAcIQAcpeAXNEoBgGPc+ZXBf1SniTr3P2yQ/h8s09ivAICt
ZRn7Lc2uP62x3zK6f5LG2K/xrj/l4E9ofb37KwFw1AoAWHHkV+VsUlosKkcGoHJ4ANX+sboTf1n635Dq/5BD+3GCILD5xAl8GxsDu9mzhNRfCwDRAgBeyc5F0O595O5X+b
Fhdx/wYLcKAIO00n9t92cAqNU7HE5dgvDLs/3gQQCY3fB7viLgI2YBwZQFsGaglAUkyssA9y5iGdBNVgb0FAHQmwDQl7KAfjwDYABYTwBY33ggNhME9hAADhIADjcTgv9I
MyYh+I9wsSxgjKixOgAc5ZIH/0RF8++QLviVtf8Bo7Ffwz3/lri/SvpvgfsbDf64WTf2a3rPf5zqnn8GgGSrxn613H+xLviZ/GssIQAUiQA4YACAYgMAlOWyj+xkepmsq/
2ls/4rpyaiWfZC/JCbg3ciw1GT3L9KcJDJ4K8SFYeXspdg9ZmzD2RQX6Hs4+TFyxaVGhwAPQkAg0LN7vpTdX8DANRmEOgVjnffHYvJTt9jVgNWBvwgLAk66cuASFf9VmGh
GShsE17ElwO7URmgB4DUCGQAWNWIyoBGMgBQGbCeALCpyWDsajqUMoHhFLgjuPQAGM11VAp+0tFm4wyCXwkAyf21AGC49LfH5NKf+bHfDYr036cC3T/SIvdXDP44mXN/ad
1fvvSXKav9jZt/LPiD6uVyAES1KSrZHnL0mfWqAFhFACggAOQlle7ATwsu+6iUnAC7mEjYBflTPR8iHvqhftlH1eg41I9LRsje/Q9EsN+9+w/OUAay+fBxZKzfjnmLi9Ej
JBmDozNx4ep16wFgae2vkv5z9aQyoHcE2n8zC8Mb/ypmAfJm4M98STCCsgC2P0CYCWBlQCcqAzojmy8HUhkgTQWKAGDzACsb9cFqCv41FPxSCcAyAAaBDQSATU2GYGfTYZ
QJjBAhMFLn/oLGUJCPFaUEwBFd8OvT/0MmOv9agz+GS3/Ggz9e2mO/Fiz9qQ3+FFu49FfeY7+mlv7Uxn7l7h8kB8CjpQWAqcs+sk1f9qG87UfY/FOVH/iRaHzVl+yq76rk
/o+lZ2Hf5Sv3NtDJzdlIL3P3A2fOIW/7XvguW4lBMQvxlWc4nhs7D80GeqBenxmw7zUTb86Nw+krlgOgpiEABmos/Zlwfxb8EgAad/RF10e7YGa9rzGrodAMFAaDfhKzgN
/4VuF4FzYa/BeVAeywEAEAiyUANNIDYIUOAH0VAJAgsJEAsJEAsKnJUGxvMoyC1BgAhwkAAgTGcR0hABxRBP9Eep9JYvBPVrj/AYOx3/0Wu7/pAz/MDf6obfnVGvxR2/Kr
BEDFDP4oxn6N3H+RBgByEUjyq5GLyDbFVgKgPK76Ste46ivJ9E2/rPH3+bJCXDTYK1Dewc42HEmp/MbDxxC9agPGpy7BDz5ReH7ifDQeNAP1+k2DQz8P2A+Yi2pDF6D6iA
A4jA5F9THh+CA4B2eu3rQQALNRc2Co6bn/fuH6dX9DAPRSAqA2e9k9FF+/OBDTGnwDDyoD+JKg2AtgzUBhMpCdFSCWAeJpQVkiAHgfgACQTwAoaixlAL0FADRiAJCCfwA2
yACwsTFBoLFwvPcegsAhHQDGiAAQ3P9I03Fi8E8wCH45AKTUf6qq+5sa/Nlp5rw/w7Hf9WVy/2Dl1J9J948qw9hvqhn3Xygb/BFSf8PBnxCZ+3MAOJoCQJEBAEp7289CMz
f9mrroMy7eCAC/Fq1UjAyX9cGc/fKNGzh24RJ2njiFjE3b4ZlbjK4RKXjTIxAtR85G/YEU7AOmw37QLFQbMR8OYwPhMCEMjhMjUYMd+MFP/Ynih344jI0oBQBCzLt/P/Pu
z9UjFDV6ReCVDydgvMsPmNngW7EZ+AMWUCkQIK0GSNuEXfRlQKZbF30fgAOgBwGgp6wEEHoAaxvJ3L/xIAr+QRwCLPiZNjcWzvbf3XQkQUBwf0FjqbYXAHDYIAPQAcDA/Q
1r//0WDv4Ypf+NtNN/pfv7qLh/WXf9RWns+ZfcP86k+xsP/ugBEKPo/GdqjP0au39gvaXwNQuAlQSAfALAsiSTgz8ma/9MC276TTJx068EALb0FxGHXwpX4Ort0k/r3aCP
ZcG+9dgJLNq6E36FqzAoMQufzQ/Hk1Pmw2n4DDgOnoEqg8ndR3ij+jhy9kmhcJwaKZ76E40aU6LVD/wcRwAYYw0AVsO5h5gBlKb5Z+D+dXoIAKjZMxyP/OSFQc1+w3SWBT
RkWcD3fKuwrhnozMqAPxDnIp0erC8DcggAue7dqQzooW8ENurDG4ESAIQsYCBBYJC+BBCDn4tf7jECOwgCB3QQGMcBcLjpePr3BFETRU0iWOjT/9K4v6ldf2oHfmwsbe2v
0fzLUxn8WaKx9Gf52K/W4I9lu/707q8EQEBdAQARjxaXbAs59sy60gDA1G0/Ft30a4X7lxYA569dx66TZ7Bs116ErCzB8LRF+C4wGq/M9kfjsRR8I2ai0lDSyLmoMt4H1a
YEw2FGhHDkl0csasyMEQ7+UDvvT+XIr3IBwADzk3/qAAjlqk0gcO4UiD8f745p9b/GzIbfYo7T95jHZwKEswJCKQuIdJaVAQSAdD4VyMoAPQBYH4DPA4gAEJYCJQAMohKA
ANB4sC79F9x/GA9+ps2k7U1GYV/TMRTYY4Xg55pA/0/SBb8eAAdFABywZuzXVPPPzNjvOqM9/+YGf4LV5/5NuL9W82+hiRN/TC39mR771Xb/INH9A+oug4/jUgLAchEAB8
0AYKkFd/0Z3vSr5v5pKu6fqOH+MQYACCcAFFgGgE3k8BMXLcMvYfF4yzsYrad4ocHYWag6ygMPjfVEpYkLUHVqIKp7hMNxTpR48AdpNskjxvjEn6lqx33LrvqSADCaABBk
BQC6EwAGhJp2f0vSf9H9Wf3PxCDw8WvDMKnB15hBAJhNAPAiACxwFkeDWRngzMqAjogXLxFJpwxAKANYH6A7lhkAYKUIgDW8CTiQlwASADYRADYTALYwADQeLgPASK5tBI
HdTRgEhAzgkAwAhyQAyIJfv99/miL4Tdf+sy0470977Ld8T/yxYM+/6nl/SUZz/4kmjvvmtX8DbfcP0XB/CQDhpQWAIvjNuX+6dvqvcH8dANQ3/VgDAL/la+A0ZgYqj5+L
SlN9UHlmIOznhKG6V5R46EcsHJnkp/7MMnHkl+GBnxqXfZQZAPLg72+h+/fQuz8L/rqkWj2opPlyOka5fo+prAxw+g6eTj/wMkA6MiyCnxbE+gACANIIAAsJANkiAJa690
C+e08qA4RG4IpGegCUNNEvATL3V2QABICtIgC2UPBvoeDf0ngUBeMo7CQIHJAA0EwOALn7T6H3maoKALXjvk2O/cq6/4bHfW1QOet/rbnz/tzUx371e/4jLDrvz9j9E8o8
9htpZvBH7/5L4W8WAIUiAPIIAEuTFM2/KmoAyDZ2/yqa7p9ovvaPKRsAfIpWod5kb9jPj5Kd+hNr3W0/Jm/6NXZ/JQBuWA6A/qEW7fk36/4SALqxl2Fo/Lsv+jz8B6Y1YG
XAd7oywM9ZuENAWA5kfYA/heVA1068D5Dl1pX3ARgA8ggABQSAYrERuEoGgHUyALA5gM0yAGxpbBz8WxqP5treeCwFM4PARAp2ks79J8uCf6ou+C07708PAOsGf4zdX+2y
jxVWDP4oAOBsbtdfgmW7/hpoHPdd33jyT732X6Jzfxb8TAsclyG8zQoTAFihAgBrav+FBu6fpuH+ierur9v1J275rRIWazEAfItXo/4Ub1SbH236wE/D8/603H+qeffnAB
gVgQ8DF1sPgDLV/sr0nwOgWxgadA7G98/0xdT6/+NlwKyG3wtDQeKZgcJy4B9iH+AvpBAA0kUALHZjfYAeWMYA4N5LPBtAKAMEAAzgAFivA8BQrs1NJAAIZcBWHQBGk8Zw
bSFtazyOAngCBboAABb8B3jwTxGDf6oY/Oruv8eS034NALDRxODPWs09/2q7/kI00v8Ilbl/pftnyzb9GKb/5i76jCvl4I/k/gEy9/erm8cBEEYA2EoAKLEKAKbS/0wzzT
8JAJZ0/g32/FsNAMoAqs2Lsu6uPw8rLvtQueuvzAAoq/uLAGCqTVnAa++Pw4SG32BaQzYV+B28nH7gfQB/8biwCH6XoNAHMAZAdx0AiggA+j5Af94IFPoAUgnAADBMAEAT
fQ+AAcAw+LdSBrCVAMC0gzKBfU0nqbi/AIB9TS3f8mvuqq+Npbzq6364v1b6bzz2azz3H1rfvPszAMy3GABs4s/SuX+5+5el+ady2UeV0FICoCw3/Zpq/qlc9uE4MrzsAC
il+8uDX8oCWn/viaGNfhZXA77DXCdxb4BYBvBrxFw7Is5V3wfgjUACwBI31gfoiXz33noANDIEAFsJkANguA4AW2UA2MZdXwj+LSIAtojaRpnAXoKA0v2nCcFvwVVfO026
v7eJ5p81p/2qT/7lWXHctyWbfrTGfuNMnviTrdH8W2LU/BOCXwJAngCA4OMqACgQAbBMBgBzgz+ZFiz9pWgs/Zlo/kk3/XIA5FsIgCILAGCt+2sBQHbTLwdAgBUA6EYA6B
eqPfjTV2Xd33Dpr7sKALrS60iufwagU9sumFaPlQHf6fcG8DLgV36ZKANALAEg0Y2NBbM+AFsJ6IYcHQAoA3Dvw5uAKxv1wyoRACU6AAzmANjcRL/+LwT/SAEATQgATcZw
yd1/a+PxFKCkxhMocCdSQE+mgDcAAA/+GRYd+FGWpT/lph9/k5t+1AZ/lmoM/lhz4o+0609t8Ee+7h9jduw3R3XwR57++0oAaL1SAMCMQwYAyCcALJcDwJKlPw33T7Ww+R
enctGn7MgvqwEwiQDgHSUE/zzD474tvOnXqqu+ygiAUg7+6IJfBQD1uoTgk1eGYkq9r6gMEPoAfDlQPCkohPUBXP9AjAiAFBEAWQwAVALkEgCWEQAK3VkGIACAZwBsGKiR
EgCbGssBIAW/HABjudQAsIUAwLSj8WQK9qkU/AIA9qps+RXu+ZPW/WdbfN6fEPymB39WWzj4Y/6a7yirt/yavurLePBHCH7j476NO/+5RsHvWzcf8wgAoW0YAKgEmHHQDA
CsmftfaNr9q6q6f4K6+0cKR36xs/6qhFgDgFVKAKi5vwSAsrq/7K4/xxFlBIDa3L+p9F/D/SXVpjLguU+nYozz95hen00Ffk9lgHReoHB5SAQBIJoAEO/2N5Ld2DxAFyoD
umKxCIA8AkABAaCYALBCzADWSABoNAjr3QgCbgQB9yHY7D4UW9yHYav7cBKBwH0k1zZ3AoE7gYC01Z0g4E4AcBO0xW0812a3CaSJ9LpJ2O0+BXvdp2GPG9N07HabwbXLbS
bXTjcP7HT1wHbXWVzbXGdjK9ccbHGdi80uc7HJxZNro4sXNrh4Yz1pncs8rHOejxLSWucFXGucfbCatMrJFyud/LhWOPljuVMAiklFDQNRyBWEgobByHcOMTv2a27XX7qK
+6tv+hEAYLn7aw3+yAGQDx8OgHyEihnAWpMZgBT85tJ/tbn/VI3JvwR191ec92dw3Lf1APBCNa9IvftbctWXHABTNCb/VG76rSne9FsqAPQNNb/rr5f63L8lAHj453no26
IjZtSTlgN/xDzdseHsOvE/xD7AX0jS9QG68jJgiZgBKADQSADAWtcBHACbnqCAfm82tn0+D9tJO75YgF1f+GD3F7700he7v/TDnq/8sefLAFIg/TtI0JfsZTAphN5H1Feh
2PVlKL0tDAf/F4mj38bg6PexOPpdHI58zxSPw99JSsAh0kGuRBxg+jYJ+7mSsU/U3m9TsIcrFbu/YUrj2vVNOnaSdnybgR3fZGD710wLse2bhdj6TSa2fp2JLV9nYfPX2d
j8TTY2fp6J1c8loLBxOPIahiDPOczisV/ta74tH/xR7vrTPu7bKP0XB3/8DADgLQJgi0kALJUBoJR7/s3X/irurwaAYAJAnoUAKCQATBQBYMr9Z5XB/VWu+nYcTgDwtxAA
uQSAriIArBn8MdX866oUawQ6/xWIn5/qg+l1WR/gewKAeFQY7wP8hlBdH+AvJBIAUgkAGXwgiAGgB5Y16oX8RqwR2IfKgH5UBvTHatf+KGk7Elv+pgAOL8SB4u04tG4vjm
zYh6MbD+D45kM4wbTlEE5uO4JT24/hNOkM6eyO4zi784SgXSdwbpf08qRC53efwpVD53Hr5BXcOXUVd05f5S9vl0nXRAn/vmVGN0XdOn0dN49dxYUVJ3DQYyPWvJ6GZQ3D
sNRZzf2jLNjzr3XRp9auv4WqS3/hKs2/YMWuP+Pa30cCgEM+Qlqv0sgA8ggAxYt1ADBZ+2epjP1aM/dvyXHfoeUMAFM3/Wot/Zlo/kk3/ZYJAOb2/Pcyv/RnDADh5btvjc
akBt9gegOhEeglNgID2E3C0jwAlQEJDACuegDkEACWNuqpA0Bx435Y4dIXa1+kVH1WJvau3YUD+4/i0IFjOHzwOI4ePonjR0/j5LEzOHX8LE6fOIczJ8/j7OmLOHdG0Pmz
l3Dh3GVcOH8ZF3W6gksXRJ0XJLzuKq7Rz/LOnft3erPh45+7/+D88hNY/12OEPzOMvc3aP4Zu78AgDSNwR9T7h9jwv1NbfoJkC39Se6/oG6BpQBIvlt5SaL6oR9ZFbT0p3
HVV5WgUgDAM1IIfi+D4Ldk6U9r0894dfcvNwD0sX7wRwsATLW6h6HDVzMxrNHPVAYIfQBP3gj8WdcIjCAAxLgIAEgRAcAagYsJALwPwAHQF8td+2D102OwxW8J9mzbj707
D+IA6dDuIziy7yiOHjiOYwdP4ASB4CSB4PTRMzhz/BzOEQTOn7yA86dIBIMLZy7pdJHp7GVcIihcopfs/y+dEf4t6coFcuEb9Ht/gA5+vLT1LNZQabCkYbjuso/SjP2avu
iz9IM/au4vAKAAC+oUwMuhAMGmAUAlQC4BICdRe+y3It0/QnnZBwPAz8usBMDcSNM3/UoAmK7u/jWtcH92y6/VAOhCAOgTqlz7Lyf31wMgHE1/XYCubTpjOu8DfI+5/JQg
donor7wPEEYAiCYAxFMZkEwASGONQALAIhEArAwocKcMoEl/rB8chR3rd2E3A8D2A9hPADi4+zAHwHEp+I+c4gA4dUwAwNlTBIDTF3CBBT/pohT8LOApI+DBf+4KLit0GZ
cpC2Dib6Ns4Ma1m9yBH5THsaUHkPMEOX+DCAJAtDoAnDSW/hpqDP6ojP2y4Dce+9UHv1XuX8cQACcIAIcNAZCmDgBL9/ynWbHnXz73b+KiT6sBMEEAgKOXbOzX1NKfJXP/
Rp1/AwAMKy0ArKz9exgP/mgBQBoL/vr5gZha/2vMoDKAHRnOGoG+Tr/wPkCY8+8iAP5GkmsnDoCFBIBs9+5CI5ABwKUXVrw6AZuSlmPntn3YvWUfAWC/DgCHCQCS+59iwU
/uf/rYWZylMuAsywAo8M+Lwc+CXuf6OrFgvyrTFZn0r79+5TruPiAlwbVzV7G85yJk1A3CEoOLPvU3/WqN/SZZ7f5RJtxfCYClRpN/cvefX6dQBMBq3gRcY5wBpNm1Ks7W
A8DaXX8SAJJV5v5V9vxbctdflcBSAqCU7m9Z7R+pC352y69VAFgiASCkVOm/Je4vzwJe+2A8xjr/wPsAs3gf4CcqA36hMuA3hBAAIgkAsS6sEdiJNwIXunXlAOB9APdeyH
ftjVV/+GDL8i3YsXkPdm/dxzOAA7uoBNgjAODoweM4oXD/szhDADh36ryY+l+UOT5zeDH4KcivkMNL4sEu+3/l267g2qXruH3rzn0vCe7evYut4euQ2MgfixtGUfBbmP5b
4f4xpR77NW7+Se7PAODpUIggDoATBACDDKABAaA1AcCRAWBxYin3/Cear/1NNf/E4OcACC4lAOZEmm7+zSjdrj/d0p/M/TkAhhIA/KwBwCzU7B1S5rl/SwDQ5ru5GNDsd8
yo/y08CADC/YG/wJ/KgGACQLhLR8QQABIIAEIfQAYA157Ib9YPa0bFYsv6ndi+cbeYATAAHMKhvUdwZD/LAI7r638GgBNnufufo/T/PAX/hbOX9ABgzn5B0JULIgAuXsXV
i9e4DAP/6oVrXBIgrlwU+gL/3Ofbng7m70H84/7IrBdOGUCsgftr7PnXOPEnXmPwx9RFn6Vxf/MAWEYZQBEBYIkBACxp/lk0+BOvvvRn4qbfKgEEgKVWAGC8HAAa7m9qz7
/ZpT9l8JcKAJ0ZAILLPPdvDgBsY5Drn/7487EemFb/G6EPwOcBfoYv7wP8TmVARyoD/uJlgNAH6IosAsBiKgFyGQBaDsTaqcnYsmEXtpN2URYglQAsA5AAcJwD4BROHz+N
MyfFBuCpC0LTTwaAy+el4L9qFPwmJQPBFbEvcD9LguMbjiD+5SCk1QkWAWDJiT/mN/2YuujT1HHfARpLfwtkAJhXp8hKAKiN/Zo69MMS948x4/6qAFhuHQBmR5pf+lPZ9G
Nu15+8888BMLIsAAjRHvzppdH5t8L95b2Az14ZhslUAsxo8D1mN/yRyoCf4eP0KwIJAKEEgCgqA+IoA0gSAbDQrTsWEQCWEADyWg7AmilJPAPYRgDYyQCwbT9fBTi4+xCV
AEeEDOCIAIBTxykDOHlWTP8v6NL/i2Lqz4OfB72kazy1l3T1kj7o5a/nb5PBgEHg+uXruHP7zn0BwImNRxHzSgCS6gQSAOI0B3+0LvswXvrL0Bj8yTYa+w01ueknz6j5Jw
S/AIC5BIDA1musAEC2NXP/Ku6vcdy3AgDhKu7PFMQAEFNKABiP/Tpa6/4iAGpQ8Neg4K9BgV9jZLigEaThJKr/HYYQAHwtB4ALAaCWKQCYW/rrarlYGfDsZ9Mw2vUnKgNY
H4AA0PBnLCAABBAAQpz/EPsAfyORAJDq1oXKgO7IduuBHJceWEYAWM0BsAPbCAI7Nu3hS4FSD+DI/mM4fuiEAQDkGcBFWe0vNPd4as8C/RILciHQr1++odM1Cmwm+ev461
VAwD7H/SgJjhMAol/25wBYJAKgNGO/CYqrvrTGfs0N/uRqDv7I3d+bA6CIA2AzAWC1EQCWigDIIQAsSiz9rr9Ejcm/GAtrf9H9OQD8CQC5VgBgnAiA0g7+iO5fY1wEP+yz
2qhwvt+/ztgoOI2PhsuEGLhNiIXb+Fg4j4lGg1FRHADvLlhkHQB6hVh25FcZ3F+aB2j2y3z0a9ER0wkAvA9AAJhPAPBz/g3BlAFEEABiCAAJBIAUygAYALIIAItdegoAmJ
yIzSIAdooAkJcAEgCYTvES4CxfAmTBL28AXhHdXwIAG/a5Tqn8dfbyijLY+f+LMoSBAgKibl67xW9turcACECiDgBaY7+WDP5kWDT4Y2rXn7/K2C9L/yX39+YAKMYcswAo
lAGgAtzf3sTYr8L9GQACSwmAWZEmxn6jTbp/jQkU9GPJ2cn53afG4cUFC/F7XBEmLd2EgNU7EV6yB7Eb9yN2w34Ert6FeUXb0SNxJUYsXIcL129ZBoBOIgCscf9SAoDtDm
zQKRi/PNkH0+p/i5kNfsDchj9hntMv8OUA+APhBIBoAkA8bwR2QbpbNw6ARWIGsGpyAjat246t63aIANiHfTsO4KAIgKNSCXDsNE6f0JcAuvT/vNTxvyKk/RT8LOivnL2M
C0fP8rX/Gzdu8bpeHvhMN67eJN3gkkNCrTRgb797++496gEQAF7yJwAEUf0fZ3LsV839E81c9mFc+1vu/r4a7u+lA8BaKwCQVR5z/6YGf+JVa3/m/tUDY8sOAEvcXwRAtX
EU+JT2d/BOw8CstYimQN9Cqezlm6YD+zrVoRcp+C26HNQQAOW89KdVBrz99lhMbPg9B8AcAoA3AYD1AYIIAGG8D/CX2AfoglRX1gcQAJD7cH+snEQAKNnGAbBj4y7s3roX
+3bqAXDs0HEcF92fAeDsKSoBTgs9gItnqQTQ1f4CAK5duY6Tmw9h5ag4LPvDB2tHJeJo4U6+1n/r1h3cuH5TgMFVAQA3rwli/9bBwTAjEHsHrC9w+2bFlwSsCRilA0C8Yu
w33cR5f9aO/YbVyUZILaZFCKmrsuvPxODPfNI8AwDMdihGQCsGgJMEgCMaAFhMAMhOtOKuPwsn/6JNp/+64JcDwM9KAIwlAHhEmt/zLwMAu+2n+vgItPVKwbCcEhQeOImr
tyqmuRSqBoAyjv2aXQ3oFoa2387BSLdfxEbgT7pGIOsDhLp0RCQBIJYAkEAASHEVywACwBIRABsJAFtKtusBQBnAAd4EZACgEoDX/3oAnD9D9T8F/8Vzl3QAuHqJBf81nD
90Gss6+yHGqSdy6g9CketolLw1D7tm5uDMjmM8G7hJQXyToCpIDwEJBDoYqJQG7OUt+riKnB4USoBAPQBMnPVv9dhvvSyE18pEWK0sxDTLRUK7fMQ+kofQ+hT8dXIsHvsV
lv70we9VZzlmVzcFgFwCQIEaACy77KNqadw/XMP9mQKsBEABA4AnASDC4l1/juT8tUifRuZi8Z5j5Pa3K9Q5dADoGWLiqq/SL/1prQS4dfRH99ZdxD7Aj1QGCI1Af8oAhD
7AX4hx6URlQBcku3ajMqAHB0BOiwFYPjEeG9ZuxZa12/lS4M5Nu/lSIAMAmwVgJcBxWQlw9jRlAGfO8+DXAeCiAIDr129gX0YJ4h8ZgAKXkdjeeDq2NZqOLa5Tsa7FdKz/
PBB7g5fj0vHzlA3cxi36fdy6cUsPAwtBwMTeVlFLhawEiHo5AAl1gqn+j1dZ+ksyWvrTNf60zvqvtxARteml8yKkvVCIlf234mD6CZxadR7H8s5gRf9tCHNbiqA6S2Rjv6
YHf+Tu76kDQEnJJgLAKrMA0Br8Sbdi7DfOsl1/au7PAeAbg1+WlAIA0tq/Cfd3mBSJ+lNjMXDRWhygP9J70ULiAPjbHADKL/gl1esSii9eGoJpMgDMEwEQRAAIJwBEMwC4
dKYyoBvSCACZBIDFLfpj+QQCwJqt2Lx2m9gIFAGw6yAvA44eOCbMAcgAcP6sLAO4IADgCgPAjZvYGb8CGS2GYFOjKQSAGdjG1GQGtjaaiU2u07G21Uys/y0CR3O24Qab/q
MS69bNO7zbrwYCrfKAQ4D+ny8VlvMv9/j6o4h8MQDxMgBYes237sCP+umIqZeO6LrCgZ9JrZcg7/u12Ol/EOe3XcatS8qNUFeOXceynhsp6BeLqb/5wR+9+xMAai/HLAKA
v0UAyEo07f4W7fqTBX+MFe4fKLh/df9SAGAMAWBmhNldf9Up+OtNjcHQnHW4cOMW7tXDCADlOPhjrgx44ZOpGO/8o9gHEADgRyVAIAEg1PkvRBEA4ly6EAC6Is21OxYSAB
YxALAMYM0WbCIIbOONwN3Ys3WfQR/gBAHglKIE0Kf/pEsSAG5gV+JKZLYchi2NpooAmImtTTywtbEH/Zteuntgo9sMrH3aE1vHZOHMliM8yG+TmwsZwW0rMgJhBaG8+wI8
A3gpEAm1gyn9TzAAQJLJa77j6qUhpnYaD/7EZtlY9HYR1o/ZjhP5Z3Dj3C3cva39PDcl7MaCpunwq7XEoPZXG/wpVLi/AIDlFgBgkQYATA3+lHbPv0bzr9wAoOL+DpMjUW
dqNAYtXovz12/iXj50AOgRYtL9y6P5Z1gGtPh5PgY364iZ9X/gfQChEfg7AigLCHX+E5HOf1MZ0BmJBIBUAkCGCIDiCXFYv3ozNq7egq3rtmPHJmUfgC8FHj4hzgEIy4Dy
EkACwNXLAgB2J65C5sPDsNl9quj+HlwMAIJmkWbT22dhDWUFq9/2w+75Rbh48AxB4A4/J4BBwCwIDFYOyrMvwHoAUa8EIb52CAeA/LjvVIO5f+7+DVIRXzcFcbXp3+4Lkf
X8Mqzsuh77Y4/g8v6ruHvLslJle84+zG0ThwU1FhEA8jSaf4W69F/u/nMVADihAoAlBIB8GQDK46Zfw8s+DJp/VUNiUTkoBpUCSQExeMg/Gg/5RaOSbzQqMy2Ixi85pQCA
icGfqhMj8FVsHg5fvHrPJ8gUAKjg5p8hAJz+DsLvHfrCox4bCPoJnpQBLHD6jQMghAAQIQIggQCQQgBId+mOLAJA0fhYlKzchA2rNmOr1AjcIgDgoAiAY+IsAAfAqbN8FU
AqAS6LJQADwI2bNzkAFjIANJrK3V+QEPxbKPi3UPAzbW48h95nLjY4z8Lq5rOx/ucYHErZiGtnL/M1f7YxSAKBDgbX1BqGehDweYFyWCpkAIh8OVABANVrvuslU5aQhIT6
qchotwhFP67Czvl7cWb1Ody+bH2/iQFgVpsozHPM5gAwNfZr6P5za6+ABwHATzMDkAPAkrHfFDOdf43Bn6oU/JVCKeBDYlAnPAHNY9PwWEImXk3LwfuZy/BexlK0j8tEy8
g01A2Ix0+LLQRAPgFgNAFAuulXBQDVKPVv7Z2C9J2H78sIKQfAX1oACC23pT+1eQD28r23xmBag+/hQQCY0/AXKgN+g58TawT+iXACAO8DEACSCQBpzt2RSQAoHMcAsJED
YMvabdi+fiffE7Bvx35dBnBU2g9wTMwA2DKgWAYoAHBDBgB3VgIIABCCXwj8rfwlBX/judjE5UklgSdKCASr2s/Hxj6pOF60hzcHWZPv9s3bChDc0soKZLMFZS0JBAAEka
MzACQql/7YUd91k5BYh0qBxulY9kEBtkzejuOLT+DqkWtl+vvhAGgtAMDfzNivt8L9V4gAWEEAWFeyMfjUMytnHDUAQI4IgGw9ACw67jvRAvcnAFSNiOPB3yA6CS8uzEHf
VesQvfcAlp88g01nL2DXxUvYT6niPtJm+v815CTxew4h++Ax3L571zoAqAS/4xQh/e+VtRpXb92+PwDIMQBAOQ/+mNsc1OGrWRjl/is86v/IAeBNAPClMoA1AsMIAJEEgF
gCAGsEpooAKBgbg7UrNmA9ZQFSH0C3KYiNBO8VGoHCMJBGH4AD4BpvArIegBIAHgoAsODfQoG/mQJ/UyMKfi4vrvUunljtNgerXvLHtim5OL/zBO6Qo7NGnyEItMoDqVnI
R4hLWRJIAIitFUL1f6K++deA5JyMRU8twtoeJTgUfxiX91zGnWvls6y8ffE+eLSO1gFAufRXaOT+XjL3n0OaSQDw5QA4qQGAPBkALJj8s+Sm36pRcTz4XeJS8EPBCqQeOo
IDV67iEgWhueEZ9uY7FpKaA2CUJ6pPj9B0/+Zzicr3yf1NA6Di3F8PgHA0/c0HvVp3wax6bB7gF3gRAHwoAwhw+hOhHACdEevSFYmUATAALGzRD/kMAMsJACv0AGCbgthE
oACAI8KeALEPoAMAWwk4d1HIAEjSKoCUAWxyn6pI/7fKUn89ALy4WPBvaOTNtd7dGyUuXljZmPRBKPZElODqqUu4w/oDVoCAlQbs9aVZKtQDIFQAQENyf+dE5L29FLu9d+
H8xvO4daH8ZxF25AgA8HZcRADIN+H+RQr3n8O10goALDSz519r7t/A/StHxqJ2TCK+yCtECgX+OaoBK2q5zRgAyj3/DADvRyy5L7W/bhJQAkD3kHKf+7ekD9CwUxC+f34w
PAgAsxr+LALgdw6AEAJABAEgxqULEigDSJEAMCYaa4rXY93yjRwAwkTgbt4IlDIAXSPwmH5H4PkzQh+AZQBsKZBNArI5AHUAzNKl/roMoJGnGPzeothFH/OwjlTiTnKbh9
Uu3ih6mF7+noDDi3fgxoVrPLW3BgSsL3Dn1h1YUxEc23AU4S8GIrZ2KNX/ZCoNqARwS8auWTsq9O9nx5L9OgD4qQKgSDf3bwiA2RwAK+FjGgCLJjhmMQAklHnuvzI5/8PJ
GRi3aQuOXrtW4cGlAIBa+j8pCj0yV+HmfdxHrusBMADcQ/eXLwe+8f5ETHL6GbMa/Iy5DX/DfAKAv1NHBDv/hXAqAaIJAPEEgCTnbkhv3g95BIDVResEALCVAIOJQPlS4A
n5luDTEgAuCaPAl/SrABkEgI3u0xSd/y2y2n+zCgBY8K9nN/2Q1rnPJwjMx1r3BVjj5oMVzt4ofMIXm0bm4PT6I7h1XThHkAW2JggMYHD75h2L+wIMABEEgBgDAOz23FWh
fz/bCQAzW8foACAf/DHn/gwAMzgA1pdsDCIATDcEwGIJAEkCAMxM/pm66qtSVCzaL8xGwqHDuHbn3uzZ5gAYSQCYpg6AWlOiMbFg0309ScYjKQ/1f59hDIDu9wYAbF9Au2
/nYGizvzC7PhsI+hXzCAB+BIAgAkCYcydEEQBiCQCJBIBUAsCy0VFYVVjCAbBx1RZsKdmmAwDrA+hXAo4rlwJP6zMA3SDQdWEOIOPh4ToACJ3/WWLwy5p/jaQSQAp+EQDi
PX8lpLXuPljD5YtVzgtQ6DIPxW+HY7vnclw4cJYf38VS/DtUbjIQqMNADwH2ursW9JuObzyGiFeCCQBhVP8n8do/zS3lngHAiwOgwCr3n117lQUAWEYAyEy6WyUjodRXfV
Ui5+9Awb/w6LF7GlwKABgM/jhOjkKdqTHwWL71vgX/HfrD6u2TjBq/zEBtFvQVOPhjqhHY6A8/dHqsF2bVY43AX+Hd8Hf4Ov2BQAJAKAEgkgHAhQGgOwGgL5aOjuQAKCne
gA0rxZUA2elAB6SDQQ4dV28EntevBAhNwFVIlwDQSL70pwfAZhkAJPeXMgAp+IVLPoWLPldz+WG1qx+WN1iA/Oa+WPF9AvbGb8K181fFsuCuLiMwVR6w1QXWTzBVq/IS4K
UgEQDJSBEBsOueAWAxpf8Fisk/taU/ufvPIgBMJwAs4AA49cyK6cdKAQAz7l8pOg5NU9K589/rhx8BoIEaAKaIM/+TozEh//5lAGfJAb+bEg6H3+egtrm7/ioIAKwPwF5+
8coITG/wE2Y3+AVeDVkfoCMCCAAhBIAIAkAMASDBqRtSGABGRWJlQQnWFq7H+hUbsXnNVt1S4N7t+/hIMAOAfE8AKwPkjUBpQ5A0CZjeggDgpgfAZmndX3R/If03BQDta7
75JZ/O/ihs6IO8toFY3X0hjhbtp7LgNp8fYBAwC4LrUl9AnQLH1h9F2ItBiCYApDEA1L+XAIiFpwgA+a4/b5XBH8n9WfALAFhFANhQskEVAIvS7FpLAEhPUDb/UjQGf2Tu
XyUmDtVjEzBk/UZcvn37/gFgqgEAxE0/bMvv6KUb7hsAVmw/gCd6eMDhDy8K/vB77v7yMuDFT6dhnNvvVAb8IvYBOsLf+U8EO/+NcBEA8QwAzfoilwEgfy3WFEh9gK26PQ
F8JWDnQbEPIJwOJJ0MpAMAKwPEeQChBFiFNALABvfp2NJoltj5FwCwicvQ/WUlgC791wKAdMtvAJa7BqLYOQB5zn7IfToEGycX4OLBc2JJcEcmFRCIMODzAiqdfCEDYCVA
OAdAMgEg9R5mAAIACs0O/sjd34MBoJoJADRkAFhKAFhoAACL3D8BD5H7v5aTi03nL9yXAOMAGCECQOXEH8cJkfg9sRAX7+Hsv3w5k9f/P09CzS4B9yX9lwOg5U/zMbBFF8
yu97MeAE4SADojmgAQRwBIbt4POSMisDxvDVbnC2UAbwSypUBxJUC+J0A3Esw3BZ3nI8EXzuoHgq4RAHYmMACMwAa36eT0Hjz4N3GJABDX/pUAmC+r/1Wu+SatMrrmmwDg
GoQilyAUOAUil96+rsdiXDtxRSgJFBC4o9kw5H0Bg8Yx6wGEvxxMGYAEgCQCQCoBYHfFAiBnP2YQAOYSAHzqGAJA6f5zDdzfo/ZqTDMHgDaaADA99svcv1Z8ImZs237fHN
YvTwQApdlqx32zPf8d5qVh1eHT9/y5bT5wHG8MnofqP08Xu/7mx37rdAkhVUwZ4NQpCH88OQCz6gsAmOf0B/wIAIEEgFDnzogiAMQSAJIoA1g8IhzFy1ZzAKwjAGwUR4JZ
BiBfCRDOBjiuWwngB4Oc1QOALQVevXYDOxJW8gxgPQfALGXwk/vLB38kAAjurwSAVvqvB0AQASAYRaRC1xDkN6R/t4rE8cQ9/HfCM4Hbdy0CARN7X6kiOEYACCMARNUiAD
RM4QBIIQDsvCcAiCUA5HAAmBv8kbu/Ry0GgNWYTwDYaBEATC39GaT/lQkA7TOzsfbsufsHAJ4BzBUAMNX4mm/HCRGoPzkaHsVbLTq9p7wel6/fxKiwTNT6bixqdvI1nf6L
Y7u1O4egPv1/bYIA+3dFZAEfvjkW05x+xZwGv8GbAOBLABD6AJ0R6dIVMQSARAaA4QSApauxKm8tSooEAKgfDqI/IdhwJUACgDwDWO9uDICNjfQA2KBzf29Z+m/s/sbpf6
AqAAqc6GWzCBwN2aEDgCQBBHfNguC2eFCMAIAQDoBUttuvfvI9BEAcB4By8q9Yl/4rl/707j+z1hpMFQHAMoDlRgDIlgEgLcGqXX9VKP3/vmgFrty+fd8A4LNsBeoPnwsH
NQCIB35WHxuOV/2ysO7Y2XvynNgW1tiCDWjacRIcfvUwe9mHNLP/5uR0zMjciJEJa9FuaAKBoPwB0P7r2Rjn3pEA8Cu8Gv4BHw6Av6kM6Ixw566IJgAkSADIXYWVy9YQAN
aLKwHscJCd2MU3Be1XNAL5SoB4OpARAFgGEG8AgEazKeAp+Ln7GwNACP55vPZfp+v+MwD4KdxfAIAs+HUAoOB3CUFevSCUvJeBy5uF3z2r7ZnkILh7W4SBQWYgbxjSRxEA
jooZQIQMAGkVDoBtOfswvVUs5jgsIQAUaTT/Vhg1/5j76wGwUaMEYADIJQBkqAPAXmPPf9VYYb//oPX3r8HGHlMXLkOtIbPhMDlC87Yfdtin49gI9EpfhdNXrlf4sl/Oul
14utcsVP9hEgV7kJH7G6b/NTsHo+XAGMSs3Itb9Ad54epNjKWUuW7nQB0cymsgqNEf/ujbqjtfCfBs+DsWEAD8RQCEOXdBlFNXJDTti0XDwlEkAmBt4To+EizfFCRfChQO
B9GfDcA3BZ3V7wm4evU6AWAlUqkEWOc+g4J/FgW7CIBGAgA2EAA2sLl/KfjZ9J84+CMs/1nq/qz+J9d3Jgg0CcOmbxfj7JIjPMhZD0AnNRDc0QbB3X/u4sj6Iwh5MRiRBI
CUhqkcAMkMAF4VnAEsETIAPQCMB3+Mmn88+CUArME8DoDTlAEcNwMAefqfpDL3L7p/VQr+OnFJ8N+z974CYExKDhyHzOUbfnQn/qpc9uFAAGg4KQbjctbh7NWKgcCNW7eR
RS75TO9ZcPhuPGp39UedXuFmr/qq0SkYz4xORsn+02Lz8B+ELVkLpz/mlms/oA59vXr08ofnh2JWA7YS8DvmOXWkMuAvBDl3IgB05QCIb9oH2QSAwiUrsYLKAAkAfE+AdD
oQvydAPB7sgHxPwFkdAKSBoKvXrvMeQCplAOvcZnD338A1l2ujIvi9xeCfp5j8Uwa/AICVhu7Pgr9hIAfA2leScdhrM24cu4p/7vyj+7kaSQUCan0ClgEcWXcEwS8EIpxK
AAaARAkAFV0CLDlAGYAAgPkmBn+MAbAGM0hTOAA2aQAgSwRAOgEgNcHiXX8MAHXjkxC6b/99C/5rt26ha2gyqhEAHC247IOd+e80IRpjF5XgKP1hlufjFKW68zOK0a7rdD
j+MEEf/Bbs+a/ZKQT/88yh53RVV0KMj1iMWt9P4U3B8i0DwvDah1Mwzfl3ngEwAPgQAAJFAEQSAGIJAFlDw1CQswLLl6ziS4FsFoCtBCiWAqU9AQSAY4dkm4JOC41AKQO4
dk3IAFIIACWUAWzkwT+HUnwCABNP/QUArJO5/zrR/aXBnzUG6b+i8+9EgU9a/WQs9gxbicsbzuCfm3c1VmfUQaBZGog9gKMsA3ghiAMgqWGyCID0ewCA/RwAszkAihVLf3
PVlv5k7m8ZAJYYAsDM2K8IgFqxifDeufu+AWDXidN4c7ofqgz1pvQ/Wg8AEzf9Oo6iTGBcFH4Nz8Gy7Qdw/WbZlgcvUjaRQ0Hxx6xoNPhpHKr/PI1S7QCLd/2xAGeuPCpx
LZUPglNdopr5h8nhqPHjjApZDXjkp/kY1bQT5jb4Hd4iAAKcOiOUABDRUATAEALA4hVYkSuuBEizAOv0ADgg2xQkPx6MHw4i2xR09eo1EQAjKZBniO4vAkBM/4XgFwCwrp
Gh+/vIOv/y9J/c3yUAxQ0IBi3DsL1jLs7lHMbdq5b1pLQgYAiCO+JyIJsDCHkpmAMgkQAQXz8JifeiB0AAmCYCYJ4cABa4/4xaazkAvC0GgNT5t2DXH9vvP6hkw327uTm5
ZAuaDJkJ+9H+FPzRsuZfpMm7/mqMjED1YSHoMDUWo1MLkUd/zOevWL5bkJ1TcJRS3AwKiF4+SWjbbQbV++Ph+JcnBT4FWe9wi/f8s25/8/7RSF13QPf5D506jxf7eKLmr5
70fmHlDgHnv4PQ5Yl+mFv/N3g17IgFBAB/p04IUQEAywD4SgBfCtwibAveKJ0PeFB5PqB8KfCMUAbw24GuXMN2AwCsdxcAsL4Rk5cCACWK4F8gzv0L6/5y91/e0B8rGgVh
82cZOB66AzePlX7Hp7mMQMgAjiL4xWCE1YogAKQgjgCQ4JaK7RU8CCQAIJ4AkMsBYNL9ayvdfzoBYLJlAEi+WyUlwaK5fwkAVSLj8OmyQpy5cfOeB/95cpXuYcmw7zMNjh
PDrbjpN1J30Wf1YWGoPSQY7caG4je/NMzJLMbCtduwlf6Yj5y5gDOXrnCHP33xCrZRjbtm92HEFW3E0NAMfDohEA93noYaP0+EQ8c5qN09UDzxN9yqPf+1CADfeOXg5EX9
zsmU5ZvR+JfJqPWnb4UAgOmTN8ZidoPf+EqABIBgAkA4ASCGAJA5JJQDoDhnpWwpcAtfCmR7AoyOB5MvBcozACoDGAC2xa1EsgEA1hEA1okAWCcCoEQHgAU692cA4HP/bs
Lgz0oXfz72u/7FOByetR5Xd5wv1z34aiDQAeAFCQCpBIBkJLimYtucneIHViwAZskBYLLzv1oX/AIA1sKLALCeAFBsBIBMEQBpSQIALNj1J7k/A0DL5AwsPX7yngNg4cbt
aD5kOuxZ+q9wfxEA4w3cn8nwpt8REagxnEAwOBSOA4NQv78vmg6Yj/ZDffD6GH98PDkE386MxEcTgtC+7xy07jkL7n9PQ83fp6B6Rw84dp6P2j2DxMs+wq0+8JO5/8MDYp
CwZp9u/pzND3SeHYea30xEnc5BFbY34OmvZmOy21/wbvAH5hMA/AgAQc5dEEYAiBYBkL9oOYoWEwCWrcVa3VLgNmEpkB8PJg0DKWcBpAxAWgq8clXIABgA1hAA1jdiwT+H
gpwBwFMHABb8a7nz6wGg2/XHpv5c/bDS2Q9r24Rhb78CXFp9EnevV8yuU8OMQCgBjlEJEEoAiBQBkEIASKcMQCgB+EpDBVxMIgHAgwDgTc7vZWLsV5/+C8E/rVYJJnEAbD
YBgBwDAFhy0y877osA4BCZgKHrNuHm3Xu3337/6XP4yisUVXpNQo2JoToA1JR3/824vwSAWsMF1RxKMBhCmcQg+nz9g1GjTwAce/nCsft8OHadB8du9LJnAGr0CkSt3sGo
zS757Btu8V1/hmO/zPmdeoRjQuo6XLmhr1kzV29H6z+nosYvcypsLJgBoOlvvhjcsge867NGoB4AoQ27IIoAsJAAkJdVTABYgZVL1/BNQToAKJYCDWYBjp3WXxMmLgVKJY
AAgJk6AKwTAbDOCAALFOn/alcfrHQiELQIwrZvFuJs2j7cuXhvsk5DAATrAJDGARDvko4t03bygSK2UiDPGMoNAIv3Y2qrOALAUg4AKf2Xb/mVp/9y91cC4AwB4ISJDMDM
3L/acd+VwuPQOnkhFh6+N9uAz1y+iqHxWajZawIcRvkKzT/Z5J9h518KfjX31wFgGIkAUHtoOJUEpMGiBtHrBtLLAUz07/70sh8FNZPhNd9WuH+NzsFo2D0c/SJX4PQl/Z
LkyfOX8evUSDh+PR51OgVWIABC+SlBvzw3DN712Ejwn/AlAARyAHRFJAEgY3AIAaAIhYtW6GYBNqzcZHBA6AGxEXhEdz6gMA2ovyjEKANwm6l3fxEAJQYAWMMlBD/b78/S
/81vxOP4/I24efwKWe396TrpM4AoDoD4Bqn8pp+sV5dh7fSN2Fd4EBdPXNI1DcsXAPEcAFpjv4bNPyn4BQCUwFMTAAtZBpA9wTGVASDe/IGf8ss+SPYR8agcGovPlhRg54
WLFfoLOH35CkYlL0a93hNQfain6k2/pXF/HQCGyAAwKFwW/OFC8EsA6Btm+ppvDfev2SWEB/8jg2L5tN+x8/q6/9rNW5gZvwyuP4xHzd+8KFDDKnRzEIPA2+9PxmynjpjX
8E/4iAAIkQFgGWUAhVQGsFmANYXrdNuCt63XHxDKpwFlB4Sekp8QrMsArmJb3AokEQBWcwDM5gDgEgFQQgBY6y4BgILeZT5WO8/HhmcicGBkMS6vO4l/bt+/U52kUeDgly
UApPPbfhgAouukIaRePEIej0fa7zlY478RRzec4CcSl88k4AECQIIIgBUGgz+m3X9qrXWYaBIAGal2bRZnEQAS71ZJii/VTb9Vw2LhGBaPP/KWY28FQeDI+YsYHJ+Jer3G
o9rgORT4kUr3N7H0JwDAtPvXGmoQ/GoAoOCvbcr9NZp/NbuGoBYFf/MB0fh5wVIkrtnPp/2kx63bdxC6eA1a/j4ZDt9PQ90uwRUa/NIhIY9/64lxTbpifn22EtAJATIApA
9iACgSAJC7WjcLsFk2DLSHnQ+4U8wADkpXhZ1SAID1AS5fVgKgRMwAStwp+N2F4BcAQOm/GwW+0zysaxWIPZ0W41z2fty+LJwp+c99W2+SAyAMobWikdggQ3nRZ312x18a
Amsmwt8tHhGvpSFrQAE2J+3E2UMXynQc+VbKAKYQAGY6LIOXLvjNu/9UywFAGUCKBgDMnPevu+yDsgDHkDj8L3sZlh85xkdiy2W67vZt5G7fg6/nk0OT81cbOlcf/GruP8
Gg8Sel/4YAUHP/wQbuP9CM+2ul/zL3r90tBI8NT0Cv8OVIWXcAR88pl6pu3rqNyKUlaPv3NDh8O5mP/1a0+0sAaPyHP3q1648FIgD8nbogmAAQ0YQBIBhLMwtRmC0HwCZx
FmCHDgBG04AiAHgJIN4WLGQAK5FIAFglA8BaAsBad8n9vbHGhdRoAbZ9moTTsTt4uv+P9J9BQ+5+lQDBLxEAakYjgYI+TvWm3yxE1MlEcM00+NVNQkDLZER/mollU1ZRiX
AY1y/dtLpHwDKAKa0FAHiaGfudYeD+U2oyAKzD3FZbStYRAIrMAsBw7NfkRZ/Ku/7sQ2JhHxSDxyJTMGvVOhy9eIlPtVndeCFdp8DYduwkRqfmoPUID9j3noTqo1nNrwx+
Xe1v0v0jtd2faYgGABTBH26F+wsAYM7ffGA0wop34Tw5vuHfLlteZGn/w+T8jt9Ooro/oMKW/dQagfUpK/nmlTGY14AB4G8OgCAJAAMJAAsLUZBdrBgGYleFyYeBeAaguy
xUeVegcFXYRXEZcAUSmzMAeHAACMEvAGCNK7109camF8JxZOZqXNt7nq+9s8Dn5/vdvftAAIAtAwa9GKoAgD74FwrXfBMAmCLqZyO8XjZCamchoFYGfBqkwLdtMhJ/X4o1
oVtxeu85flqRJd8PB0CrRMyozgCw0njXn8HgDwMAd/+aAgAmmARAOgFgkQiAxHjr7/oLk931F0wvSVUDYlHHPxrvx6bCa/kabDl+EueuXuV1rto3zF7HhmsuXr+BI+cuYN
mOPRielI0nxs2FY99JsB86B46TQ1FjerTqnn8e/FoAKI37D1J3/9r9rHB/kgPV+8+NTcGGQ8pdiOcoJS7cvBd/ecTA6buxqPHDdNTtdG+c3zALeO2T6Zjh1pn+QP+GnwiA
cAJA2kAhAyjIKubDQPpzAYSDQXYYnAvAZwE4AE4bTQOyEmBr7EokNBuBVa4eFPSC+69xmUt1/lysbxeAvT1zcHHlUdwRg0IKfMPgv68AoAwg6EWWAcRQ+p+h6v5S8HMA1F
+EsPqLEVJvMYLZ7b51FsOndia8XVIQ+HIGsoauwI7FB3Dx2GV+OrHWY3XMDox1j8FMxzxy/5XKPf+1jAd/5O4/peZ68wBovVgLAPHq6T+v/eNM3vRbjV3z7RMFR89gPO4b
gd/jMzB+cT5i1m1C8oatyNyyA3m79vKXsWs38Tv++sRm4M1ZgXAfPhPVBkyF/XACwKQQ4bTfGdGKAz+5+0824f5jLXR/OQAGWZH+W1j7N+kXhbmLt+DAqfPYfeQUMlZuRa
dZsWj5x1TU/GYCav02l2r+kHsa+PLtwW1+nI+RLXvBt/5f8GUAaNAVYQSAVJ4BFHAAFHMArCUACCcEC0eEG58MpDgh+NRZXQlwiTKdIyV7sfjtuVjeYDrWNJyF1Q1no6Tl
Auz4Lhmnk3fi5oXrVDbe4QdzKubxH6QMYCMB4KVwhHAALDRyfzkAhOBfhNB6DAA5XMH1liCoXi6/5tuvVg48a2TAs2kqIr9YgmKvzdi/8hgun76q6HVcOHYFYZ0WY7hjJG
bXKuIA0Kr9Fel/TUMAbDWTASQTABLira79WfBr3vRLquYfhyrzo1BpVhCqTvVF3Wk+aDjJG26T56HZlAVwG++JeqPnoMaIWag0ZAYqj/SC/cQAOEwPhyM5ZI2ZMaqXfSrW
/a1J/4db0fyTB78l6b/Krj+2nfdhKgO+nJ6Md4cHoOlvUyndn8jX+evoXD/svgCAbQ92+SsQnZ8cShnAXzwDCJQAMCAIuRkFygyA7QdQAEDlunCD24L15wJcwf4UyiC+iM
KGZwKw7bM4HJm/FlcOnKfs745wA/BtQQ8sACgDCKQMINgIANruHyoLfh0A6i3l13zziz5rLYOXw2LMqpuBBU9mIeGvQqwK2Y6dyw5h08K9iO6ViyFOAZjgmG7R2K+h+08m
ja+2HnM4AM4SAE4qAeCUlmr3aDYBIEkbALrg1wCAoftLwc+u+nbwI/ky0et94mDvHY2qnhGoMiccVTzCYD8zDNU8wlF9VgQcZ1PGMDtG0KwY2VVfBqm/mV1/Ru5vCIBhZW
z+WbD0J9/1x1YAHP/0Q/VfPFHj93n6Nf5u9yfwDSHw+ZsT4eXcGX4NO+sAkMIAQBlAfiZlADlSCSD0APjRYBuFHgCfBdhzSB0AsqVA6Y6Ac1uP40z+Plzcfgo3rt3ArTu3
cYtKQyZTAJBD4P71AAgAL4RRBhBL6f9CHvxqAJDcX0j/DdyfFEAAYFd8S9d887P+6xTA23EpPByyMdstC7PbpmNii1gMrxWO8Y6pFPhFuvP+DNN/o6U/0f0ncwBssAAAi2
QAiDcBAK3aX839KfirS8HvQ1rAFAOH+aR59G/vWDgyeZE8SXMp6OdEU+BH66/51rjq2/DEH4sHf4aXbulP1f17abi/ycs+wsSgv/+BL6kmlQHPfjUH0xr3gF+DzgiQA4Ay
gLzMImEcOE9ZAuzcuEvYEKS4Llw6G1CfAZyXnQ145bJwS9CNW7dw8/YtfmvwzRu3dAC4feu2SQDc734AzwBeiqAMgAEgUxf80WLzz9j9F8uCX+7+S3XBr7zoU7js07tmIW
Y7LIMHAWF2rXzVTT9MUvAbDv5Mlbn/JALAOBEAJQSAQrMZgMbcv0n3lwAQaAoAUvCL8qKA94wRAl8K/tl6AKi6vxEATA3+RFjm/kM0av8BBp1/LQD0vLd3/ZV/HyAMzX7z
w7DW/eHfoBMHQGiTvhwAS9JZBlDEMwDDDUG6swHlJcBBYwDwEkC8JOSKdFvw1evc/W+w23lumgeA6v79+5AJHN14XBUAUTIAsOBXB4Dg/hIA/Ayu+WYAmC+76kvY9rucD/
54Km76WWlm8KdE5/4s+AUAbMDsVttKSgIJANMMAZAqA0B8vAXubyr9lwW/lP4buT/JmwJeDgCD4Dfr/uaaf4ad/5Fqgz8R1rl/KSb/HvTg150SRCVKx+dGwbdhZz0ABlIG
kC5kAIZbgtnZgBIA9hMAdGcCGB4Oesa4BGAAuHblmh4AKhmAIQQehOAXBoEIAC9HIIgAEMucX3R/If3PNmr+hRq5f66G+1ty1ddKi8Z+Dd1/YukAIHb/o61o/gWaSf/niw
Dwlrm/KQDMMJf+l0PtP6SUgz//EffXlQE9wvH++9Pg7dKN9wAEAATLMoCVegCs3qy/KHSLPgM4LMsATssuCtVfF245AKzJAO4lDFgPIOCFcATKAFAa99cDQO/+apd9mNr1
JzX/tNx/suj+E2tuxFizAMgiACQSAFjgmxn7Nev+hs0/H2P3d9Byf6P0P8qy5p9W+m+J+1fQ3P+/CQCsDGj3w3xMb9qbAyDEvTeSu/thSVq+0ARcvFKRAZgGwEn9TcHyDM
AAAPIywFwG8KDMAuzN3w/vtgEIqhVHAMjS1f5Gzb96yu6/ofszqbt/odmLPs27vzL95wCw34hZmgBIIQBkEgASkswDoLTNP3ntbwoAcvdnwV/qpT8zu/7Kw/17aez6+5cF
vzQV6PZXIAY+PgRBDbohpGEPJHw9CzlxucjPFlYB2JkAuhJADgALmoD8qnBZCcCCXwLATUuygH/uPwDY+O7asPXwcJmPsLrJVP9nGXX+wzWCX1r7N0z/efCLtb8cAMZXfa
1Q1P4eGmO/xun/RhkAtpesDTz3TMG0UyoAYBlAAmUAsSqDP5EWLP0FxSlq/+rWur8WAAwHfyZZOfgz0symH0vcv09Yqff8/1sAIEHg+9cmItCpO4Lrd0f0s6ORPT8N+fxU
IBkAVos9APmpQLImoHQsmGIZUGoCEgAk99cBwEQPQDcU9AAA4OrZa0jqmo6ZjvMR2SCD3D9LZelvsa72V7r/UoX7+2m4v3TZh6fKeX9aJ/5MU1n6kwf/hBqbMIYA4GESAJ
nGAFB1/3CVzr987V8e/BIALHX/WZbW/mWY+1dzf425/zpWjv3W+RcHv1QGvPzFHMxz60UZQE9EuPRFWic/5C8sQjHbC5BXYjAJuMt4FPjQccWZAOdkZwJIAFDrAUgQsKYE
uNcQ2L1kLzxaz8P8WmFG7q82+BMiH/ypKwz+CMEvAEBy/wUa7j/XrPuvUXF/JQAmKACwQwMAyQYAsHTpL9gg/bfE/aXuvxT8mkt/YvCbSv+19vyPNrXnP7zCdv392wHAMo
Dmv/lhcssBCHXqifAGvRDXbgQWT0nA8lzhPAC+GchgFWDf9v26W4LZIBADAN8LcFJ5MrAqAMQMQJ4FPIgAOLntFEI+j8K0ml70c0lRcf9s7bFfg9pfzf1NXfRp0a6/mvLJ
vw0KAIznANhkDgBZExzjCQAxceabf+bcvyKaf+Xl/pak/+bc/z+w9KcKgK7slKBgdHt2DMIpAwh37ouohn2R+OQYZI+OxvKs5Vi/ajM2r9uObRt3YhcF/96dwhTgYXYvwB
EK/mPiDcFnyPnPXcCFC5dw6dIVPgDEjgZndwTeuHmT69bt27h15w7fLco2grHt43cfkE0/0uPurbvYX3QQoV9GY3Kd2QioF0vun2m868+o86+e/vvJAFAa95+ptedfpfaX
3J8BYDQBYKY2AFIIAJkEgAQBAJYs/QVb2fzztnDt35LmX3mO/Voz99/rv+v+8izgk/dmIMS5NyKc+iLauT9iG/ZHXIuhSP3RC0vmpmBFejHW5ZZgY956bCnchO0rtmLnqu
3YvXYn9q7fjf0b9+Lg5v04su0gju44hOM7j+DkrqM4tec4Tu09gTP7T+LsgVM4d/A0zh06w3X+0FlcIF08fE7QEUGXdC/P49JRQRdlunT0gqAjF/T/lumiWV1U6IL48vzB
8zhQfBB5Mygwn/fDlBpzKPhjTI79hpro/AdY5P5FFrm/1qafySoAGM8BsFkGgLMqAEgiACzMUgIgSrbrL9zCpT958Fvj/h4WLP1Z1PyLKPuuP/mWXyt2/f3b3V++L6DDd/
Pg1WQAuX8fRBEA4lwGItFpEBKcByH20RGIfmMiIj+YxBX90RTEfjwNcR9PR/wnM5D4qQeSPpuF5M/mIOXzuUj5whOpX3gh7UsvpH85jzSfK+OrBcj4ny8W/s+PXkryJwUg
4+tAmYKQzhWM9G+CkfZ1iE6pX4eJCudKEZX8vwiupP9FciX+L4orQVT8/6K54rhiuGL/R86uUxwiP4vG/Kf8MaOBJzxr+lGAJyBWZerP/NivcvDHr9SDP6u1T/xRGfyRu/
84AsAo+80EgJ0laygDyJ92WgMAcQl3q0bHWbTnX5f6WzL3LwW/NZN/U0vh/pbs+R9SRvfv9d8Y/DEFgMZ/BGIU1f4xBIBoDoBBSHQZjBQXygKchiKlwVAkNCAg1B+MpAZD
kNxgGFIbDEdag5FIbzAKGQ1GY2GDschsMI40HlkNJ5AmIqvBJGQ3mEwvp9DLqaRppOmkGfS6mciq74HM+rPo37Po42bT5yDVn4sMUnp9TxKBpL43aR5S681DSr35XMn1Fi
Cpng/JF4l1fZFQ1w/xXP6IrRuAGKY6gYjmCkIUKbJOMCLqhCC8dgjCaocilCsMIaTg2uEIqhVBikJEXRb46bqNP3r3z9bV/mq7/hgAAmW7/gyX/kwN/kin/RoP/qhs+a1l
2v1Z8CsBcN5KAIRbPvhT3U/u/rHq7i8HwCwzS3+lOfDTkvRf0/3LMPjT/d8f/NJYcIPOIfj9lckiAAboAeA6DOmuI7DQZRSyXMdgketYLHGdgFy3iVjqNgl5blOQ7zYVhW
7TUeQ2k+SBYrdZWO4+G8vd5pA8SV4kb6xwm0eaT/9egOWuPvTSl+RH8qePYQpAkWsgKRiFpAJ21bdrKPJdw5BHWuYSTorAUq5I5LpEYYlLNHKco7HYOYZrkXMssp3jkEXK
dIrHQqcEZHAlIp2U5pSE1IbJSGmYgmSuVCSxCz8bphHg0vmJP/GKAz/UAKDc9afc8luWsV/Tgz/K9H+90eDPeJn7jxUBMMMkADJkACjXXX9ld38dACZYeOLPSLVdfxGWDf
5Yk/7/x9xfagSyLOCdT+Yg2G0AYp0YAAYTAIaIABiJha4SAMZhidt4EgPAZCyj4M93m4YCtxkEAQEARQSAYvc5FNBzSQIAigkAywkAywkAxQSAYgJAMQGgmABQLAKgiABQ
SAAodA0SARBMwR8qBL8IABb8uVyRFPyRyCEICACIpuCPoeCPpeCPRSYHQBwBIJ4DQAj+RMpm9ABI4krll30k8OO+0xXHfcWopv+LrN71Z9r9BQDM0Zj7N+3+G3Tp/0QD9x
9bYwtG2m/hAFirWgIkCgCoEUsAiIrTbv7Jl/4M9vybbP5ZOvdvqftXxNx/WXb9/UeCXz4P8NiPPvBoOQzxrAFIAEii9J8DwIUBYLQOADkiAHINAFBAACjkAJitCoBiAkAx
AwB3fx8x+P3o/f3p4wIEiQAQgj+Egj9E5/5LSbkiAJZwALDgjzIK/iyj4E/Quz8phQCQTGIASDQBgGhTe/5lm36sc3+1pT8t919tsftPMHB/iwDQRg4A+dKfpe5vYtefQ1
l2/Vnb/BtpZumvnI/7rvsfBADLABr9GYhBT42jYBiAeAkALsM5ADI5AMYSAMYTACYQACZRBjCFgpMBYLouAygk9y+i9L/YTQKAFPzeQvBL7u/qS4EvBH+RgfsX6NyfBX8o
d/+lMgAskbn/Yg33X8jTf2P3T+Hun8zTf6X7p2mc9ist/ak3/5jMuf8CDff3NHPVl7H7l5gc/Bkvc385AFgTMM8QAM4JlAGkEwBiCACRcWXc8288+aff8x9dtqW/CVY0/+
7Brr+6/5GlP7UygL388a0ZiHEeiATnwUh2GYZUlxEEgFEEgDHIJgAsFt2fA8CVAEDurweAhwAAlgFQ8BeR+xcZub8AgCI3PQAKpQyAAFAgc38h/ScAuIQZuX+OgfszGbt/
vMz9CQAN9e6fLLp/Aq/91dw/0+R5f6EVMvizymjwR7HpRzX9N27+seAfU2MrRhAAprfaVbJaCwBtLQGAhvtbNfdvOPk3o6zuH2n5ZR8mBn9qW3zV138fAMIpQWF49Usv+D
cdhmQnAQBpBIAMVwkA4zgAclQAkE/uX0AAKCAAFBIACg0AsJwDYAGKWPCL9b/k/obBb879c2Tuv0jn/jEa7p+g4f4pCvePN+n+WSbdP1Bl8MfXgrFfT6vGfk3P/evTfxb8
SgBoZgBt00QAsJN+LRn7VZv719r1Z8nY73Trx345AKy96kvr0A+b+xsBoNWv/pjWdgwFzBACwHAFALI4ACZwAOS6TiYATMUyDoAZIgCk4J8jA4A3F3P/IlcJAIL7F3KpAS
BEDwAL3V+X/svcX0r/U1XcX9/517t/nLz2r1+egz8FpXJ/teaf2tivYfrPgn80A0DVrSIAzhMAzhgAIF4EQLQlAIjVz/37WTD3b2LXn+bc/+Qo7Zt+K3LXnw0AiuVAp84h
6PP8FCQ7C/V/GtX/GVT/6zMAwf31AJhOWQADgJD+KwHgJQJgnhIAbhIAhOAvYOIAYM4vpf8CAJbKALCElKPa+S8P97ck/Tdf+/vLNv343Ef3ZwAYTgCY1mq39QAw3vUXa3
7XnyVjvx73Yc+/NQd+WnHT738t+OVZwFfvz0Gs+3CkOssBIDQAOQAo+HMp/WcAyDOT/ksAKBIBUCgCQHJ/ffAHicEfLHb+Tbi/UfqvbP5laDT/klWbf+my5p/2ab/hqu6/
RMP98zTcv6h0m34Mjvu2xP1HO26zEABRiXerhpu+7EN715/K0p8EAGv2/Jta+rPU/YdV7Ik/df7j7i8HwDPf+SCg+WikOw/nDcCFBun/Eu7+BABK//PE9D+fA0At/dcHvw
AAXxEAYvCLAMgXAZCnW/pTAsCw9ldf+jN2f2HwJ0nF/VM1an/5ZR/Zqpd9GJ/1rzb2W6Ax+LNcdenPQ/XEn7Ua5/2J7l9T2/1HEQCGVd2GqQSA1QSAZUYAiCMApDIAJAgA
CCvj3L9XGeb+5ef9lfWqryFlvOrLxGUf//Xg5/MAbCz4zyBMfnwiAWAE0sn9F5L7Z0sAcJ3E3T9XrP8ZAPIo+PPJ/RkACkQAFKq6/wJe/zP3L5C5f74IAEXw6wAQoQDAYj
NLfxni5J+x+5se/LHssg/LB3803b+O8sAP5a6/NUbHfSvP+1Pf9GPo/iz4LQXARAUAWPCbmPt3UJv8s2bsd6aF7l+Wpb/SXvbR2+b+8l5A11dmKtL/bJ7+T1Ck//r6f6YI
ACn4DQDgqk//C90kAAQYpf95Gs0/pftHKVL/bIsHf1KMBn/iVNw/2ir3t2TTjzWDP2vKNPjDAeAoAGCk43YMJQBM0QKAW3yqXduUrPE1IhPuVA2Ltey2H18L5/5V3N/R2j
3/4zX2/I+y8rjvgWV3/zrd//8Ev1QGvP/ZPMQ3GqNL/zkAXCeqAEBI//NdBfcv4MHvyQFQyN1/Hnf+Qh0A5LW/HACWuz8DQLbK5J+a+7P0P1mj+afu/uVx2Ue+yU0/Wqf9
Wub+G1Tdn0nu/noA7FEHwCNJC+1aJS3s5xgRf8Ve7v4hsvP+zM39G172Ycnkn7nLPsrL/Utz15/N/XVTgW1+CUBAywnIdBEAsMh1gggAIf1f6jqNADBDB4ACBQBY8HtzAB
SS+0vBXyACQEr/83nwq6T/LkL3P5cyAIX7u0QpOv8693dWc/9Ek+4fb6H7Kw/81HJ/c+f9FRm5v1b6P93s3L/24I8EABb8AgC2iwC4QAA4a6d4PJ2+yO7ZjMXvOoTF7bMP
jdV2/9IO/pT1uG+rmn9Wjv32/f899mvJVKBzpxCMfno6sl2E9H8RBT+r/3UAIPdfJtX/rrNEAMyVAWCeAgAFHAD6+j/f1Tj9V7p/mKb7m970Y675p1/6i1Vp/hm7/6JSXf
Vlya6/WZrNP9Mn/hjX/lL6v00VAKvUADBozQa7KZu3164RFhdcOST2TsUf923BXX8TSrnnf6iJsd8BVp73ZwOA2AwMxc/velL9P05Y/uMAmMwBwNxfAMBMEQCzKQuYIwLA
0wgABaL7F7iy4PdTcX8BAMrgD1fs+JPX/ubHfpMUgz9JGoM/sRYM/oRZO/hTV6P2N9rzv8roqq8ZFrr/BBPuLwFghOMODCEATG61lwNgqSEAuq1Ya/dD3nK7p9Ky2zuGxe
dUCY69rer+/mUc+7X0wE9ze/7Le+nPNvhjdjXghW/9EN90IgU/S/8ncQDI3Z8BgLm/vP4vUEn/JfcXAODPa/98g6W/ZaSlLtLgj5T+R5is/bXcP9WE+5ua+zd93h9L/3M1
3F+66SdfdfBHy/2tu+pLPvizWXPpTwh+CQA7tAHAHgCYHno2bVG7OmEJAVUCY05UC4y9Ru5/kwBwk4L/JgX/zeq+sTcp+G+S+9+k4Bc0j+RN8oq5SQC4SQC4ScF/k4KfFH
PHcXYsuGaRZhIcZoiaTpoWQxAgTSFNJjhMEjWRNIE0njQumiBAGhOFWqSao0mjSCNJIwTVGk4aRoAYShoiarCoQaSBpAFMBIn+pH6i+kYQCOhlH1JvUb2YCBQ9BdXpIaq7
TN3uj+rqFMbP77sXqt09DE3+DIZnu2kUiAwCkwkALP0X3H+pLP0XMgDB/SUAFOgA4KNI//Nl6b8SAMrgl9f/evePVq39lbv+kiwe/DG35Vdr7DfQxNjvArWx3zoaY7/i5J
9a7T9V5aYfc4M/cvcfThrMAPAIASCAADBVBQAFJ07ZDVu70c7O8RG7D7Pzar+WvuT1eqGJ/e0DYicTAKYQAKYQAKaQ+08hAEwh959CwT+Fgn8KBT+XoxdpLmlOtKC5sZPr
9J+1rP4vI1D/15Go/wvpZyb6/5/o5Y8jdGrwA+l7puFo8B29/Ha4KPr3NyPQ8Ovhev1P1FekL0V9wTRM0OeiPpPpU0FOnzANhdPHQ+H8sfjyI3opVoKLjwAALH5JREFU14
cyfUBvN9T791cu7w9Hm9fGov1LE9H+5Qn3RC+8PBHTW02kYJzEg38Jd/9pOvdfJgFA5v6C5lGAs9R/gQwA+to/Txf8QvrP3d9Vy/0jDdw/Rub+WmO/xnv+rd/1t1jjuG/r
xn7Nu/9qs1d96Qd/Npoc/JG7vwIA/hoAkLKAAavX29m9+pVdnxUlDzUMS6pU1T+mEgGgEgGgEgGgEgGgEgGgEgGgEgGgEgGgEgU/F7l/JQJAJQp+entCJfqElZu4vzakef
Un0Lx6e8vlUM4y/Lzi/7eQvb6FKOn1Laob//tB0cPVO+A1+4/wZdU/8UXVjhWuz0l9HHsgyWmMavOP1/+u8vrfUwx+bwUADNP/fDH9zzNyf/ncvzL9l7t/lkn3Nz/2G2/B
nv8wjaW/QFNXfVns/uau+jJ33Lcy/efuX8PY/Yc77uQAmPTIPtMAKM9HreHzGVEqN3N9bWTLym3xcJV2NpWTWlRpiw72r+CT6n/gs+p/k/6qMH1C+r1GV4Q6jeTBLzX/cg
2af3kq6T8DQKEi+EX3d9Uv/UkAyDNI/3Nl7q8DgFHwG+/6S9ft+jNu/pnb8qs++CN3f+OxX+P0v8Bo6c/LYNdf6Wv/DaVy/2EEgEFVd4oAuHhvAFBbAoDb6yNbVn0cLe1t
Ki89TGpj/xTerf4tvqjeqUKD/1uHzvCsP4R3/oXmn5T+T9et/bPgz1N0/027f76r3v2F1F9K/0N16X+ugfvniO6fbQQASzf9pFh84k+EGfcPKsVpv6bc39Tgj7H7bzI48F
Pm/iq1Pwt+OQBWEgByp567dwBo7v7GyEeqPoFH7NvbVE5qac9+nk/gxWrv4/MKCv5PSV9QdjG+bn9ksqO/zLh/nor76wGgb/4JwW8IAOP0X+n++qW/bMXgT6zG2G+iauef
A0DF/WNMDP6Yu+rLT3XuX+uyD5XBH5Njv/qlP7Wz/s0t/Q0Xg3+Ywy4MJABMfGT/vQNAjRELRAC8NbJV1fZoZd/BpnIUg0D7ai/jo+q/EAT+Lvfg/7T6n+hfuzeSXFjdP1
EHgFxZ80/v/iz9nyMCwDD458vc31dW+weIzm/c/FO6f4Ru15+h+2ea2PWXYvXYb5bG4E+O6q4/fysGf7Sv+lqlcdWX9pZfS9zfEABD7wcAHu01x649AaC161sjW1d9Eq3t
bSpPMQg8av8M3qz2ZbkD4BMK/r9qdkMEq/vd9Ov+SvefYeD+xs2/Qs3mX6CB++vTf9Xa3yVKVvsrAWCJ+ydpun+GgftnKS/7MHJ/cc9/vfJ3/xkm3F9t049599+hC34BAL
vuLQDyW/e2Q/87ld+u+erI1lSvtrF/2qZy1VMcBM9Xe5fc+o9ygwCr+39w7Iz5DYaKQz9yACiX/qTOf57o/vkcAFq1v5j+u8k7/3r3V2v+GZ73Z1z7a+z6MzH2G6ex59/c
pp9gK8d+Ldn1N1Mj/Z+i1fwz2PWnNfZr6P73BQCHXbrY4Y2DlT90eHUk+4NlbmVT+YpBoH21l/B+9R/KBQAs9f+SPs+UegOEQz/ETT/q7j9Tkfqz4M834/7y2j9P4f4hBr
W/bOlPNvij5v4LzWz6SSrFWf/mrvry1zjuW/W0X3HP/1zVsd/SX/VlifsPcxAAMMRhNwYQACbcSwDsc+tqh3cPVf7Q8bWRbao9g7bVnrWpnMUg0Jb0erXPyq0ZOKx2H6S6
jNEd+SVf989VcX+W+uubf96CFLW/D72fBABh8EdZ+wfLan+1sV9l+p9pNPYbb+XYb4bq4I8y+OWTf6Yv+6iYsd91Kkt/ytpfDwA199en/0NEAPSvuhvjCQArCABL7hkA3j
lQ+SPH10c+Wu05tLOpQvQowfXZam9S6v57mbIAlvr3qNUDcc6jyPXHYxGr/XVn/gmpv+Guv3y1pT9d8C+Qdf59xbHfQBUACO6fK2v+5ai4v2LqTzb4o2/8mbvsI1236y/G
xHl/6kt/uSrHfSs3/WgN/niWevDH3KYfefq/3WjwRw+A3RisA8ABAsAlAsD5ewGAbhwAHzu+MbJttefxmE3lpnYysUzg8Wov4L3q35UaAKzp95tjVwQ1HMav+xJu/NG7/1
I3g6k/lV1/0thvoYH7C51/P13tL2/+Gbr/Elnnf7EMAHL3X2iR+1s29luW4761rvryVh38WVmqsd9JGoM/Yy0Y/JG7/30EwD4CwJsj29Ef6GM2lYPUYcAg8Er1jwgAf5bK
+b9x6IRZ9QcJZ/2Jx33luMlHfgX3z5MO/dDt+ptrQeffV7fpx3jwR+7+puf+s8p41Ve8bNOP2uBPad3fx+qrvlarXvVlftefyuSf41bVsV+5+wsA2GMDwH8ZCqwUeKr6a/
io+q+lGvYZW7cfMsSrvqTgzzU47muZ/Lw/cctvvsqmn0JD9xdrf2X9L2/+yYPfuPmnXPqLs3jsN8lM7V++7l9g5divivvXKh/3Hy4O/sjTfwaAflX3YNy9B8Deyp8QANgf
6uPVXrSpnKXMDF7AW9W/sqoZyNy/b+2eSHIZRen+eOOjvnXLfvLz/pTuXyjb8qs69ivb9adW+2s3/wxrf3OXfSSVaew33NSmH5Xjvi0f+7Vk8Ed97FfL/Y3Hfo2X/uTuP0
gGgOUEgJx7B4DdBIC3CAC2YK1oCLAs4IXq71Jgd7TY/f+swYZ9hlPwC6l/ju6mnym6q74Uwe9meOCH8sQf5dSfn+quP8Olv1yVuX/DsV8dAJwsO+8vSTzrX2vpL6qB8eAP
d//62pd9+Gte9qF90eccjcs+TA/+GM/9C82/LZYt/Tkq3X+QCIC+HAAH7w8A2tkC9R5A4Hk8We0VKgPMzwSwMd8fHDrDp8FgHvw54jXfueI138z9l7kJN/3kye76KxDT/0
LdWX/6E3+0tvzqd/0p3X+ZmU0/6rV/nOZVX+run16qa76tH/zRp/+mrvqyxv0nqA7+SAd+KNP/4SqDP3L3H+iwlwNgrA0A//1S4M3qn/K63pTzf+XwN6bXG4BFVPcv4Vd8
s+CfSME/Wbzldyq/6Vd504/8sg/hqi/dcV9GJ/746c77Vy79qbv/Egvcf6EFl31oAUA+9x+lMvcfZuaqL8vcXzn2O9ek+6+x2P3HWzP4Y7D0J7j/XgEAVfbaAPBfhwBbEX
i++lv4zEF9JuBTUcNq90aGyxhK9YXgX8KDX3D/Za5S8Cvv+VPe9ONp0Wm/BW7y1F+/9Gfc/Isw2fzLNHXZh1Vjv+bdv7THfQvubzz3b/1pv9q7/kZrDv6Ydn89AA6VrPC/
TAC4YAPAfxUAHaq9hA8cvlXNAth6f/eaPRDvPJIcfzzXUl3wT6agn0JBL13yOcPgos85ojxl6b8p9zes/YPMuL+w9r+IAJCt0fzTu7/l6b/lV31pjf0us3js10vh/pak/+
Z2/W22fM+/g37t39D9B1bfiz4EgNEPH1q1IvByexsA/uNlwGvVP8IXDsqZgI/ZsE+NLghpOIyCnQX/BB78y7gm6YI/n4K/gIK/QAx+42u+PTUv+yiUn/WvcuKPcstvmMGW
X+b+kWbGfs2f9Z+oufQn7PqLNHHVl6k9/6YGf8p21dc6y6/6MuP+QzTS/wHV96FP1X0Y4nIgfX38VfdFk20A+E9nAc9Wfx2fOPysKwOkYZ+59QdS4I+jgJeCnwW+3vkLKP
UvUABALfg9dcFfpAt+4abfQoPa37rBnyju/osMO/+lOu8vTdH51xr8CdcY/AkyM/ijWvtbdNWXcvBHrfafqLrpx3jXn9T8G2Zi6U8K/v7V9qGv/b7LgxocGMJO/s4eb+sB
/KcB0J5evuvwFZUBQs3PZgPG1e2HHNexPPiZ8ij487ko+N31wV8oBn8huX8RAaCIAFDEg19+zbd00Se74ddH1f0LVNx/meppv4a1f7TS/Z3lgz+ye/6cjMd+Te760236UR
/8CbbK/QvM3vRj1v0tGPwZpzH4o7XnXz74I3f/fvb77vavsT8z8JuTD4f+fMoue4INAP9R6WcCXqrOzgn4nR8aOrROX77cl0+pfz45Pwv8AnL+QvcppKmkaRTQ00kzSB4o
psBf7j4bxe5z6N9MniQvkjdpHmk+aQGKKfCLKfC5KPiLKPgLZbf8FlDg53Ppb/ldRoG/lEt+2WcUcvg13+ymH/0tv1nO8cgi188kLaTAzyClU+CnOSVT+s+UInb/UwkCaQ
SAdCRS4CdQ4MdxLSQIZBIEsggAWQSAbDH9X0RaTBDIQRgFfigFvtD8W8oVRAqkwA+gwPenoPcj+ZLz+9YthA85/wIK/PkU9PNI3uT83nzpj9X+K0mrCAKrCACrCQBs7HcN
AWAtqYQgUEIAWEcAYGf9rydtIAhsIAhsJABsIgBsovR/M2kLQWALAWArAWArAWCbwWm/5gd/WPD3r7aXBf/lvjX2LQr64eQL4t0fdvfkYQPA/c0Cnqz2Mt6v/j261ezFU/
x1jVhnf4Is+KcaBP9MXfAXu4nB7z5XO/jdfMj9fUks8GXB7xaku+NPEfyuYYrA11/1Fa275FMI/jjxqO94ReBnUODrgz+F3F8f+FLws8CPVwS+FPzZYvBLgS8F/xIx+HP1
gV9vmS74/WTB71NXCHwp+L3F4Peqs8Io+GcZBP8MMfin8eBfz4NfCnx98G/mwS8FvhD826Tgv0bp/w4K/vWkjRT8G4dz7dxIwc9Fwb9xMNeejYOYHPdu6Oe4L2tUk4NDQ3
852XxTytWH8r0u2t2TBqANAA/CZODzeL7ae/it5o+YWP9vjKrXEYPq/IqBkur+RvpVEH/db1wDuH7HgLqkOn+I6ijqT5n+woDaf6F/7b/Rvw5TJ65+dTqjX22mLjr1rd2V
q0/tbjJ1R2+uHuhVqydXT65e6KFQb3SXqVutPqS+6FqTRC+7cPVD55qS+qMT1wD8LeqvGgN1+rPGIK6OXIPxh6jfawzR6TdHpqH4VdQvjsO4fnYcrtNPjiO4fmRyGIEfHE
aKGoXvRX3nMBrfVR+Nbx2YxnB9w1R9LL5mchD0P4dx+F/1cfiKazy+1GkClW/j6H1nbB3kuOKL0Y672pD7P04AeJwA8Di5/+PDHHY9TsHPRQB4nIL/8X6V9zw+4ZGDjxZ4
XnRiNf/iyeftDq27aXdPHw8iANpWeQ6PVjJQ5efQruoL/9EVgRfQlsqBVvZP45GqJPun0NK+Ax62b48W9k+guf3jpMfQrGo7NGWq0pZetkWTqo+iSZVHhZdGavNAqOkDp9
YWq5kValK1Jf3unlz7R/WQx7s6pNgRAEg77AgAdgQAOwKAHQU/FwHAjgBg17/KHrvxDx+kWv+c3Zn9t+22L7pmd88fDxoA2ju8jFfdP8A7LT/H2w8Leoe9bPEZXmj49n92
SbCduFtQOkGIHc/WWjxPkB0sKhw1/gQ/ZVi6c+Dhqo/Z9ECIAE3QbWP/fEnH6uEdfqo+3+5f83iQAMCc/5VG7yPGNxH7dx7Eri17SHuxe+s+7Ny0G2O7T6H3eR6P2//39g
dIAOAQsFeHQCvdnQNPyGBg04OgFvbt6Hf2gg0AZRFL819v9jHyM4tg+Lh9+w48x/qiLb3PY/Yv/L/IAh4VTxbWQ0CeDShhYNP91cMEgLb2L9oAUHYAfIS8hYVGALh18xbm
jl4gAuC/elaAIQSe1UFAOF5cDoInbRevPEB6hEqydvYvEQAiCAALbACwAaAsWYAeAm11x4sbgkAPBJvut54iCDxBf5evlPxZParDz9V9bACwAaB0qwGGENCD4FmNeweetu
kBUGvKAh63f5UyAAYAXxsAbAAoGwQMG4M6EEiyt+lBUhvKBJ6wf62kowMBwOFfBID9DABv2wDwYEPAGAbtOAies+kB0aPVnkb7aq+X/OkQTQDw/xdlAO4EgPf22gDwQEJA
CwTP2y5beeAufnkaHaq9SRlAzL8LAFHP9LJD6JXKr9V8fWTbqs9X+Dq/qSW88gBAuyoUHBX8fdwfELygeemITQ+CnqPf2yslb1Xp2eGNKp3/PQDo9GV/tvOocvvaL49sU/
mZivljpqB/qtar+LDt13i69muaAVpWADDAvN7kI7zR9JP/xKyA7S6Ff5fa2j9X0tiudYemdo/+ewDQ7Yt+HAAdCACPVn62Qv6QWcA/W+9N+E0N4dN8HRxfwWMqc/1lAcCj
FPzvPPI5ohbEo8tnfVFR34sNEjaZAkAzu8c7NLd7wgYAQwAw50+PzMLJo6cwpttktHd82QgCpQXAo1WexdstP+Of/+ypc+jxv4H/bwBg04OjdvbPcwC0sAFAHQDJoRk8mE
8dO41RnSfiieov8Zq9LABgz/nthz9DSlgGbt+6jUsXLqPbF/1tALDpvgCguQ0AWgB4nQPgn3/+4QF94shJjPh7At/9J0HAUgBIm4F48JPzJ4em87ezx8Xzl9Dty4oHAHvO
7LmwvoMkltFY23tgH8O+7zaVniE9q9koZe8n/3qGfRT2MfxtbNt0FbW3Pa/69naGr7c37t/Iv1fDz634OVR+TrW0M/V3wX5P7HtnLy1t3vKfheznrvXcTb4/fS32vZVnr8
gGACsAwB7HDh3HyE4T0EGEAG/iWQgA9lzfevhTJAanceeXHvcCACxzebb+G3je+S087yTouYZv0utf5M9PCOZnTP5xsbc9SgH/hMNLeLXx+3i/7Zd4p/Vn/POw586/T9n7
P0UZ1AvS16OXT9d5Tf92+nl0qPkKnqePfYHe/gw9N/YcdVusHV/in1f62OcavMm/ru77oNezz83+/Xh15fPsUONl/cc6Ce/DP7e99HPQv42JPU9LArjNQ8/iyVqv4I0WH+
P9dl/irZaf0v+/yl9vEgT0ddnHyb8m/57489JoQNd+VfG7Yt/rk/TzYj9j9jto/dAzwi5TGwDuLQDY4/ihE5QJjOd/TG3oF2EaAD5CgLHgbyEEv+T8CgBUYAnAIPVq4w8w
d9wCpMVkIjkiDcmR6UgMS0XAzFDMHOaFgb+NwKcdvhUaQwaBrHPVas/j6xd+gdcEXyxOXYo1hSVYvnQVkujzDPlzNF52e1e385Hpr096IiUqnX+tlKgMjO0zma+sCDB8Dn
9+3ANxQUn0nBZiwbRA+vj3Rcd7Hl8+/yOi/eORQh/LnrPXBB+82vQDHrwLpgbwz5cavRBh86LwWYfv+Mexz8u+/pfP/YTQeZH09gz+fvPp/V92fY//rl5xfx/ek/2QSl8z
RXxevb8bbNKN2d8Cg8pf9HyD50QgL7uQf++Fi5cj1CsKXT7vwwOWPweVj2crVd2/7o+E0BT+s2A/f/bcg+aG491WnxsFMusP9ftpKJLC03Tvz36OYV7R8Bzjg7E9p+DnN/
/mYGMwKMuMiQ0ApQCAlAkM/2s8+wHitaYfmgRAq4ee4s7PPtfNG7eM3q+iAcCcnTnWhlWbofa4fecOzpw+i42rN2P6sDk8WIzSZvo+WQCw97l9+7bR5zh/9jwFbALeIVd8
VCwLRnaZgLv/3NG9T/HSFXjJ5R3upq3snobnWB/cvXuXv+3Q3sMUyN8LmQgFzJC/RuH6teu6j12ZtwZvtfqEn72we/te3ev/of/G9pisS4+ZG3f9X1+cOnla9z7s/VnPpa
Xdk3iv9RfYsXmX4rn7Tg7S3LLNXseCf1yfqTiw56Dqz+/IwaPwGOXFsxS1UoaBM8ov3ujjLl26hK5f9jUC7iOVn0KgR5jq3x173ZXLV7B3537EBydxIEvfuw0A5QyAp2rp
m4BqjxOHT2LwH2PwZstPsCy9QBUAHsO88UaTj5ASvtDI+aXHhXMX0e3zfhUOgJLl62HucfHCJfh7hPB0XUpr2R/1h4/9D+tWbjD5sbdu3YLvtCA8VedV7rZd6I+bgUV6bN
2wg/8s2B88S/GjfOP1/ZWjJ9Hp816U2j6N1pWfxtQhs3Htqh4ALAt43vUtvNroA6MATghNxbMN3uDPlwGgy5d9cOzIcd3b2ftLAGCOu2X9NsXHz58YwDMStSBibtz1q744
dvi4ye/93JlzGPrXGKNgZN/riy7v8qzB8HGHwDu66yS0NfiYVgQAv+khuHXjlsmv+c8/d1GUuwKfP/UDf/42AJTzIBD7ZQ7+fTR3e63HkQPHMGPIXMoAjA8EuX7tBiK84x
Drl0zOf1P149kfQeGiFfig9VcVNg2oA0CxEgCsD6GWkRw9dAydPhOCkafFVKfOGu3NA1x6MOe+RLC4euWq4mMPHziCb1/6DY/YPYUvn/0Ru7fp3froweN4v/WXaG3HUvH3
sCR9mQyCFzC88zh6GwGgytPwnR6s+Jn5EFiYk77e5ENs37xT8TVZgH/Y7n8UbBoA2LSTH8+mA8A6ywDAwPdcg7ewOCVX4cbs58Cgbfg7zc8u4mYg/5tkP3v2NTeXbFX9/X
tSWfa444uqAJD/btjPm/3/3Tt3jT7H/MkBvJ/yWCnOn7QBwAwE2Nr/qC6TNCHA/jDYOv7p42eMU2sKMPa2a1euaQZ/EQX/l0//VKGjwGoAuHP7Dk+rfWYEYW3xOkV2wlL8
kLmRPPBZELxEDla8dKUiCDau2YIBHYdjxoi5VMJcVHzeCX2moVWlp/BOm8+xuqBEXyZQkP/2fmeCAwVia+Xbrl29hpnDPHmQPln7FV6bs58PB+n165jUb4bQb6EMwhAAVy
5fRe/vB/OyQ8g8ygcA7Of21XM/48Dug4qSIydtKbp91w/xIcmKn9vJY6fw50c9eAkkfQ4G0W9f+R37dh5Q/Rtg/Q9WOpgDwOH9RxC+IIZnQqzckj/Y7+Id+r7alaIpaAOA
Bd1f1uxjTT/mYOX1YEQvyF6OL5/6scKX/9QAwP645k8JQGO7tvjq+Z+xYfUmxfNjzb03W3zCa3V2wOnBPYcUb/cY4YXW9k/jZbf3sHb5OsX3xRpzj1V/Aa80fh+LyD3lgT
rg9+FoYdcenz/9A7aUbFMcnxY8O4IHzEuu76JgcTEPN/ZgZcSA34bz5/KGCgDu3v0H/jND8ASVFa3LEQAsG+n3y1CcOaUvY87Sc+n/yzBQ0OCbl3+l/z+nzwxu38KorhMV
ZQD/HD8PVfQk5I81RSV41f19hXurAWBN4Tq82fZjPFH3Rf7zleDIsycCMMvYtJqQNgCUQz+AQ6DTBBwrBwjw4M8qxv+e+YUHf0XvAVADAHMu9kf2SKUnKbV+AeHe0bqGHK
/X123nQcobZ62+pBr9lMIFx/eexp87yw6WZRUoMiLWsWZdcZaWhnpGyUoicvIBMwgAT+DPj7vj4L7Dip9LRlwWnqz3Cl5v/hE2rdWnzHt37MfPb/3NM4c3mn5sBAD2NVl/
4rXGH/KAKy8AMOCM6DKelye6ku/QUXT+oje9jTIc+pzHD59QfK6JfafzhqkcAFMGeBiVStLjAIH1/TZfKjJANQCUFG/AB499hYftOuDvT3ry8kuesY3pMZmN9doAUJFDNL
wc6DzRZE/AbPBTDVe0eAUF/8/iEk7FbwDSBkAwWlG9zf5Ipw+dy9Nw3R/m3kP448NuPOjee+RL+kM/qQDAxD7TeYOLAYDVvrq3kRsnR6bhqTqv8bfPGOqJu2LpwLOOSf54
+KH2fNnw/DllKlucuxIvNX0X79Fz3bNtn+71G1ZuxkePf60JAKm86Cg+3/IEAOtLnJcB4OjhY/zzMwC8+8gXvBEs/7lMoJ+LfGCHLasGzQlXwFXRACb3ZjCUu7caANbS74
4BgD2nD9p8hcP7jigMhfWh2lazAaCCtwYLEGDTgKXJBFjaVpyzEl+x4K/87D3b/WceAM9gVLeJ/I9RV88eP4nePwzizbz36A/dFADyVADAVhEY4NhS4OVLl3XfP5s9eNzh
BarpZ+LmTWUTbeOqzXj70U/x03t/4agMsgwMb7b4mINKCwA32YrLSG+0fOjBAQD7nC+5vYtFyUt073Pj+g362Z5SPG/2s3+0qmUAYCUS+xls27BDUQL5zQjG444v2QBwLz
IBVg6M7jJJERSWBP/y3FX46umfzJ4xcD8AMLwT/aGfvaBw1GF/j9X9oVsNgLqvCek4pctsVUFK1fMXFeFF57fhNc7X6Ge0a+sefPHCj+jzy2Cck9XWrCH4TD1h6EULAOxz
s6U29nVZPfwgAID93N979As+NKQbIjtyAumxmboa/g4/Ot5H4d6WAGCTbFWBfe+J4akE3det/ruyAaCUjUG2H2BM18lGNaB68LO0f6XQ7a9y7w/+sAgABn/oF85fICiUBQ
Cv84/94pkfsaVku+7t61ZsxMeUzrMOutFchTgLwOpZKWu4des2FkwO5N19FlhaABCWZI/is6e+5xOGDwIAWLB+9+rv2LVlt+y57MKcsfNxSfz+WPrOeh/P1H1D9/VLA4Ck
iDT6HDYA3LtMQIQA7wmYKAdYQBRmL9el/ffjuZYWAMMIAK3LAAD2x/pumy+wKn+t3uW37OFjrLnpecaTcRcv8d7AnNHzdWvsVy5dwaT+M3k2oQYA9sfPUmD2uHzxMkZ0nY
COH3W7vwBgTcCqwr6JPj8O1q0AsLezwZ2Bv4/QDRYJDcyN/Go5qRFYOgCk2gBw7yHwPEHgJc3GIKM7W+f/+rlf72nN/6AAgAXAy+7vYVGKvgZmy4mDO45SQEH+nGaP8kak
T6yuaXb65Bn0/XmoJgDYBN6h/Yd5psA+hu0t6PFdf96tv58AYM+V/Xwm9J6GGzdu6MpAth/g65d/wfYNOxVr/O+3/kq3J8AGgH8JABSrA3xY6IQy7c9Zif89+8t9Df77CQ
Be7lR/kS8FSkNErAEW5BmGnWJafPXqVe7WgpPfRXJEOpZl65cVD+w+hG9f/o3/4asB4NCBI0hP0A/HsBJjQv9pOLj30H0FAAvmJxxexIIpgYrZC8+Jvnij7ccKALLeS8eP
u+syRBsA/kUAkHbbtXd8hSAwkfcE2B/y8iWr8dWzP9/34L+fAOCNMPrcbMKPdb+FNP8yipYtx7GjJ3R1e0ZsFndw9odcsmo9dm7Tz/pv37AD77X6gn8PagBgDcapQz345h
jeZKPPmxCRjAN7D95XALDf+4tu7yKdvjfdHMTV6xjTazKeb/qWYkDq6pVrGNNzMqTzLW0A+JcBQLc6QOXA+J7TsDgxF1+T8/Nu/wPw3O4nANjnZptkzp05r/u6zPHZVCB7
bC7Zhlmj5/EhIfaHfPLEKcV8QNHSFbyMkA7EMATA8WMn0POHgchKzOFfm4Fm68ZtvNt+PwHAfuZvt/qMj1tLD9aj6PZNXzxW8wU+aq3PDG5i3uQAtK5iA8A9AQAf0KgqUz
k5NPtc7ICIl5zfLb/PWQ7P9f4C4GnelGObhOR/sNJjaWY++vw0WD/f/o9y6TSJSoIONYWzGNUAwIDxx4ddMXmgBwX/Td0eDPmo7P1YBWAjyexcAvlmKHai1AftvkIzu8f5
HIS0rZoNh7FMge2i5FulbQCo+AyAreXLVd67CMvzJJ+yPtfSAOB8eQGg0tP45IlvsXPTbtVVkhDPSHz8xDdGew0kZ2Q73djnkJquhgA4RQBggPn+tT9w5uRZ1a9xrwHAtv
eyBmi3//XD2TP6eQZ25sHv73bFx09+A89xPorx4LXL1+O1Jh/yn6kNABUIALa/u9MnvREyO5IUhdC50fCZEsSX6R6oyznsX+SHkg7/cxzCPWMQTM+XPdfZI+bhRad3rPpF
l2YQiE0FstexICgLAFggvNn8Y6OtyNJjcn8PnuKXrDB++1UqE8b3ZTsLTQPgr0964Hmnt7GqYI3qIRr3HABU+rEj30d1m4Tbd24r9kIwELK5CLaNXJ6lsNHnD9t9zc9YNA
uAJh8r9kqw7zk5Kt0GAEvETueZN9Gfu4v0x3Ke6tO+3w+9Z7P5lmYSzzd4G9kJS3Tjnuzpsq74W80/tWrvtyUAYOvn8k0vp0+dQf9fh5dpEEg62JOdz7c4NZd/nPzBlscG
/TEKTzd4HRnxWaoHpfT4tj9//qYA8PenPfFotWexYEqA6n75+wEAtgdg1ghviydF2VwA28vASgeTABDHoeXfA/uZsx2C/EwAGwAsBMD1m7K1ZBEAlZ99YO7wY7/IFwgAWf
FLlLvHKgQAT2PKwFnccXVr0weP8qk8YS/Al6UGAPt5sqyLnZ0nPwiV18THT+KXdzqhrcNzPNU3fLD7GL54+gfdkVmaAPisJz9/oDul3KzRds8A0FIdAMz92QlFSeGpFgOA
nakwuvtE/rswBwAGHvZ3IJ83mT1yHtpWt50HYBEAvCf46Zal+B7vU+fQ6xth4wv7fA+CWCrI5t8zYxcr/lD27zpYrgB4pJLwPbN/y8/6Y4MqrInV0q6DsB34iHI78ITecg
AUGm0HflqWjrKf+bShc/hyl/zBNrSwphhL8cf1nGq0Y277xp0U8B/pxqdNAYAFxtsPf4rNBsFtKQDaapwHYLgdmC07SgBg5yTIR8ElMLKfJzt8dGX+GsXPhQGWZTWsvDI8
UYg1ML3G+/IUXwsA7HfIdjuyrdHnZL0FVkrw34dtO7A1AND/8M+fuYDhf4/ntShrxLz+AOjVJh9w4i9JzasQALA/Ljak0sSuHT/eujBnufKgisJ1PFhaibsBDScd2Xgugw
c7sro4d4UiA4gLSeLbgXV74umPevCfoxS7DdmjcMkKvNHiIx5ovX8YjMuXrygCpjCnGC+6vKM/m9AUAOhrsI1A7CRhawHgNdaXg459Dvazai0ekc6CbejfYxQn8Jw4dhLd
v+7HDwRhW5RPHdcf9HH3n7sY32sqB8D7j36pOAXoypWr8PMIxrDOYzG8yzgM+Xs08rIKddDjG3rC0vBk7Vf5czE6EKRoHd5u/RnB4Ul4jPZWwPrKlSvo+d2gUh0TbgOAGA
wbV29BTsoy5FLA5abmPwDKQ15GIV86qggAsHQ8M2Exev84GHGBSfwwUMXustBUHsSPVhIuL926frvieaTHZOGD9l/xJTg2iitPR+eOWYB2snSUff2f3/pLESzswcZ2WX+A
bfT55qVfcfTgMYWrxQQkoD1bAhRBYgoAbfg5+c9Ryj7eaJuxOQCEe8fyI8rep2yEZSQfPPY/HsDsLH/2/clnCljfwmdaIF57+EOM7zdNkdVcvnyZH+fNDjX986NuytKBvr
ePHvuaPwcGGrYUOGPoXH5upHzb8xvNPuYHphgCgO2UHNd7Ci/V9u06oGh27t2xD5/yo9FtACgVAP5Nj/ICAPsDYjvuzp09x51LccLt6XMY9MdI3aGg7HSfmMBERYPt8qUr
3JVYmi5P3dlJNT2+7o+2sv3t7HfEpvnYH678wTb9PF5NWCl455HPsFnW2WaAmjXSG4/aP6vYe6EFAPY5WNnEQCJNBVoKADbCvbZoPT91h4mNE7OMiJ2881rjD1CyXHkaMg
MZOzKNzfD/c1cfiNs27sCXz/7E+xEjO09QHKLKviZ77tLvjTVeRxqUF1vWbsMnFMhN7R4zOhWYgYctKUr7CuSPmIBEKhdfL9Uqlg0A/08BoPVgqeXC+EXC9J14Sg2rwdnp
QCz9NfdgB2ayCzzkf4zMmdhx3vKlPnbQ5+iuE4WbbioLF5ewoSD5DABzc/lBGeYAwD7Py43eR3bSEqsAoPZgZ++PoK/P+hPsiHKtI73kPzd2YQnLGhi0Zo+apwAj+97YWY
c6ABBc//q0h+6sBD4nsO8w38rMsgNLjgXny4fb9+HXdzqrXuZiA4AGAFjH+dat2/9KABzce5gA8FmpALB+5UbNk41ZU5C5EbuJ5pP23yrSSZaCswlHdjT4JZUuu/Q5tqzb
LvwxGhxOyZ4rc6ispBz9MuPJ07yWZoHLAMPeHukbJ8swLvPlPfnn4gCgFJmlw9KDHRjKViv45xH34XsM9zI6aER3L0DrL7hTm3uwrckcAHbCwafsFqHr129oHvayJCOPX5
XGftasF8EOMZE/In1j8VRdWV+E3o8NSMknBS+KX7M5lQDsxiZ2WIjRz5n+Y1+PHd/GDnLt+c0Afr1YaY4E/38JADZVNrHvDOzYuAt7t+/H3m37/j2i58vOFni98UdW/cL5
nYQtP0Fq5EL+B7eN0vZt5IosEBgUcumPN3B2GL8A4/mGb6mmkux17Pjq8X2mYv2qTTh+9CRvjrFTchmUWNbw05t/aV5eyTrUk/vPpJJhF38OrN/yMYFGuo+QrZuPoLSZBS
vbKciu3Xq7hTLTYc+B7Ztnc//sLgD2vivyV+PHN//UdfHZ9/rHB92wsmANf/sO+lzs/dnPjDU03ySAZMRm87fxn4OK2Ndn2QovZcRLPNmRZL7Tg/jXZTsa2dDU6RNnuAOz
JU52LwF7P/Z8X3B6B/HByfzzsBKJfb+sJGAnJUs/G5atvNbkI6RFZ2Ine570eVlmMvTPsfxYM9ZkZSsx2zft4s9pO/2+Nq3dwm9Xig9NwZgek/gtR2UdZf9/BwD2w3rR+V
2+tPVvE7tUg6X/HRxfLtX9Bmx14Z2Wn/PbiZneIVdky2w86KsJO9hMgUUK1Debf8KbY6yTzY7N/prq7ufqv2nyqDM+19DwHfr6n/HnwFL5J2soB1fY52BOzd9Ogdpe5ftk
r2PPWfg8n9HP4xM+MSnd68cv1qRshX08+zxMrInJPo5f88V/DsLHv60h/nkJPs/We9Nos9cHj/4PXb7qw1cHenw7gGdLLDuSZyrtZV9D0nP13zK6e5AdKMNKH/37fc7hwX
4HbNrzHcPnRN8ry0ba13iZ7xxsZ7sctHR7AdgPro3YNPq3qbR7H/hUnnizrEJmrqnW+jxsYo3f4kPix3VZ8MfYTpyR53cHqt3EI3v7o7Ir1Y2yCfn3obLdWjqQU/61dJ/L
/kX1n4OK2lV9QfV2ZH59Gfve+bKhOjQNv4ZWg07r/djPU+05SVe6264Hf8C2A9tk079RNgDYZJMNADYA2GSTDQA2ANhkkw0ANgDYZJMNADYA2GSTDQA2ANhkkw0ANgDYZN
N/EQDrCABP/isB0L7Wy8NtALDJpjJnAO3/dQBgj8ccnu/0aJVnr9t+kTbZVOqj5/Ka2T3W/F8HAKKW3cN27Z9qW/W51bZfpE02lUo3SBNc7R6p+q8DwCMPPWnnbte6KhGs
I30TB22/TJtsskp3SfGklmSidv8qALBHS7sOdm0qP2P3RPWXHOkb+Lx1lWeCW1V+agNpp0022aSqXaTtrSo/vYRK5xGPVXvhEQKAHdO/8sGeOGUA9C+HhxrbtW3Q1O6xlq
TWNtlkk6ZaNbF7rLGrXWuHDg6v8Pj51wKAPZpT6mKTTTaVXraH7WF72B62h+1he9getoftYXvYHraH7WF72B62h+1he9geVj/+D3XiQQ2Z737rAAAAAElFTkSuQmCC
'''
icondata = base64.b64decode(icon)
tempFile = 'icon.ico'
iconfile = open(tempFile,'wb')
iconfile.write(icondata)
iconfile.close()

# Application frame
app = customtkinter.CTk()
app.title(f'Convertitore YouTube - V{current_version}')
app.resizable(0, 0)
screenWidth = app.winfo_screenwidth()
screenHeight = app.winfo_screenheight()
windowWidth = 720
windowHeight = 520
x = (screenWidth // 2) - (windowWidth // 2)
y = (screenHeight // 2) - (windowHeight // 2)
app.geometry(f'{windowWidth}x{windowHeight}+{x}+{y}')
app.wm_iconbitmap(tempFile)
os.remove(tempFile)

# Languages
language_strings = {
    'DE': {
        'placeholder': 'Geben Sie den Link ein..',
        'download_button': 'Herunterladen',
        'download_complete': 'Download abgeschlossen!',
        'invalid_link': 'Der eingegebene Link ist ungültig!',
        'copyright_label': '© 2024 Debeleac V. Andrei | Alle Rechte vorbehalten.',
        'new_version': 'Neue Version verfügbar',
        'file_downloaded': 'Datei erfolgreich heruntergeladen!',
        'error_during_file_download': 'Fehler beim Herunterladen der Datei!',
        'directory_not_selected': 'Keine Verzeichnisse ausgewählt.',
        'paste_button': 'Paste',
        'error_conversion_mp3': 'Fehler beim Konvertieren in MP3!',
        'conversion_in_progress': 'Konvertierung läuft. Bitte warten!',
        'file_already_present': 'Datei bereits im ausgewählten Verzeichnis vorhanden!',
        'playlist_downloaded': 'Download der Playlist abgeschlossen!',
        'playlist_button': 'Playlists herunterladen',
        'playlist_error': 'Der eingegebene Playlist-Link ist ungültig!',
        'popup_close_button': 'Schließen',
        'popup_remember_box': 'Nicht mehr anzeigen',
        'main_news': 'Was in dieser Version neu ist, ist Folgendes:',
        'date_label': 'Datum: 06/06/2024',
        'download_speed': 'Download-Geschwindigkeit:',
        'news': '''1. Aktualisierung der Bibliotheken, um die korrekte Funktion des Programms sicherzustellen.\n2. Wiederherstellen von Songdetails nach der Konvertierung in MP3.'''
    },
    'EN': {
        'placeholder': 'Enter the link..',
        'download_button': 'Download',
        'download_complete': 'Download complete!',
        'invalid_link': 'The link entered is invalid!',
        'copyright_label': '© 2024 Debeleac V. Andrei | All rights reserved.',
        'new_version': 'New version available',
        'file_downloaded': 'File downloaded successfully!',
        'error_during_file_download': 'Error downloading file!',
        'directory_not_selected': 'No directory selected.',
        'paste_button': 'Paste',
        'error_conversion_mp3': 'Error converting to MP3!',
        'conversion_in_progress': 'Conversion in progress.. please wait!',
        'file_already_present': 'File already present in the selected directory!',
        'playlist_downloaded': 'Playlist download complete!',
        'playlist_button': 'Download playlist',
        'playlist_error': 'The playlist link entered is invalid!',
        'popup_close_button': 'Close',
        'popup_remember_box': 'Don\'t show again',
        'main_news': 'What\'s new in this version are the following:',
        'date_label': 'Date: 06/06/2024',
        'download_speed': 'Download speed:',
        'news': '''1. Updating the libraries to ensure the correct functioning of the program.\n2. Recovering song details after converting to MP3.'''
    },
    'ES': {
        'placeholder': 'Ingresa al enlace..',
        'download_button': 'Descargar',
        'download_complete': '¡Descarga completa!',
        'invalid_link': '¡El enlace ingresado no es válido!',
        'copyright_label': '© 2024 Debeleac V. Andrei | Reservados todos los derechos.',
        'new_version': 'Nueva versión disponible',
        'file_downloaded': '¡Archivo descargado exitosamente!',
        'error_during_file_download': '¡Error al descargar el archivo!',
        'directory_not_selected': 'No hay directorios seleccionados.',
        'paste_button': 'Pegar',
        'error_conversion_mp3': '¡Error al convertir a MP3!',
        'conversion_in_progress': 'Conversión en progreso... ¡espera por favor!',
        'file_already_present': '¡El archivo ya está presente en el directorio seleccionado!',
        'playlist_downloaded': '¡Descarga de la lista de reproducción completa!',
        'playlist_button': 'Descargar listas de reproducción',
        'playlist_error': '¡El enlace de la lista de reproducción ingresado no es válido!',
        'popup_close_button': 'Cerrar',
        'popup_remember_box': 'No mostrar mas',
        'main_news': 'Las novedades de esta versión son las siguientes:',
        'date_label': 'Fecha: 06/06/2024',
        'download_speed': 'Velocidad de Descarga:',
        'news': '''1. Actualización de las bibliotecas para asegurar el correcto funcionamiento del programa.\n2. Recuperar detalles de la canción después de convertirla a MP3.'''
    },
    'IT': {
        'placeholder': 'Inserisci il link..',
        'download_button': 'Scarica',
        'download_complete': 'Scaricamento completato!',
        'invalid_link': 'Il link inserito non è valido!',
        'copyright_label': '© 2024 Debeleac V. Andrei | Tutti i diritti sono riservati.',
        'new_version': 'Nuova versione disponibile',
        'file_downloaded': 'File scaricato con successo!',
        'error_during_file_download': 'Errore durante il download del file!',
        'directory_not_selected': 'Nessuna directory selezionata.',
        'paste_button': 'Incolla',
        'error_conversion_mp3': 'Errore durante la conversione in MP3!',
        'conversion_in_progress': 'Conversione in corso.. prego, attendere!',
        'file_already_present': 'File già presente nella directory selezionata!',
        'playlist_downloaded': 'Scaricamento della playlist completato!',
        'playlist_button': 'Scarica playlist',
        'playlist_error': 'Il link della playlist inserito non è valido!',
        'popup_close_button': 'Chiudi',
        'popup_remember_box': 'Non mostrare più',
        'main_news': 'Le novità di questa versione sono le seguenti:',
        'date_label': 'Data: 06/06/2024',
        'download_speed': 'Velocità di scaricamento:',
        'news': '''1. Aggiornamento delle librerie per assicurare il corretto funzionamento del programma.\n2. Recupero dei dettagli della canzone dopo la conversione in MP3.'''
    },
    'RO': {
        'placeholder': 'Introduceți link-ul..',
        'download_button': 'Descarcă',
        'download_complete': 'Descărcare completă!',
        'invalid_link': 'Link-ul introdus nu este valid!',
        'copyright_label': '© 2024 Debeleac V. Andrei | Toate drepturile sunt rezervate.',
        'new_version': 'Nouă versiune disponibilă',
        'file_downloaded': 'Fișier descărcat cu succes!',
        'error_during_file_download': 'Eroare la descărcarea fișierului!',
        'directory_not_selected': 'Nu a fost selecționată nici-o locație.',
        'paste_button': 'Lipește',
        'error_conversion_mp3': 'Eroare la conversia în MP3!',
        'conversion_in_progress': 'Conversie în curs.. așteptați!',
        'file_already_present': 'Fișier deja prezent în directorul selecționat!',
        'playlist_downloaded': 'Descărcarea playlist-ului completă!',
        'playlist_button': 'Descarcă playlist',
        'playlist_error': 'Link-ul playlist-ului introdus nu este valid!',
        'popup_close_button': 'Închide',
        'popup_remember_box': 'Nu mai arăta',
        'main_news': 'Ceea ce este nou în această versiune sunt următoarele:',
        'date_label': 'Data: 06/06/2024',
        'download_speed': 'Viteză de descărcare:',
        'news': '''1. Actualizarea bibliotecilor pentru a asigura funcționarea corectă a programului.\n2. Recuperarea detaliilor melodiei după conversia în MP3.'''
    },
    'RU': {
        'placeholder': 'Введите ссылку..',
        'download_button': 'Скачать',
        'download_complete': 'Загрузка завершена!',
        'invalid_link': 'Введенная ссылка недействительна!',
        'copyright_label': '© 2024 Дебеляк В. Андрей | Все права защищены.',
        'new_version': 'Доступна новая версия',
        'file_downloaded': 'Файл успешно загружен!',
        'error_during_file_download': 'Ошибка загрузки файла!',
        'directory_not_selected': 'Каталог не выбран.',
        'paste_button': 'Вставить',
        'error_conversion_mp3': 'Ошибка конвертации в MP3!',
        'conversion_in_progress': 'Выполняется преобразование. Пожалуйста, подождите!',
        'file_already_present': 'Файл уже присутствует в выбранном каталоге!',
        'playlist_downloaded': 'Загрузка плейлиста завершена!',
        'playlist_button': 'Скачать плейлист',
        'playlist_error': 'Введенная ссылка на плейлист недействительна!',
        'popup_close_button': 'Закрывать',
        'popup_remember_box': 'Больше не показывать',
        'main_news': 'Что нового в этой версии:',
        'date_label': 'Дата: 06/06/2024',
        'download_speed': 'Скорость загрузки:',
        'news': '''1. Обновление библиотек для обеспечения корректной работы программы.\n2. Восстановление деталей песни после конвертации в MP3.'''
    }
}

# Icons
svg_code1 = '''
<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512'>
    <path d='M288 32c0-17.7-14.3-32-32-32s-32 14.3-32 32V274.7l-73.4-73.4c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3l128 128c12.5 12.5 32.8 12.5 45.3 0l128-128c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0L288 274.7V32zM64 352c-35.3 0-64 28.7-64 64v32c0 35.3 28.7 64 64 64H448c35.3 0 64-28.7 64-64V416c0-35.3-28.7-64-64-64H346.5l-45.3 45.3c-25 25-65.5 25-90.5 0L165.5 352H64zm368 56a24 24 0 1 1 0 48 24 24 0 1 1 0-48z'/>
</svg>
'''

svg_code2 = '''
<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512'>
    <path d='M104.6 48H64C28.7 48 0 76.7 0 112V384c0 35.3 28.7 64 64 64h96V400H64c-8.8 0-16-7.2-16-16V112c0-8.8 7.2-16 16-16H80c0 17.7 14.3 32 32 32h72.4C202 108.4 227.6 96 256 96h62c-7.1-27.6-32.2-48-62-48H215.4C211.6 20.9 188.2 0 160 0s-51.6 20.9-55.4 48zM144 56a16 16 0 1 1 32 0 16 16 0 1 1 -32 0zM448 464H256c-8.8 0-16-7.2-16-16V192c0-8.8 7.2-16 16-16l140.1 0L464 243.9V448c0 8.8-7.2 16-16 16zM256 512H448c35.3 0 64-28.7 64-64V243.9c0-12.7-5.1-24.9-14.1-33.9l-67.9-67.9c-9-9-21.2-14.1-33.9-14.1H256c-35.3 0-64 28.7-64 64V448c0 35.3 28.7 64 64 64z'/>
</svg>
'''

svg_code3 = '''
<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512'>
    <path d='M24 56c0-13.3 10.7-24 24-24H80c13.3 0 24 10.7 24 24V176h16c13.3 0 24 10.7 24 24s-10.7 24-24 24H40c-13.3 0-24-10.7-24-24s10.7-24 24-24H56V80H48C34.7 80 24 69.3 24 56zM86.7 341.2c-6.5-7.4-18.3-6.9-24 1.2L51.5 357.9c-7.7 10.8-22.7 13.3-33.5 5.6s-13.3-22.7-5.6-33.5l11.1-15.6c23.7-33.2 72.3-35.6 99.2-4.9c21.3 24.4 20.8 60.9-1.1 84.7L86.8 432H120c13.3 0 24 10.7 24 24s-10.7 24-24 24H32c-9.5 0-18.2-5.6-22-14.4s-2.1-18.9 4.3-25.9l72-78c5.3-5.8 5.4-14.6 .3-20.5zM224 64H480c17.7 0 32 14.3 32 32s-14.3 32-32 32H224c-17.7 0-32-14.3-32-32s14.3-32 32-32zm0 160H480c17.7 0 32 14.3 32 32s-14.3 32-32 32H224c-17.7 0-32-14.3-32-32s14.3-32 32-32zm0 160H480c17.7 0 32 14.3 32 32s-14.3 32-32 32H224c-17.7 0-32-14.3-32-32s14.3-32 32-32z'/>
</svg>
'''

svg_code4 = '''
<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 448 512'>
    <path d='M224 0c-17.7 0-32 14.3-32 32V51.2C119 66 64 130.6 64 208v25.4c0 45.4-15.5 89.5-43.8 124.9L5.3 377c-5.8 7.2-6.9 17.1-2.9 25.4S14.8 416 24 416H424c9.2 0 17.6-5.3 21.6-13.6s2.9-18.2-2.9-25.4l-14.9-18.6C399.5 322.9 384 278.8 384 233.4V208c0-77.4-55-142-128-156.8V32c0-17.7-14.3-32-32-32zm0 96c61.9 0 112 50.1 112 112v25.4c0 47.9 13.9 94.6 39.7 134.6H72.3C98.1 328 112 281.3 112 233.4V208c0-61.9 50.1-112 112-112zm64 352H224 160c0 17 6.7 33.3 18.7 45.3s28.3 18.7 45.3 18.7s33.3-6.7 45.3-18.7s18.7-28.3 18.7-45.3z'/>
</svg>
'''

svg_code5 = '''
<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512'>
    <path d='M256 48a208 208 0 1 1 0 416 208 208 0 1 1 0-416zm0 464A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM175 175c-9.4 9.4-9.4 24.6 0 33.9l47 47-47 47c-9.4 9.4-9.4 24.6 0 33.9s24.6 9.4 33.9 0l47-47 47 47c9.4 9.4 24.6 9.4 33.9 0s9.4-24.6 0-33.9l-47-47 47-47c9.4-9.4 9.4-24.6 0-33.9s-24.6-9.4-33.9 0l-47 47-47-47c-9.4-9.4-24.6-9.4-33.9 0z'/>
</svg>
'''

svg_code6 = '''
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512">
    <path d="M64 0C28.7 0 0 28.7 0 64V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V160H256c-17.7 0-32-14.3-32-32V0H64zM256 0V128H384L256 0zm2 226.3c37.1 22.4 62 63.1 62 109.7s-24.9 87.3-62 109.7c-7.6 4.6-17.4 2.1-22-5.4s-2.1-17.4 5.4-22C269.4 401.5 288 370.9 288 336s-18.6-65.5-46.5-82.3c-7.6-4.6-10-14.4-5.4-22s14.4-10 22-5.4zm-91.9 30.9c6 2.5 9.9 8.3 9.9 14.8V400c0 6.5-3.9 12.3-9.9 14.8s-12.9 1.1-17.4-3.5L113.4 376H80c-8.8 0-16-7.2-16-16V312c0-8.8 7.2-16 16-16h33.4l35.3-35.3c4.6-4.6 11.5-5.9 17.4-3.5zm51 34.9c6.6-5.9 16.7-5.3 22.6 1.3C249.8 304.6 256 319.6 256 336s-6.2 31.4-16.3 42.7c-5.9 6.6-16 7.1-22.6 1.3s-7.1-16-1.3-22.6c5.1-5.7 8.1-13.1 8.1-21.3s-3.1-15.7-8.1-21.3c-5.9-6.6-5.3-16.7 1.3-22.6z"/>
</svg>
'''

icon1 = generateIcon(svg_code1)
icon2 = generateIcon(svg_code2)
icon3 = generateIcon(svg_code3)
icon4 = generateIcon(svg_code4)
icon5 = generateIcon(svg_code5)

# Language selector
languagesVar = customtkinter.StringVar(value='IT')
languages = customtkinter.CTkOptionMenu(app, values=list(language_strings.keys()), command=changeLanguage, width=60, variable=languagesVar)
languages.pack(side=customtkinter.TOP, padx=5, pady=5, anchor=customtkinter.W)

# Link frame + paste button
frameLinkPaste = customtkinter.CTkFrame(app, fg_color=app._fg_color)
frameLinkPaste.pack(pady=(10, 0))

# Link input
urlVar = customtkinter.StringVar()
placeholder = language_strings[languages.get()]['placeholder']
link = customtkinter.CTkEntry(frameLinkPaste, width=350, height=40, textvariable=urlVar, corner_radius=0)
link.insert(0, placeholder)
link.pack(side=customtkinter.LEFT)

# Paste button
paste_button = language_strings[languages.get()]['paste_button']
pasteButton = customtkinter.CTkButton(frameLinkPaste, text=paste_button, command=pasteLink, width=30, height=40, corner_radius=0, border_spacing=5, image=icon2)
pasteButton.pack(side=customtkinter.RIGHT)

# Bind events for placeholder
link.bind('<FocusIn>', onEntryClick)
link.bind('<FocusOut>', onFocusOut)

# Bind CTRL + A event to handle the deletion of all text
link.bind('<Control-a>', handleCtrlA)

# Bind CTRL + Z event to handle the undo of all text
link.bind('<Control-z>', handleCtrlZ)

# Format selection
formatVar = customtkinter.StringVar()
formatVar.set('mp4')
formatChoice = customtkinter.CTkFrame(app, fg_color=app._fg_color)
mp4RadioButton = customtkinter.CTkRadioButton(formatChoice, text='Video (MP4)', variable=formatVar, value='mp4', state='disabled')
mp4RadioButton.pack(side='left', padx=10)
mp3RadioButton = customtkinter.CTkRadioButton(formatChoice, text='Audio (MP3)', variable=formatVar, value='mp3', state='disabled')
mp3RadioButton.pack(side='left', padx=10)
formatChoice.pack(pady=20)

# Frame download button and playlist button
downloadPlaylistButtons = customtkinter.CTkFrame(app, fg_color=app._fg_color)
downloadPlaylistButtons.pack()

# Download button
download_button = language_strings[languages.get()]['download_button']
download = customtkinter.CTkButton(downloadPlaylistButtons, text=download_button, command=startDownload, state='disabled', image=icon1)
download.pack(side='left', padx=10)

# Playlist button
playlist_button = language_strings[languages.get()]['playlist_button']
playlistButton = customtkinter.CTkButton(downloadPlaylistButtons, text=playlist_button, command=downloadPlaylist, state='disabled', image=icon3)
playlistButton.pack(side='left', padx=10)

# Binding the on_url_change function to the entry widget
urlVar.trace_add('write', onUrlChange)

# Finished downloading
messageLabel = customtkinter.CTkLabel(app, text='')
messageLabel.pack(pady=(9, 0))

# Progress percentage
pPercentage = customtkinter.CTkLabel(app, text='0%')
pPercentage.pack()

progressBar = customtkinter.CTkProgressBar(app, width=400)
progressBar.set(0)
progressBar.pack(padx=10, pady=10)

# Check latest version + copyright
frame = customtkinter.CTkFrame(app, fg_color=app._fg_color)
frame.pack(side=customtkinter.BOTTOM, fill='x', pady=5, anchor=customtkinter.E)
if latest_version and latest_version > current_version:
    new_version = language_strings[languages.get()]['new_version']
    downloadNewVersionButton = customtkinter.CTkButton(frame, text=new_version, command=downloadLatestVersion, image=icon4)
    downloadNewVersionButton.pack(side='left', padx=10, anchor=customtkinter.SW)
else:
    download_speed = language_strings[languages.get()]['download_speed']
    downloadSpeedLabel = customtkinter.CTkLabel(frame, text=download_speed)
    downloadSpeedLabel.pack(side='left', padx=10, anchor=customtkinter.SW)
copyright_label = language_strings[languages.get()]['copyright_label']
copyrightLabel = customtkinter.CTkLabel(frame, text=copyright_label)
copyrightLabel.pack(side='right', padx=(0, 10))

# Load default language from config
defaultLanguageFromConfig()

# Load news window
showNewFeaturesPopup()

# Run application
app.mainloop()